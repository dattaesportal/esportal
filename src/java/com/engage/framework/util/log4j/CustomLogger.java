/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.framework.util.log4j;

/**
 *
 * @author DELL
 */

import org.apache.log4j.Logger;

public class CustomLogger {

    private static Logger logger;

    public CustomLogger() {
        logger = Logger.getLogger("CustomLogger");
    }

    public CustomLogger(String className) {
        logger = Logger.getLogger(className);
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void warn(String message) {
        logger.warn(message);
    }

    public static void error(String message) {
        logger.error(message);
    }

    public static void fatal(String message) {
        logger.fatal(message);
    }
    
    public static void print(String message) {
        System.out.println(message);
    }


    public static void debug(String message,Exception exception) {
        logger.debug(message,exception);
    }

    public static void info(String message,Exception exception) {
        logger.info(message,exception);
    }

    public static void warn(String message,Exception exception) {
        logger.warn(message,exception);
    }

    public static void error(String message,Exception exception) {
        logger.error(message,exception);
    }

    public static void fatal(String message,Exception exception) {
        logger.fatal(message,exception);
    }
}