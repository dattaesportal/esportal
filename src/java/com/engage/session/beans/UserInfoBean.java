/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.session.beans;

import java.util.HashMap;

/**
 *
 * @author manish
 */
public interface UserInfoBean {

    public String getLastLoginTime();

    public void setLastLoginTime(String lastLoginTime);

    public String getProfileId();

    public void setProfileId(String profileId);

    public String getUserDisplayName();

    public void setUserDisplayName(String userDisplayName);

    public String getUserId();

    public void setUserId(String userId);

    public String getUserName();

    public void setUserName(String userName);


}
