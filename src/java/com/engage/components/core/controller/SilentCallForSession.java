/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.engage.components.core.controller;

import com.engage.framework.util.log4j.CustomLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 *
 * @author cmssjava
 */

@Component
@org.springframework.stereotype.Controller
public class SilentCallForSession implements Controller {

    @Override
    @RequestMapping(value = "/SendSilentSession", method = RequestMethod.POST)
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        ModelAndView mav = new ModelAndView("home");
        try {
            HttpSession session=hsr.getSession();
            session.setMaxInactiveInterval(15*60);
            mav.addObject("data", "success");
            mav.addObject("status", "200");
//            System.out.println("---------------complete silent call------------");
        } catch (Exception e) {
            mav.addObject("msg", e.getMessage());
            mav.addObject("status", "200");
//            e.printStackTrace();
            CustomLogger.error("SendSilentSession", e);
        }
        return mav;
    }
    
}
