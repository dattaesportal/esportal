package com.engage.components.core.controller;

import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.engage.components.core.helper.DashboardHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.DealerDashbordVO;
import com.engage.framework.util.log4j.CustomLogger;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class DashboardController {

    @Autowired
    DashboardHelper dashboardHelper;

    @RequestMapping(value = "/loadSearchCriteria", method = RequestMethod.POST)
    public ModelAndView loadSearchCriteria(@RequestBody String payload) throws CustomNonFatalException, JSONException {
        ModelAndView mav = new ModelAndView("home");
        try {
//                        System.out.println(payload);
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }

//            String forDashboard = FrameworkAppConstants.Pre_Sanction_Dashboard;reqJson.get("userId")
            String forDashboard = reqJson.get("forDashboard").toString();
            List<SangamMstrSelectCriteria> lstSelectCriteria = dashboardHelper.getSelectCriterias(forDashboard);

            mav.addObject("status", "200");
            mav.addObject("lstSelectCriteria", lstSelectCriteria);
        } catch (JSONException e) {
            mav.addObject("status", "400");
            CustomLogger.error("loadSearchCriteria", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", "Error : " + e.getMessage());
            CustomLogger.error("loadSearchCriteria", e);
        }
        return mav;
    }

	@RequestMapping(value = "/dealerDashboard", method = RequestMethod.POST)
	public ModelAndView saveSourcingData(@RequestBody String payload) throws CustomNonFatalException, JSONException {
		ModelAndView mav = new ModelAndView("home");
	 try {
//            System.out.println(payload);

            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID=Long.parseLong(reqJson.get("profileID").toString());
                        List<DealerDashbordVO> lstPostSanctionDashbordVO=dashboardHelper.getDealerDashbord(reqJson.get("userId").toString(), fromRowNum, toRowNum,profileID);
                                                 
                      
                        mav.addObject("status", "200");
			mav.addObject("lstPostSanctionDashbordVO", lstPostSanctionDashbordVO);
			
		} catch (Exception e) {
//                    e.printStackTrace();
                    CustomLogger.error("postSanctionDashboard", e);
			mav.addObject("status", "400");
			mav.addObject("msg", "Error : " + e.getMessage());
		}

		return mav;
	}
}
