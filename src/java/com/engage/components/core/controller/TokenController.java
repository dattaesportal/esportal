/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

/**
 *
 * @author DattatrayT
 */

import static com.cmss.engage.framework.util.AESAlgoClass.decrypt;
import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsTokenRequestLog;
import com.engage.components.core.helper.TokenHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.engage.framework.util.log4j.CustomLogger;
import java.awt.BorderLayout;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestParam;
import com.engage.components.core.entity.bean.EsTokenUser;
import com.engage.components.core.entity.bean.EsTokenRequestLog;

@Component
@Controller
public class TokenController {
    
    @Autowired
    TokenHelper tokenHelper;
    
    @RequestMapping(value = "/sec/gettoken", method = RequestMethod.GET)
    public ModelAndView downloadBureauReport(HttpServletResponse response,
            @RequestParam(value = "userid", required = true) String userid,
            @RequestParam(value = "password", required = true) String password
    ) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        try {
            
            if(userid == null || (userid!=null && (userid.equals("") || userid.equalsIgnoreCase("null")))){
                throw new CustomNonFatalException("userID not found","userID not found");
            }
            
            if(password == null || (password!=null && (password.equals("") || password.equalsIgnoreCase("null")))){
                throw new CustomNonFatalException("password not found","password not found");
            }
            
            EsTokenUser esTokenUser=tokenHelper.getEsTokenUser(userid);
            if(esTokenUser != null){
                if(!password.equals(esTokenUser.getPassword())){
                    throw new CustomNonFatalException("user not found","user not found");
                }
            }else{
                throw new CustomNonFatalException("user not found","user not found");
            }
            
            String jwt=tokenHelper.createJWT();
            
            EsTokenRequestLog esTokenRequestLog=new EsTokenRequestLog();
            esTokenRequestLog.setCreateDate(DateUtility.getCurrentDate());
            esTokenRequestLog.setModDate(DateUtility.getCurrentDate());
            esTokenRequestLog.setRequest("userid : "+userid+" password : "+password);
            esTokenRequestLog.setResponse("jwt : "+jwt);
            tokenHelper.setEsTokenRequestLog(esTokenRequestLog);
            
            if (jwt != null && !jwt.equals("")) {
                mav.addObject("status", "200");
                mav.addObject("token", jwt);
            }else{
                throw new CustomNonFatalException("please try again","please try again");
            }
            
        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("/sec/login", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("/sec/login", e);
        }

        return mav;
    }
}
