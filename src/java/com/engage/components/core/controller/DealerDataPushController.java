/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.helper.DealerDataPushHelper;
import com.engage.components.core.helper.TokenHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author VEN00288
 */
@Controller
@Component
public class DealerDataPushController {

    private static final String AUTHENTICATION_SCHEME = "Bearer";

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    DealerDataPushHelper delarDataPushHelper;

    @RequestMapping(value = "/dealer/pushloandetail", method = RequestMethod.POST)
    public ModelAndView getDelarData(@RequestHeader("Authorization") String auth, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");

        String token = auth.substring(AUTHENTICATION_SCHEME.length()).trim();

        try {
            tokenHelper.parseJWT(token);
            delarDataPushHelper.setDelarDataLog(payload);

            /*
                Blogic
             */
            String response = "success";  //temporary
            String applicationId = "123456";
            delarDataPushHelper.setResponse(response, applicationId);
            mav.addObject("status", "200");
            mav.addObject("message", "success");

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("message", "Invalid Token");
            e.printStackTrace();
        }
        return mav;
    }
}
