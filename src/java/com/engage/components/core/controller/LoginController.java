package com.engage.components.core.controller;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.cmss.engage.security.LDAPAuth;
import com.cmss.engage.security.RSADecryptClass;
import com.engage.components.core.entity.bean.FrameworkMstrSingleSignonTbl;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.entity.dao.SingleSignOnDAO;
import com.engage.components.core.helper.LoginHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.framework.util.log4j.CustomLogger;

/**
 *
 * @author SurajK
 */
import com.engage.session.beans.UserInfoBean;
import java.net.InetAddress;
import java.sql.*;
import java.util.ArrayList;
import javax.naming.AuthenticationException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

@Component
@Controller
public class LoginController {

    @Autowired
    LoginHelper loginHelper;
    @Autowired
    UserInfoBean userInfoBean;

    @Autowired
    SingleSignOnDAO singleSignOnDAO;

    @RequestMapping(value = "/sec/login", method = RequestMethod.POST)
    public ModelAndView gethomePage(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        CustomLogger.info(payload);
        String msg = null;
        String userAgent = null;
        try {
            if (!reqJson.has("j_username") || reqJson.get("j_username") == null || "".equals(reqJson.get("j_username").toString().trim())) {
                throw new CustomNonFatalException("User name not entered", "User name not entered");
            }
            if (!reqJson.has("j_password") || reqJson.get("j_password") == null || "".equals(reqJson.get("j_password").toString().trim())) {
                throw new CustomNonFatalException("Password not entered", "Password not entered");
            }
            if (!reqJson.has("captcha") || reqJson.get("captcha") == null || "".equals(reqJson.get("captcha").toString().trim())) {
                throw new CustomNonFatalException("Verification Text not entered", "Verification Text not entered");
            }
            try {
                if (reqJson.has("userAgent")) {
                    userAgent = reqJson.get("userAgent").toString();
                }
            } catch (Exception e) {
            }

            String user_name = reqJson.get("j_username").toString();
            String user_password = reqJson.get("j_password").toString();
            String captcha_session = (String) session.getAttribute("CAPTCHA");

//             System.out.println("sessid:"+session.getId());
            String captcha_form = reqJson.get("captcha").toString();
            String phrase = "";
//             System.out.println("captcha_form:"+captcha_form);
//             System.out.println("captcha_session:"+captcha_session);
            if (!captcha_form.equals(captcha_session)) {
                throw new CustomNonFatalException("Verification Text does not match with image", "Verification Text does not match with image");
            }

            if (reqJson.get("j_phase") != null || "".equals(reqJson.get("j_phase").toString())) {
                phrase = reqJson.get("j_phase").toString();
            }

            //String key = accessControlService.getPrvKey();
            String key = "MIIEowIBAAKCAQEA37kQNR2cQQzvL4xyvrRn8vPhECCoL1cJ2e5gwvC/JGeLaQ44G/lJQ+p8qFXE55tum3SBSGa6uiPm7rbBlyD0G6qcNx+lfAbqFd2Cw/VgEhHOmqbuCfxbViiafaUzZ5GQgMNxXaZZM3vsBN3N1yqK2i8K6i4kyMpeMVm+6qyMmqQyBt+GRkwDZDL3I659VgcE7RiwPJlLupZ+EVumetMGIxIr1VhQpj9kIJEWzvS51Ss8hUiS/1x2mu+VfYWKndtU5hkOPHQ1m20Vv2uXvse4g99mADOYdV6U6FBLa1VFUTT7osg6zM8+t5fPM5d4kv5s3Ecp+clMLeAAkaVuybcgsQIDAQABAoIBADWmR4C1TRc8Zs9cOtgIozTbzwjzC+HuWWptg767xNqSChhzVHlA2pTy0ln713BDEc8trSMTn4uruGFeaiY3vNI1o6iEakbH0bLUn9smKJestYLYrRSv86dBrWqcByu6+JAoTjNLBPEXso2oMwh6JzoTNebake+kt9nVz7avEjiy3G5DslU3MdyvXQ0HY2h4YvAs2BI8EG3tHmP1MXt/gbZkex6SJJFmAQtYouHp6D1gcVT0Yss0KT91A4o847jfT98a91ZxA+/5+7Pw/MUJdOapwE5b9JmuKu2RCnrEUeVXViBj+Abh6Ba4/jT/UXWhU1B9I0sEEq/7lXz46tX3h1ECgYEA+9pkXj7xCVC1Vx7QMYIPUjGIr3e7KKtMZEUPrrc8Y36tA0x4izzoy2WHcy4egHducXsOHURjR0eDBKAMMYcot+j30SvCwBxdhOQc64mTUG1mr/EpIZpx8H7HKCQCK8ZUdPO+5K1Ib7ne/Xz5UIhD1SFBV01UaHHXugNvFzQVYtMCgYEA42gY4/2OmIns4leLeaKlvat47v8M4D3qk34uTFNwG6wMQpzbShPgTDcAzoZe0n7kGoIWoL2IENa+KH8e0EvhlDfrgn/Hgh7bxYl1n+zo7HvghbX3t1VfojOFfQCVjHOYmhrWHdi8MEJxyI8brEtRnDJh09gOgD1UvtNN3ePpU+sCgYEArPJPshhu1QLDREw4k4bIlpzFTBlgQJFBKCn0lvnlUj2O+ZDtxk/RXLsfxIyLDZT2Fp92Fcr8kdodsnNlbC1wXLpyUwVF+z5/Sd6LrYMP0k8OEQv+PsEcNRYaFQqFIyJSpO0szUPhVjfBVTkYy6CHVDg98oF70S08AtSQ2NI/TVkCgYACYkH4idznznJUQRSuNr1ECrsezj+lTiDaYPU9SZagQ6JxRFGsKh8szVrNz2zJChl8CacREkuN2aJurD53coQv2JI4cPx3ablmDbXU8lfSus20uR6apmN1QNMCZ3Ip4cBUxTk/YNlALEIjFxT0LmnxmDmcTeSBBaOQyIhFbjEw9wKBgDE6T0rMllzxgwdmHJwLWajPyuLSHhFBHkV02IwDErc0WCTMSUSps5O1xG9aRo85+xmYfb0uPaJySG/Km9aziELkXj1Y/wl6E5Xya2GNlqnbvjDDAEhN8unS7HxuQhc2BfsWBntMp9iyjf6AIaJ9sbqltxSdnz6grDVH6QZ8ux39";
            System.out.println("user_password 1 : "+user_password);
            user_password = RSADecryptClass.decryptData(user_password, reqJson.get("j_key").toString(), key, phrase);
            System.out.println("user_password 2 : "+user_password);
            if (session != null) {
                session.invalidate();
            }
            session = req.getSession(true);
            session.setAttribute("success", "successfully accessed");
//            FrameworkMstrUserLogin mstrUserLogin = loginHelper.getUserByName(reqJson.get("j_username").toString());

            FrameworkMstrLogin frameworkMstrLogin=loginHelper.validateUser(user_name, user_password, 1L);
            if(frameworkMstrLogin != null){
                mav.addObject("userId", frameworkMstrLogin.getUserId());
                mav.addObject("userName", frameworkMstrLogin.getUserName());
                mav.addObject("userDisplayName", frameworkMstrLogin.getDisplayName());
                mav.addObject("profileId", frameworkMstrLogin.getUserType());
                mav.addObject("profileName", "Dealer");
                mav.addObject("profileDescription", "Dealer");
                mav.addObject("status", "200");
                mav.addObject("msg", "Authentication success");
                userInfoBean.setUserId("" + frameworkMstrLogin.getUserId());
                userInfoBean.setUserName(frameworkMstrLogin.getUserName());
                userInfoBean.setUserDisplayName(frameworkMstrLogin.getDisplayName());
                userInfoBean.setProfileId("" + frameworkMstrLogin.getUserType());
                
                FrameworkMstrSingleSignonTbl fmsst = new FrameworkMstrSingleSignonTbl();
                InetAddress serverIp = InetAddress.getLocalHost();
                fmsst.setCreTime(DateUtility.getCurrentDate());
                fmsst.setIpAddress(req.getRemoteAddr());
                fmsst.setSessionId(session.getId());
                fmsst.setUserId(userInfoBean.getUserId());
                fmsst.setHostName(serverIp.getHostAddress());
                fmsst.setUserAgent(userAgent);
                singleSignOnDAO.saveCurrentSession(fmsst);
            }else{
                throw new CustomNonFatalException("Authentication Failed", "Authentication Failed");
            }
            
            mav.addObject("status", "200");

            /*Spring security*/
            List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();
            AUTHORITIES.add(new SimpleGrantedAuthority("LOGGED_IN"));
//            System.out.println("mstrUserLogin.getUserId()" + mstrUserLogin.getUserId());
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(frameworkMstrLogin.getUserId(), "DEFAULT", AUTHORITIES);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
//            cnfe.printStackTrace();
            CustomLogger.error("/sec/login", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("/sec/login", e);
        }
//         System.out.println("mav : "+mav);
        return mav;
    }

    @RequestMapping(value = "/sec/EntityLogin", method = RequestMethod.POST)
    public ModelAndView getEntityLogin(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");

        String msg = null;
        try {

            JSONObject reqJson = new JSONObject(payload);

//            List<SangamDataNotification> notificationsList = loginHelper.getSangamDataNotification();
            List<String> notifications = new ArrayList<String>();
//            if (notificationsList != null) {
//                for (SangamDataNotification n : notificationsList) {
//                    notifications.add(n.getNotificationMessage());
//                }
//            }

            String currDate = DateUtility.parseDateToString(DateUtility.getCurrentDate(), "dd-MM-yyyy");

            mav.addObject("currDate", currDate);
            mav.addObject("notifications", notifications);
            mav.addObject("status", "200");

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("/sec/EntityLogin", e);
        }
//         System.out.println("mav : "+mav);
        return mav;
    }
}
