package com.engage.components.core.util.excepton;

/**
 *
 * @author rishi
 * The class is a custom exception class.
 * Specifically more for session/user-level and non fatal exceptions
 */
public class CustomNonFatalException extends Exception{
 public CustomNonFatalException() {
       super( "Session Exception" );
    }

    public CustomNonFatalException( String message,Integer intMessage ,Exception ex) {
       super(intMessage.toString());
    }
    public CustomNonFatalException( String message,Integer intMessage) {
       super(intMessage.toString());
    }
    public CustomNonFatalException( String strMessage,String strMessageId) {
       super(strMessageId);
    }
     public CustomNonFatalException( String strMessage) {
       super(strMessage);
//       System.out.println("err  "+strMessage);
    }
}
