package com.engage.components.core.util.excepton;
/**
 *
 * @author rishi
 * The class is a custom exception class.
 * Specifically more for Application and fatal exceptions
 */
public class CustomFatalException extends Exception{
 public CustomFatalException() {
       super( "Application Exception" );
    }

    public CustomFatalException( String message ,Exception ex) { 
       super(message);
    }
    
    public CustomFatalException( String message ) { 
       super(message);
    }
}
