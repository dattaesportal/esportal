package com.engage.components.core.helper;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.entity.dao.DashboardDAO;
import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import com.engage.components.esportal.vo.DealerDashbordVO;
import java.util.ArrayList;

/**
 *
 * @author Shrikant Katakdhond
 */

@Service
public class DashboardHelper {

    @Autowired
    DashboardDAO dashboardDAO;

    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard) { 
        return dashboardDAO.getSelectCriterias(forDashboard);
    }
    
    public List<DealerDashbordVO> getDealerDashbord(String userId,Long fromRowNum,Long toRowNum,Long profileID) {
        
        List<DealerDashbordVO> lst=new ArrayList<DealerDashbordVO>();
        
        DealerDashbordVO dashbordVO=new DealerDashbordVO();
        dashbordVO.setApplicantId("123");
        dashbordVO.setApplicationDate("30/10/2017");
        dashbordVO.setApplicationNo("16866");
        dashbordVO.setCaseCount("10");
        dashbordVO.setCustomerName("Shrikant");
        dashbordVO.setProduct("2WH");
        dashbordVO.setProgress("1");
        dashbordVO.setStatus("I");
        dashbordVO.setSubmittedDate("31/10/2017");
        
        lst.add(dashbordVO);
//        List<DealerDashbordVO> lst=dashboardDAO.getDealerDashbord(userId,fromRowNum, toRowNum,profileID);
//        if(lst!=null){
            return lst;
//        }else{
//            return null;
//        }
    }
}
