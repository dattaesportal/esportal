/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.engage.components.core.helper;

import com.engage.components.core.entity.bean.SangamApplicationConfigPropertiesTbl;
import com.engage.components.core.entity.dao.SangamApplicationConfigPropertiesTblDAO;
import com.engage.framework.util.log4j.CustomLogger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author cmssjava
 */
@Component
public class ApllicationConfigListener implements ApplicationListener<ContextRefreshedEvent> {
    
    @Autowired
    SangamApplicationConfigPropertiesTblDAO sangamApplicationConfigPropertiesTblDAO;
    
    public static List<SangamApplicationConfigPropertiesTbl> Properties;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        try {
            List<SangamApplicationConfigPropertiesTbl> lst=sangamApplicationConfigPropertiesTblDAO.getProperty();
            Properties=lst;
//            CustomLogger.info(".... ApplcationConfigListener:"+Properties.size());
        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    
}
