/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsDealerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.entity.dao.DealerDataPushDAO;

/**
 *
 * @author VEN00288
 */
@Service
public class DealerDataPushHelper {

    @Autowired 
    DealerDataPushDAO dekarDataDao;
    
    public void setDelarDataLog(String payload) {
        System.err.println("payload :::::"+payload);
        try
        {
            
            EsDealerData esDealerData=new EsDealerData();
        
            esDealerData.setApplicationId("123456");
            esDealerData.setRequest(payload);
            esDealerData.setResponse("");
//            esDealerData.setCreId("1");
//            esDealerData.setModId("123");
            esDealerData.setCreDate(DateUtility.getCurrentDate());
            esDealerData.setModDate(DateUtility.getCurrentDate());

            dekarDataDao.saveDelarDataLog(esDealerData);
        }catch(Exception e)
        {
        
        }
    }

    public void setResponse(String response,String applicationId) {
        try
        {
           dekarDataDao.saveResponse(response,applicationId);

        }catch(Exception e)
        {
        }
    }
    
}
