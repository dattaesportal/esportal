package com.engage.components.core.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.entity.dao.LoginDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;

@Service
public class LoginHelper {

    @Autowired
    LoginDAO loginDAO;

    public FrameworkMstrLogin validateUser(String userName, String password, Long userType)throws CustomNonFatalException{
        
        FrameworkMstrLogin frameworkMstrLogin=loginDAO.getFrameworkMstrLogin(userName);
        
        if(frameworkMstrLogin == null){
            throw new CustomNonFatalException("Invalid UserName", "Invalid UserName");
        }
        
        if(!frameworkMstrLogin.getUserPassword().equalsIgnoreCase(password)){
            throw new CustomNonFatalException("Invalid Password", "Invalid Password");
        }
        
        if(!frameworkMstrLogin.getUserType().equals(userType)){
            throw new CustomNonFatalException("Invalid Password", "Invalid Password");
        }
        
        return frameworkMstrLogin;
    }

}
