package com.engage.components.core.entity.bean;
// Generated 7 Mar, 2016 6:02:07 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SangamApplicationConfigPropertiesTbl generated by hbm2java
 */
@Entity
@Table(name="sangam_application_config_properties_tbl"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class SangamApplicationConfigPropertiesTbl  implements java.io.Serializable {


     private long propertyId;
     private String propertyName;
     private String propertyValue;
     private String propertyDescription;
     private String createdBy;
     private Date creDate;
     private String modBy;
     private Date modDate;
     private Character isActive;

    public SangamApplicationConfigPropertiesTbl() {
    }

	
    public SangamApplicationConfigPropertiesTbl(long propertyId) {
        this.propertyId = propertyId;
    }
    public SangamApplicationConfigPropertiesTbl(long propertyId, String propertyName, String propertyValue, String propertyDescription, String createdBy, Date creDate, String modBy, Date modDate, Character isActive) {
       this.propertyId = propertyId;
       this.propertyName = propertyName;
       this.propertyValue = propertyValue;
       this.propertyDescription = propertyDescription;
       this.createdBy = createdBy;
       this.creDate = creDate;
       this.modBy = modBy;
       this.modDate = modDate;
       this.isActive = isActive;
    }
   
     @Id 

    
    @Column(name="property_id", unique=true, nullable=false)
    public long getPropertyId() {
        return this.propertyId;
    }
    
    public void setPropertyId(long propertyId) {
        this.propertyId = propertyId;
    }

    
    @Column(name="property_name", length=50)
    public String getPropertyName() {
        return this.propertyName;
    }
    
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    
    @Column(name="property_value", length=200)
    public String getPropertyValue() {
        return this.propertyValue;
    }
    
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    
    @Column(name="property_description", length=200)
    public String getPropertyDescription() {
        return this.propertyDescription;
    }
    
    public void setPropertyDescription(String propertyDescription) {
        this.propertyDescription = propertyDescription;
    }

    
    @Column(name="created_by", length=50)
    public String getCreatedBy() {
        return this.createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="cre_date", length=23)
    public Date getCreDate() {
        return this.creDate;
    }
    
    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    
    @Column(name="mod_by", length=50)
    public String getModBy() {
        return this.modBy;
    }
    
    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=23)
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    
    @Column(name="is_active", length=1)
    public Character getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }




}


