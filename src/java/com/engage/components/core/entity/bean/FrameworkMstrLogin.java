/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author DattatrayT
 */
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "framework_mstr_login", schema = "dbo", catalog = "ES_Portal"
)
public class FrameworkMstrLogin implements java.io.Serializable {
    private Long userId;
    private String userName;
    private String userPassword;
    private Long userType;
    private Long userProfile;
    private Character isActive;
    private String displayName;
    private String mobileNo;
    private String emailId;
    private Date userLastLoginTime;
    private Date userLastLogoutTime;
    private Date modTime;
    private String modId;
    private Date creTime;
    private String creId;

    public FrameworkMstrLogin() {
    }

    public FrameworkMstrLogin(Long userId, String userName, String userPassword, Long userType, Long userProfile, Character isActive, String displayName, String mobileNo, String emailId, Date userLastLoginTime, Date userLastLogoutTime, Date modTime, String modId, Date creTime, String creId) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userType = userType;
        this.userProfile = userProfile;
        this.isActive = isActive;
        this.displayName = displayName;
        this.mobileNo = mobileNo;
        this.emailId = emailId;
        this.userLastLoginTime = userLastLoginTime;
        this.userLastLogoutTime = userLastLogoutTime;
        this.modTime = modTime;
        this.modId = modId;
        this.creTime = creTime;
        this.creId = creId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "user_password")
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Column(name = "user_type")
    public Long getUserType() {
        return userType;
    }

    public void setUserType(Long userType) {
        this.userType = userType;
    }

    @Column(name = "user_profile")
    public Long getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(Long userProfile) {
        this.userProfile = userProfile;
    }

    @Column(name = "is_active")
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Column(name = "display_name")
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Column(name = "mobile_no")
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Column(name = "email_id")
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_last_login_time", length = 23)
    public Date getUserLastLoginTime() {
        return userLastLoginTime;
    }

    public void setUserLastLoginTime(Date userLastLoginTime) {
        this.userLastLoginTime = userLastLoginTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_last_logout_time", length = 23)
    public Date getUserLastLogoutTime() {
        return userLastLogoutTime;
    }

    public void setUserLastLogoutTime(Date userLastLogoutTime) {
        this.userLastLogoutTime = userLastLogoutTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_time", length = 23)
    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    @Column(name = "mod_id")
    public String getModId() {
        return modId;
    }

    public void setModId(String modId) {
        this.modId = modId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_time", length = 23)
    public Date getCreTime() {
        return creTime;
    }

    public void setCreTime(Date creTime) {
        this.creTime = creTime;
    }

    @Column(name = "cre_id")
    public String getCreId() {
        return creId;
    }

    public void setCreId(String creId) {
        this.creId = creId;
    }
    
}
