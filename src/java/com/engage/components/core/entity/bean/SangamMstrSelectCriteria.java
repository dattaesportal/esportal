/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 *
 * @author Shrikant Katakdhond
 */
//@Cacheable
//@Cache(region ="sangam_mstr_select_criteria", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name="sangam_mstr_select_criteria"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
        
public class SangamMstrSelectCriteria implements java.io.Serializable{
     private Long selectId;
     private String criteria;
     private String modBy;
     private Date modDate;
     private String createdBy;
     private Date createdDate;
     private Character isActive;
     private String validationType;
     private Long validationSize;
     private String columnName;
     private String forDashboard;

    public SangamMstrSelectCriteria() {
    }


    public SangamMstrSelectCriteria(Long selectId, String criteria, String modBy, Date modDate, String createdBy, Date createdDate, Character isActive, String validationType, Long validationSize,String columnName,String forDashboard) {
        this.selectId = selectId;
        this.criteria = criteria;
        this.modBy = modBy;
        this.modDate = modDate;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.isActive = isActive;
        this.validationType = validationType;
        this.validationSize = validationSize;
        this.columnName=columnName;
        this.forDashboard=forDashboard;
    }


     
     @Id @GeneratedValue(strategy=IDENTITY)

    @Column(name="select_id", unique=true, nullable=false)
    public Long getSelectId() {
        return selectId;
    }

    public void setSelectId(Long selectId) {
        this.selectId = selectId;
    }

    @Column(name="criteria", length=50)
    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    @Column(name="mod_by", length=50)
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name="create_by", length=50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=23)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name="is_active", length=1)
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }
     
    @Column(name="validation_type", length=23)
     public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    @Column(name="validation_size")
    public Long getValidationSize() {
        return validationSize;
    }

    public void setValidationSize(Long validationSize) {
        this.validationSize = validationSize;
    }

    @Column(name="column_name")
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    
    @Column(name="for_dashboard")
    public String getForDashboard() {
        return forDashboard;
    }

    public void setForDashboard(String forDashboard) {
        this.forDashboard = forDashboard;
    }
    
    
}
