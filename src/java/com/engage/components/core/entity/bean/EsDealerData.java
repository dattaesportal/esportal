/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */

@Entity
@Table(name="es_dealer_data_request_log"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class EsDealerData {
     private long logId;
     private String applicationId;

     private String request;
     private String response;
//     private String modId;
     private Date modDate;
//     private String creId;
     private Date creDate;

    public EsDealerData() {
    }

    public EsDealerData(long logId, String applicationId, String request, String response, Date modDate, Date creDate) {
        this.logId = logId;
        this.applicationId = applicationId;
        this.request = request;
        this.response = response;
        this.modDate = modDate;
        this.creDate = creDate;
    }

    

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "log_id", unique = true, nullable = false)
    public long getLogId() {
        return logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

     @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
    
    
    
    @Column(name = "request")
    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Column(name = "response")
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

//    @Column(name = "mod_id")
//    public String getModId() {
//        return modId;
//    }
//
//    public void setModId(String modId) {
//        this.modId = modId;
//    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

//    @Column(name = "cre_id")
//    public String getCreId() {
//        return creId;
//    }
//
//    public void setCreId(String creId) {
//        this.creId = creId;
//    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

     
    
    
}
