package com.engage.components.core.entity.dao;

import java.util.List;
import com.engage.components.core.entity.bean.*;

public abstract interface SingleSignOnDAO {


	public FrameworkMstrSingleSignonTbl getCurrentSession(String userId);
        
        public void saveCurrentSession(FrameworkMstrSingleSignonTbl fmsst);

	//public FrameworkMstrUserLogin setUserLogin(FrameworkMstrUserLogin userLogin);
}
