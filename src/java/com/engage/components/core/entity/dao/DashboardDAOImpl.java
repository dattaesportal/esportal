package com.engage.components.core.entity.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import com.engage.components.esportal.vo.DealerDashbordVO;
import com.engage.session.beans.UserInfoBean;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Shrikant Katakdhond
 */

@Repository
public class DashboardDAOImpl implements DashboardDAO {

    private static final String CacheRegion = "socialbanking";

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    UserInfoBean userInfoBean;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(SangamMstrSelectCriteria.class);
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        cr.add(Restrictions.eq("forDashboard", forDashboard));
        List<SangamMstrSelectCriteria> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
    
    @Override
    @Transactional
    public List<DealerDashbordVO> getDealerDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID) {
//        String sql = "	select \n" +
//"	S.[APPLICATION_ID] as applicantId,\n" +
//"	S.[APPLICATION_FORM_NO] as applicationNo,\n" +
//"	A.[FIRST_NAME]+' '+[LAST_NAME] as customerName,\n" +
//"	S.[PRODUCT] as product,\n" +
//"	S.[CREATED_DATE] as applicationDate,\n" +
//"	case S.[STATUS]\n" +
//"	when '"+FrameworkAppConstants.POST_SANCTION_SAVED+"' then 'SAVED'\n" +
//"	when '"+FrameworkAppConstants.POST_SANCTION_REJECTED+"' then 'REJECTED'\n" +
//"	when '"+FrameworkAppConstants.APP_STS_LPC_APPROVED+"' then 'LPC APPROVED'\n" +
//"	when '"+FrameworkAppConstants.APPLICATION_STATUS_APPROVED+"' then 'SAVED'\n" +
//"	when '"+FrameworkAppConstants.MOVE_TO_POST+"' then 'RE-SUBMIT'\n" +
//"	when '"+FrameworkAppConstants.APPLICATION_AUTO_RETRY_POST+"' then 'ON AUTO_RETRY'\n" +
//"	end as status,\n" +
//"	S.[PRE_SUBMITTED_DATE] as submittedDate,\n" +
//"	S.[PROGRESS] as progress\n" +
//"	from\n" +
//"	[dbo].[sangam_data_sorcing] as S inner join [dbo].[sangam_data_applicants_details] as A \n" +
//"	on (S.[APPLICATION_ID]=A.[APPLICATION_ID] and  A.[APPLICANT_TYPE]='P')\n" +
//"	where\n" +
//"	S.[STATUS] in ('"+FrameworkAppConstants.POST_SANCTION_SAVED+"', '"+FrameworkAppConstants.POST_SANCTION_REJECTED+"', '"+FrameworkAppConstants.APP_STS_LPC_APPROVED+"', '"+FrameworkAppConstants.APPLICATION_STATUS_APPROVED+"' ,'"+FrameworkAppConstants.APPLICATION_AUTO_RETRY_POST+"' , '"+FrameworkAppConstants.MOVE_TO_POST+"')";
//
//        if (userInfoBean != null && userInfoBean.getProfileId() != null
//                && userInfoBean.getProfileId().equals("2")) {
//            sql += " AND S.CREATE_BY IN( select profile_.USER_ID as y0_ "
//                    + " from sangam_mpng_user_profile profile_ "
//                    + " where profile_.PROFILE_ID='2' and profile_.USER_ID in "
//                    + " (select user_id from sangam_mpng_user_branch "
//                    + " where BRANCH_ID in (select BRANCH_ID from sangam_mpng_user_branch "
//                    + " where USER_ID=" + userId + ")))";
//        } else {
//            sql += " AND S.CREATE_BY = " + userId;
//        }
        String sql = "exec SPgetPostSanctionDashboard " + userId + "," + fromRowNum + "," + toRowNum + "," + profileID;
        List<DealerDashbordVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("applicantId", StringType.INSTANCE)
                .addScalar("applicationNo", StringType.INSTANCE)
                .addScalar("customerName", StringType.INSTANCE)
                .addScalar("product", StringType.INSTANCE)
                .addScalar("applicationDate", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .addScalar("submittedDate", StringType.INSTANCE)
                .addScalar("progress", StringType.INSTANCE)
                .addScalar("caseCount", StringType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(DealerDashbordVO.class))
                .list();

//		System.out.println("Size: "+lst.size());
        return lst;
    }
}
