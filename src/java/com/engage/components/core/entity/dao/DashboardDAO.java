package com.engage.components.core.entity.dao;

import java.util.List;
import com.engage.components.core.entity.bean.*;
import com.engage.components.esportal.vo.DealerDashbordVO;

/**
 *
 * @author Shrikant Katakdhond
 */

public abstract interface DashboardDAO {
    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard);
    public List<DealerDashbordVO> getDealerDashbord(String userId,Long fromRowNum,Long toRowNum,Long profileID);
}
