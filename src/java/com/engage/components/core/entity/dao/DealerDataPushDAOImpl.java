/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsDealerData;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author VEN00288
 */
@Repository
public class DealerDataPushDAOImpl implements DealerDataPushDAO{

    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Override
    @Transactional
    public void saveDelarDataLog(EsDealerData esDealerData) {
      sessionFactory.getCurrentSession().save(esDealerData);
    }

    @Override
    @Transactional
    public void saveResponse(String response, String applicationId) {
        
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(EsDealerData.class);
        criteria.add(Restrictions.eq("application_id",applicationId));
        List<EsDealerData> lst = criteria.list();
        EsDealerData delarData=lst.get(0);
        delarData.setResponse(response);
        sessionFactory.getCurrentSession().saveOrUpdate(delarData);
        
    }

     
}
