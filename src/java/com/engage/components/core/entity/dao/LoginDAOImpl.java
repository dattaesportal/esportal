package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.DateUtility;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.*;

@Repository
public class LoginDAOImpl implements LoginDAO {

    private static final String CacheRegion = "socialbanking";

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public FrameworkMstrLogin getFrameworkMstrLogin(String username) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(FrameworkMstrLogin.class);
        cr.add(Restrictions.eq("userName", username));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<FrameworkMstrLogin> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
}
