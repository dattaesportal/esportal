/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsDealerData;

/**
 *
 * @author VEN00288
 */
public abstract interface DealerDataPushDAO {

    public void saveDelarDataLog(EsDealerData esDealerData);

    public void saveResponse(String response, String applicationId);
    
}
