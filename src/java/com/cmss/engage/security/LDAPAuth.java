/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.security;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.ArrayList;
import java.util.Hashtable;  
import java.util.List;
import javax.naming.AuthenticationException;
import javax.naming.Context;  
import javax.naming.NamingException;  
import javax.naming.ldap.InitialLdapContext;  
import javax.naming.ldap.LdapContext; 

/**
 *
 * @author SurajK
 */
public class LDAPAuth {
    
    public static String doLDAPAuthentication(String userName,String password,String ldapUrl) throws JsonProcessingException{
        LdapContext ctx = null;
         try{
             Hashtable ldaphs = new Hashtable();
            ldaphs.put(Context.INITIAL_CONTEXT_FACTORY,  "com.sun.jndi.ldap.LdapCtxFactory");  
            ldaphs.put(Context.SECURITY_AUTHENTICATION, "Simple");
            ldaphs.put(Context.SECURITY_PRINCIPAL,userName+"@"+ApplicationPropertyManager.getProperty("LDAP_DOMAIN"));  
            ldaphs.put(Context.SECURITY_CREDENTIALS, password);
            ldaphs.put(Context.PROVIDER_URL, ldapUrl);
            ctx = new InitialLdapContext(ldaphs, null);
            return FrameworkAppConstants.SUCCESS_AUTHENTICATION;
         }catch(AuthenticationException nex){
            CustomLogger.warn("doLDAPAuthentication - AuthenticationException", nex);
            return FrameworkAppConstants.FAILURE_AUTHENTICATION;
         }
         catch(NamingException nex){
             CustomLogger.warn("doLDAPAuthentication - NamingException", nex);
             return FrameworkAppConstants.NETWORK_ERROR;
         }catch (Exception e){
             CustomLogger.warn("doLDAPAuthentication - Exception", e);
             return FrameworkAppConstants.NETWORK_ERROR;
         }
    }
   
}
