/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.security;

/**
 *
 * @author Godwin
 */
 
import com.engage.framework.util.log4j.CustomLogger;
import javax.servlet.ServletException;
 
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
 
import java.awt.image.BufferedImage;
 
import javax.imageio.ImageIO;
import javax.servlet.http.*;
 
import java.io.*;
 
 
public class CaptchaGenServlet extends HttpServlet {
 
         public static final String FILE_TYPE = "jpeg";
 
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {
 
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);
 
            String captchaStr="";
 
        //captchaStr=Util.generateCaptchaTextMethod();
 
            captchaStr=Util.generateCaptchaTextMethod2(6);
 
            try {
 
                int width=100;      int height=40;
 
                Color bg = new Color(255,255,255);
                Color fg = new Color(0,0,0);
 
                Font font = new Font("Arial", Font.BOLD, 20);
                BufferedImage cpimg =new BufferedImage(width,height,BufferedImage.OPAQUE);
                Graphics g = cpimg.createGraphics();
 
                g.setFont(font);
                g.setColor(bg);
                g.fillRect(0, 0, width, height);
                g.setColor(fg);
                g.drawString(captchaStr,10,25);   
 
                HttpSession session = request.getSession();
                session.setAttribute("CAPTCHA", captchaStr);
//                System.out.println("captchaStr:"+captchaStr);
//                System.out.println("captchaStr set:"+session.getAttribute("CAPTCHA"));
                
//                System.out.println("sessid:"+session.getId());
               OutputStream outputStream = response.getOutputStream();
 
               ImageIO.write(cpimg, FILE_TYPE, outputStream);
               outputStream.close();
 
            } catch (Exception e) {
//                    e.printStackTrace();
                CustomLogger.error("CaptchaGenServlet", e);
            }
        }
 
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {
            doPost(request, response);
        }
 
 }