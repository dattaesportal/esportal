/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.security;

import com.engage.components.core.util.excepton.CustomFatalException;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.StringTokenizer;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author manish
 */
public class RSADecryptClass {
    /*
     static {
     java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
     }
     */

    public static PrivateKey getPrivateKeyFromString(String privateKey) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
        RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
        return privKey;
    }

    public static String decrypt(String cipherText, PrivateKey privateKey) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(Base64.decodeBase64(cipherText)), "UTF-8");
    }

    public static String decryptData(String strDecryptData, String strKey, String strPrivateKey, String phrase) throws CustomFatalException {
        try {
            
            strDecryptData = java.net.URLDecoder.decode(strDecryptData,"UTF-8");
            strKey = java.net.URLDecoder.decode(strKey,"UTF-8");
            phrase = java.net.URLDecoder.decode(phrase,"UTF-8");
            
            if (strDecryptData == null || "".equals(strDecryptData.trim())
                    || strKey == null || "".equals(strKey.trim())) {
                return null;
            }
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(strPrivateKey));

            RSAPrivateKey decryptionKey = (RSAPrivateKey) keyFactory.generatePrivate(privSpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, decryptionKey);
            //}
            byte[] utf8 = cipher.doFinal(Base64.decodeBase64(strKey.getBytes()));
            String strDecryptedKey = new String(utf8, "UTF8");
            StringTokenizer sttoken = new StringTokenizer(strDecryptedKey, "|");
            String[] keyiv = new String[2];
            int i = 0;
            while (sttoken.hasMoreElements()) {
                keyiv[i] = sttoken.nextToken();
                i++;
            }
           
            if (!"".equals(phrase)) {

                AesUtil aesUtil = new AesUtil(128, 1000);
                String strDecryptedData = aesUtil.decrypt(keyiv[0], keyiv[1], phrase, strDecryptData);
                return strDecryptedData;
            } else {
                Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
                IvParameterSpec iv = new IvParameterSpec(keyiv[1].getBytes("UTF-8"));
                byte[] key = keyiv[0].getBytes("UTF-8");
                SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                aes.init(Cipher.DECRYPT_MODE, secretKey, iv);
                String strDecryptedData = new String(aes.doFinal(Base64.decodeBase64(strDecryptData)), "UTF-8");
                return strDecryptedData;
            }

        } catch (UnsupportedEncodingException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();
            throw new CustomFatalException("Data Transmission error. 1");
        } catch (NoSuchAlgorithmException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 2");
        } catch (NoSuchProviderException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 3");
        } catch (InvalidKeySpecException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 4");
        } catch (NoSuchPaddingException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 5");
        } catch (InvalidKeyException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 6");
        } catch (IllegalBlockSizeException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 7");
        } catch (BadPaddingException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 8");
        } catch (InvalidAlgorithmParameterException e) {
            CustomLogger.error("DATA DECRYPTION", e);
//            e.printStackTrace();;
            throw new CustomFatalException("Data Transmission error. 9");
        }
    }
}