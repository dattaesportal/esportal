/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.framework.util;

import java.security.Security;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author inmite
 */
public class AESAlgoClass {
    
    public static final String STR_KEY="GKsks749JWHHsu43";
    public static final String STR_IV="Ks8730Jwimak39IU";

    public static String encrypt(String strPlainText) throws Exception{
             Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
             Cipher aes = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            IvParameterSpec iv = new IvParameterSpec(STR_IV.getBytes("UTF-8"));
           
            byte[] key = STR_KEY.getBytes("UTF-8");
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            aes.init(Cipher.ENCRYPT_MODE, secretKey, iv);
            String strEncryptedText = Base64.encodeBase64String(aes.doFinal(strPlainText.getBytes("UTF-8")));
            return strEncryptedText;
             
             
    }
    
    public static String decrypt(String strEncryptedText) throws Exception{
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
             Cipher aes = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            IvParameterSpec iv = new IvParameterSpec(STR_IV.getBytes("UTF-8"));
           
            byte[] key = STR_KEY.getBytes("UTF-8");
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            aes.init(Cipher.DECRYPT_MODE, secretKey, iv);
            String strDecryptedText = new String(aes.doFinal(Base64.decodeBase64(strEncryptedText)), "UTF-8");
            return strDecryptedText;
    }
    
    
}
