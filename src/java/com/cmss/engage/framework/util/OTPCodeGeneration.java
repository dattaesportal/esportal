// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 7/31/2009 9:41:20 PM
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ActivationCode.java

package com.cmss.engage.framework.util;

import com.engage.framework.util.log4j.CustomLogger;
import java.io.PrintStream;
import java.security.*;
import java.util.Random;
import org.hibernate.loader.custom.CustomLoader;

public class OTPCodeGeneration
{

    public OTPCodeGeneration()
    {
    }

    public static String genId()
    {
        String randomNum = "";
//        String randomAplhabets[] = {
//            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", 
//            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", 
//            "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", 
//            "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", 
//            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", 
//            "y", "z"
//        };
        String randomAplhabets[] = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"            
        };
        int lowerBound = 0;
        int upperBound = randomAplhabets.length-1;
        try
        {
            SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
            randomNum = (new Integer(prng.nextInt(0xF423F))).toString();
            //System.out.println(randomNum);
            int lenRandomNum = randomNum.length();
            int tmpLenRandomNum = 0;
            if(lenRandomNum < 6)
            {
                tmpLenRandomNum = 6 - lenRandomNum;
                for(int i = 0; i < tmpLenRandomNum; i++)
                {
                    int random = (int)((double)lowerBound + Math.random() * (double)(upperBound - lowerBound) + 0.5D);
                    randomNum = randomNum + randomAplhabets[random];
                }

            }
        }
        catch(NoSuchAlgorithmException ex)
        {
//            ex.printStackTrace();
            CustomLogger.error("OTPCodeGeneration", ex);
//            System.err.println(ex);
        }
        catch(Exception ex)
        {
//            ex.printStackTrace();
            CustomLogger.error("OTPCodeGeneration", ex);
//            System.err.println(ex);
        }
        return randomNum;
    }

    public String toHex(String randomId)
    {
        String hexrep = "";
        try
        {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte result[] = sha.digest(randomId.getBytes());
            for(int ii = 0; ii < result.length; ii++);
            hexrep = encode(result);
        }
        catch(NoSuchAlgorithmException nosuchalgorithmexception) { }
        return hexrep;
    }

    public String encode(byte data[])
    {
        int len = data.length;
        StringBuffer ret = new StringBuffer((len / 3 + 1) * 4);
        for(int i = 0; i < len; i++)
        {
            int c = data[i] >> 2 & 0x3f;
            ret.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(c));
            c = data[i] << 4 & 0x3f;
            if(++i < len)
                c |= data[i] >> 4 & 0xf;
            ret.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(c));
            if(i < len)
            {
                c = data[i] << 2 & 0x3f;
                if(++i < len)
                    c |= data[i] >> 6 & 3;
                ret.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(c));
            } else
            {
                i++;
                ret.append('=');
            }
            if(i < len)
            {
                c = data[i] & 0x3f;
                ret.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(c));
            } else
            {
                ret.append('=');
            }
        }

        return ret.toString();
    }
   public static String docNumber(){
       int randomNo = (int)(Math.random()*9000)+1000;
        String docNo = ""+randomNo;
       return docNo;
   }
    private final int fillchar = 61;
    private final String cvt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
}