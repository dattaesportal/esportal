
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clsUserLogDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clsUserLogDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="iUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StrsessionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LOCAL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMOTE_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMOTE_HOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMOTE_PORT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iModuleID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clsUserLogDetails", propOrder = {
    "iUserID",
    "strsessionID",
    "localaddr",
    "remoteaddr",
    "remotehost",
    "remoteport",
    "mode",
    "iModuleID"
})
public class ClsUserLogDetails {

    protected String iUserID;
    @XmlElement(name = "StrsessionID")
    protected String strsessionID;
    @XmlElement(name = "LOCAL_ADDR")
    protected String localaddr;
    @XmlElement(name = "REMOTE_ADDR")
    protected String remoteaddr;
    @XmlElement(name = "REMOTE_HOST")
    protected String remotehost;
    @XmlElement(name = "REMOTE_PORT")
    protected String remoteport;
    protected String mode;
    protected int iModuleID;

    /**
     * Gets the value of the iUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIUserID() {
        return iUserID;
    }

    /**
     * Sets the value of the iUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIUserID(String value) {
        this.iUserID = value;
    }

    /**
     * Gets the value of the strsessionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrsessionID() {
        return strsessionID;
    }

    /**
     * Sets the value of the strsessionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrsessionID(String value) {
        this.strsessionID = value;
    }

    /**
     * Gets the value of the localaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOCALADDR() {
        return localaddr;
    }

    /**
     * Sets the value of the localaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOCALADDR(String value) {
        this.localaddr = value;
    }

    /**
     * Gets the value of the remoteaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMOTEADDR() {
        return remoteaddr;
    }

    /**
     * Sets the value of the remoteaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMOTEADDR(String value) {
        this.remoteaddr = value;
    }

    /**
     * Gets the value of the remotehost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMOTEHOST() {
        return remotehost;
    }

    /**
     * Sets the value of the remotehost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMOTEHOST(String value) {
        this.remotehost = value;
    }

    /**
     * Gets the value of the remoteport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMOTEPORT() {
        return remoteport;
    }

    /**
     * Sets the value of the remoteport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMOTEPORT(String value) {
        this.remoteport = value;
    }

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMode(String value) {
        this.mode = value;
    }

    /**
     * Gets the value of the iModuleID property.
     * 
     */
    public int getIModuleID() {
        return iModuleID;
    }

    /**
     * Sets the value of the iModuleID property.
     * 
     */
    public void setIModuleID(int value) {
        this.iModuleID = value;
    }

}
