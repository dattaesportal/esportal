
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchBranchListResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchBranchListResult"
})
@XmlRootElement(name = "FetchBranchListResponse")
public class FetchBranchListResponse {

    @XmlElement(name = "FetchBranchListResult")
    protected String fetchBranchListResult;

    /**
     * Gets the value of the fetchBranchListResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFetchBranchListResult() {
        return fetchBranchListResult;
    }

    /**
     * Sets the value of the fetchBranchListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFetchBranchListResult(String value) {
        this.fetchBranchListResult = value;
    }

}
