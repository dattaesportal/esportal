
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://tempuri.org/", "string");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FetchBranchListResponse }
     * 
     */
    public FetchBranchListResponse createFetchBranchListResponse() {
        return new FetchBranchListResponse();
    }

    /**
     * Create an instance of {@link FetchBMSCompanyList }
     * 
     */
    public FetchBMSCompanyList createFetchBMSCompanyList() {
        return new FetchBMSCompanyList();
    }

    /**
     * Create an instance of {@link InsertUserLogDetailsOldResponse }
     * 
     */
    public InsertUserLogDetailsOldResponse createInsertUserLogDetailsOldResponse() {
        return new InsertUserLogDetailsOldResponse();
    }

    /**
     * Create an instance of {@link FetchMenuResponse }
     * 
     */
    public FetchMenuResponse createFetchMenuResponse() {
        return new FetchMenuResponse();
    }

    /**
     * Create an instance of {@link InsertUserLogDetails }
     * 
     */
    public InsertUserLogDetails createInsertUserLogDetails() {
        return new InsertUserLogDetails();
    }

    /**
     * Create an instance of {@link ClsUserLogDetails }
     * 
     */
    public ClsUserLogDetails createClsUserLogDetails() {
        return new ClsUserLogDetails();
    }

    /**
     * Create an instance of {@link DoLDAPAuthentication }
     * 
     */
    public DoLDAPAuthentication createDoLDAPAuthentication() {
        return new DoLDAPAuthentication();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     * 
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link UserActivityLogOnMenuClick }
     * 
     */
    public UserActivityLogOnMenuClick createUserActivityLogOnMenuClick() {
        return new UserActivityLogOnMenuClick();
    }

    /**
     * Create an instance of {@link InsertUserLogDetailsResponse }
     * 
     */
    public InsertUserLogDetailsResponse createInsertUserLogDetailsResponse() {
        return new InsertUserLogDetailsResponse();
    }

    /**
     * Create an instance of {@link SendEmail }
     * 
     */
    public SendEmail createSendEmail() {
        return new SendEmail();
    }

    /**
     * Create an instance of {@link Authorize }
     * 
     */
    public Authorize createAuthorize() {
        return new Authorize();
    }

    /**
     * Create an instance of {@link FetchBranchList }
     * 
     */
    public FetchBranchList createFetchBranchList() {
        return new FetchBranchList();
    }

    /**
     * Create an instance of {@link UserActivityLogOnMenuClickResponse }
     * 
     */
    public UserActivityLogOnMenuClickResponse createUserActivityLogOnMenuClickResponse() {
        return new UserActivityLogOnMenuClickResponse();
    }

    /**
     * Create an instance of {@link UserActivityLogResponse }
     * 
     */
    public UserActivityLogResponse createUserActivityLogResponse() {
        return new UserActivityLogResponse();
    }

    /**
     * Create an instance of {@link FetchBMSCompanyListResponse }
     * 
     */
    public FetchBMSCompanyListResponse createFetchBMSCompanyListResponse() {
        return new FetchBMSCompanyListResponse();
    }

    /**
     * Create an instance of {@link FetchMenu }
     * 
     */
    public FetchMenu createFetchMenu() {
        return new FetchMenu();
    }

    /**
     * Create an instance of {@link InsertUserLogDetailsOld }
     * 
     */
    public InsertUserLogDetailsOld createInsertUserLogDetailsOld() {
        return new InsertUserLogDetailsOld();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link DoLDAPAuthenticationResponse }
     * 
     */
    public DoLDAPAuthenticationResponse createDoLDAPAuthenticationResponse() {
        return new DoLDAPAuthenticationResponse();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link UserActivityLog }
     * 
     */
    public UserActivityLog createUserActivityLog() {
        return new UserActivityLog();
    }

    /**
     * Create an instance of {@link SendEmailResponse }
     * 
     */
    public SendEmailResponse createSendEmailResponse() {
        return new SendEmailResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

}
