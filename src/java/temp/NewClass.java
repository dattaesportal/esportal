/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package temp;

import com.engage.framework.util.log4j.CustomLogger;
import com.equifax.services.eport.servicedefs._1.CreditReportWSInquiryPortType;
import com.equifax.services.eport.servicedefs._1.V10;
import com.equifax.services.eport.ws.schemas._1.InquiryRequestType;
import com.equifax.services.eport.ws.schemas._1.InquiryResponseType;
import com.equifax.services.eport.ws.schemas._1.ReportFormatOptions;
import com.equifax.services.eport.ws.schemas._1.RequestBodyType;
import com.equifax.services.eport.ws.schemas._1.RequestHeaderType;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Normans Consultancy
 */
public class NewClass {
    public static void main(String a[]){
        
        RequestHeaderType header = new RequestHeaderType();
        header.setCustomerId(3);
        header.setUserId("STS_LTF");
        header.setPassword("Getrpt247");
        header.setMemberNumber("999BB00304");
        header.setSecurityCode("AA3");
        header.setProductCode("VID");
        header.setProductVersion("1.0");
        header.setReportFormat(ReportFormatOptions.XML);
        

        RequestBodyType requestBody = new RequestBodyType();
        requestBody.setInquiryPurpose("V1");
        requestBody.setFirstName("AMI");
        requestBody.setLastName("SHAH");
        requestBody.setPANId("AOSPS8417");
//         System.out.println("1");
        InquiryRequestType inquiryRequest = new InquiryRequestType();
        inquiryRequest.setRequestHeader(header);
        inquiryRequest.setRequestBody(requestBody);
//        System.out.println("2");
        V10 v10 = new V10();
//        System.out.println("2.0");
        CreditReportWSInquiryPortType creditReportWSInquiryPort = v10.getCreditReportWSInquiryPort();
        
//        System.out.println("2.1");
        try {
            java.io.StringWriter sw1 = new StringWriter();
            JAXBContext context1 = JAXBContext.newInstance(InquiryRequestType.class);
            Marshaller m1 = context1.createMarshaller();
            m1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m1.marshal(inquiryRequest, sw1);
//            System.out.println("request::"+sw1.toString());
        } catch (JAXBException ex) {
//           ex.printStackTrace();
            CustomLogger.error("NewClass", ex);
        }
        InquiryResponseType inquiryResponseType = creditReportWSInquiryPort.getConsumerCreditReport(inquiryRequest);
        try {
            java.io.StringWriter sw1 = new StringWriter();
            JAXBContext context1 = JAXBContext.newInstance(InquiryResponseType.class);
            Marshaller m1 = context1.createMarshaller();
            m1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m1.marshal(inquiryResponseType, sw1);
//            System.out.println("response::"+sw1.toString());
        } catch (JAXBException ex) {
           ex.printStackTrace();
           CustomLogger.error("NewClass", ex);
        }
//        System.out.println("3");
        
//        System.out.println("output :" + inquiryResponseType.getReportData().getVerifyIDResponse().getVidNsdlResponses().get(0).getNsdlResponse().getReturnCodeDesc());
//        System.out.println("Web Service Called !!");
    }
}
