<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<div style="margin: 10px;">
		<h3>Module Management</h3>
	</div>
	<c:if test="${not empty modules}">
		<table border="1">
			<tr>
				<td>Module Id</td>
				<td>Module Name</td>
				<td>Module Class</td>
				<td>Start</td>
				<td>Stop</td>
				<td>Status</td>
				<td>Active on startup</td>
			</tr>
			<c:forEach var="listValue" items="${modules}">
				<tr>
					<td>${listValue.routeId}</td>
					<td>${listValue.routeName}</td>
					<td>${listValue.routeClass}</td>
					<td><input type="button"
						onclick="location.href='startmodule.do?id=${listValue.routeId}'"
						value="Start"></td>
					<td><input type="button"
						onclick="location.href='stopmodule.do?id=${listValue.routeId}'"
						value="Stop"></td>
					<td></td>
					<td>${listValue.activeOnStartup}</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>


