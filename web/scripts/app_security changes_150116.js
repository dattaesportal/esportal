(function() {
    "use strict";
    angular.module("app.chart.ctrls", []).controller("chartCtrl", ["$scope", function($scope) {
            return $scope.easypiechart = {
                percent: 65,
                options: {
                    animate: {
                        duration: 1e3,
                        enabled: !0
                    },
                    barColor: "#31C0BE",
                    lineCap: "round",
                    size: 180,
                    lineWidth: 5
                }
            }, $scope.easypiechart2 = {
                percent: 35,
                options: {
                    animate: {
                        duration: 1e3,
                        enabled: !0
                    },
                    barColor: "#66B5D7",
                    lineCap: "round",
                    size: 180,
                    lineWidth: 10
                }
            }, $scope.easypiechart3 = {
                percent: 68,
                options: {
                    animate: {
                        duration: 1e3,
                        enabled: !0
                    },
                    barColor: "#60CD9B",
                    lineCap: "square",
                    size: 180,
                    lineWidth: 20,
                    scaleLength: 0
                }
            }, $scope.gaugeChart1 = {
                data: {
                    maxValue: 3e3,
                    animationSpeed: 40,
                    val: 1375
                },
                options: {
                    lines: 12,
                    angle: 0,
                    lineWidth: .47,
                    pointer: {
                        length: .6,
                        strokeWidth: .03,
                        color: "#000000"
                    },
                    limitMax: "false",
                    colorStart: "#A3C86D",
                    colorStop: "#A3C86D",
                    strokeColor: "#E0E0E0",
                    generateGradient: !0,
                    percentColors: [
                        [0, "#60CD9B"],
                        [1, "#60CD9B"]
                    ]
                }
            }, $scope.gaugeChart2 = {
                data: {
                    maxValue: 3e3,
                    animationSpeed: 45,
                    val: 1200
                },
                options: {
                    lines: 12,
                    angle: 0,
                    lineWidth: .47,
                    pointer: {
                        length: .6,
                        strokeWidth: .03,
                        color: "#464646"
                    },
                    limitMax: "true",
                    colorStart: "#7ACBEE",
                    colorStop: "#7ACBEE",
                    strokeColor: "#F1F1F1",
                    generateGradient: !0,
                    percentColors: [
                        [0, "#66B5D7"],
                        [1, "#66B5D7"]
                    ]
                }
            }, $scope.gaugeChart3 = {
                data: {
                    maxValue: 3e3,
                    animationSpeed: 50,
                    val: 1100
                },
                options: {
                    lines: 12,
                    angle: 0,
                    lineWidth: .47,
                    pointer: {
                        length: .6,
                        strokeWidth: .03,
                        color: "#464646"
                    },
                    limitMax: "true",
                    colorStart: "#FF7857",
                    colorStop: "#FF7857",
                    strokeColor: "#F1F1F1",
                    generateGradient: !0,
                    percentColors: [
                        [0, "#E87352"],
                        [1, "#E87352"]
                    ]
                }
            }
        }]).controller("morrisChartCtrl", ["$scope", function($scope) {
            return $scope.mainData = [{
                    month: "2013-01",
                    xbox: 294e3,
                    will: 136e3,
                    playstation: 244e3
                }, {
                    month: "2013-02",
                    xbox: 228e3,
                    will: 335e3,
                    playstation: 127e3
                }, {
                    month: "2013-03",
                    xbox: 199e3,
                    will: 159e3,
                    playstation: 13e4
                }, {
                    month: "2013-04",
                    xbox: 174e3,
                    will: 16e4,
                    playstation: 82e3
                }, {
                    month: "2013-05",
                    xbox: 255e3,
                    will: 318e3,
                    playstation: 82e3
                }, {
                    month: "2013-06",
                    xbox: 298400,
                    will: 401800,
                    playstation: 98600
                }, {
                    month: "2013-07",
                    xbox: 37e4,
                    will: 225e3,
                    playstation: 159e3
                }, {
                    month: "2013-08",
                    xbox: 376700,
                    will: 303600,
                    playstation: 13e4
                }, {
                    month: "2013-09",
                    xbox: 527800,
                    will: 301e3,
                    playstation: 119400
                }], $scope.simpleData = [{
                    year: "2008",
                    value: 20
                }, {
                    year: "2009",
                    value: 10
                }, {
                    year: "2010",
                    value: 5
                }, {
                    year: "2011",
                    value: 5
                }, {
                    year: "2012",
                    value: 20
                }, {
                    year: "2013",
                    value: 19
                }], $scope.comboData = [{
                    year: "2008",
                    a: 20,
                    b: 16,
                    c: 12
                }, {
                    year: "2009",
                    a: 10,
                    b: 22,
                    c: 30
                }, {
                    year: "2010",
                    a: 5,
                    b: 14,
                    c: 20
                }, {
                    year: "2011",
                    a: 5,
                    b: 12,
                    c: 19
                }, {
                    year: "2012",
                    a: 20,
                    b: 19,
                    c: 13
                }, {
                    year: "2013",
                    a: 28,
                    b: 22,
                    c: 20
                }], $scope.donutData = [{
                    label: "Download Sales",
                    value: 12
                }, {
                    label: "In-Store Sales",
                    value: 30
                }, {
                    label: "Mail-Order Sales",
                    value: 20
                }, {
                    label: "Online Sales",
                    value: 19
                }]
        }]).controller("flotChartCtrl", ["$scope", function($scope) {
            var areaChart, barChart, lineChart1;
            return lineChart1 = {}, lineChart1.data1 = [
                [1, 15],
                [2, 20],
                [3, 14],
                [4, 10],
                [5, 10],
                [6, 20],
                [7, 28],
                [8, 26],
                [9, 22],
                [10, 23],
                [11, 24]
            ], lineChart1.data2 = [
                [1, 9],
                [2, 15],
                [3, 17],
                [4, 21],
                [5, 16],
                [6, 15],
                [7, 13],
                [8, 15],
                [9, 29],
                [10, 21],
                [11, 29]
            ], $scope.line1 = {}, $scope.line1.data = [{
                    data: lineChart1.data1,
                    label: "Product A"
                }, {
                    data: lineChart1.data2,
                    label: "Product B",
                    lines: {
                        fill: !1
                    }
                }], $scope.line1.options = {
                series: {
                    lines: {
                        show: !0,
                        fill: !0,
                        fillColor: {
                            colors: [{
                                    opacity: 0
                                }, {
                                    opacity: .3
                                }]
                        }
                    },
                    points: {
                        show: !0,
                        lineWidth: 2,
                        fill: !0,
                        fillColor: "#ffffff",
                        symbol: "circle",
                        radius: 5
                    }
                },
                colors: ["#31C0BE", "#8170CA", "#E87352"],
                tooltip: !0,
                tooltipOpts: {
                    defaultTheme: !1
                },
                grid: {
                    hoverable: !0,
                    clickable: !0,
                    tickColor: "#f9f9f9",
                    borderWidth: 1,
                    borderColor: "#eeeeee"
                },
                xaxis: {
                    ticks: [
                        [1, "Jan."],
                        [2, "Feb."],
                        [3, "Mar."],
                        [4, "Apr."],
                        [5, "May"],
                        [6, "June"],
                        [7, "July"],
                        [8, "Aug."],
                        [9, "Sept."],
                        [10, "Oct."],
                        [11, "Nov."],
                        [12, "Dec."]
                    ]
                }
            }, areaChart = {}, areaChart.data1 = [
                [2007, 15],
                [2008, 20],
                [2009, 10],
                [2010, 5],
                [2011, 5],
                [2012, 20],
                [2013, 28]
            ], areaChart.data2 = [
                [2007, 15],
                [2008, 16],
                [2009, 22],
                [2010, 14],
                [2011, 12],
                [2012, 19],
                [2013, 22]
            ], $scope.area = {}, $scope.area.data = [{
                    data: areaChart.data1,
                    label: "Value A",
                    lines: {
                        fill: !0
                    }
                }, {
                    data: areaChart.data2,
                    label: "Value B",
                    points: {
                        show: !0
                    },
                    yaxis: 2
                }], $scope.area.options = {
                series: {
                    lines: {
                        show: !0,
                        fill: !1
                    },
                    points: {
                        show: !0,
                        lineWidth: 2,
                        fill: !0,
                        fillColor: "#ffffff",
                        symbol: "circle",
                        radius: 5
                    },
                    shadowSize: 0
                },
                grid: {
                    hoverable: !0,
                    clickable: !0,
                    tickColor: "#f9f9f9",
                    borderWidth: 1,
                    borderColor: "#eeeeee"
                },
                colors: ["#60CD9B", "#8170CA"],
                tooltip: !0,
                tooltipOpts: {
                    defaultTheme: !1
                },
                xaxis: {
                    mode: "time"
                },
                yaxes: [{}, {
                        position: "right"
                    }]
            }, barChart = {}, barChart.data1 = [
                [2008, 20],
                [2009, 10],
                [2010, 5],
                [2011, 5],
                [2012, 20],
                [2013, 28]
            ], barChart.data2 = [
                [2008, 16],
                [2009, 22],
                [2010, 14],
                [2011, 12],
                [2012, 19],
                [2013, 22]
            ], barChart.data3 = [
                [2008, 12],
                [2009, 30],
                [2010, 20],
                [2011, 19],
                [2012, 13],
                [2013, 20]
            ], $scope.barChart = {}, $scope.barChart.data = [{
                    label: "Value A",
                    data: barChart.data1
                }, {
                    label: "Value B",
                    data: barChart.data2
                }, {
                    label: "Value C",
                    data: barChart.data3
                }], $scope.barChart.options = {
                series: {
                    stack: !0,
                    bars: {
                        show: !0,
                        fill: 1,
                        barWidth: .3,
                        align: "center",
                        horizontal: !1,
                        order: 1
                    }
                },
                grid: {
                    hoverable: !0,
                    borderWidth: 1,
                    borderColor: "#eeeeee"
                },
                tooltip: !0,
                tooltipOpts: {
                    defaultTheme: !1
                },
                colors: ["#60CD9B", "#66B5D7", "#EEC95A", "#E87352"]
            }, $scope.pieChart = {}, $scope.pieChart.data = [{
                    label: "Download Sales",
                    data: 12
                }, {
                    label: "In-Store Sales",
                    data: 30
                }, {
                    label: "Mail-Order Sales",
                    data: 20
                }, {
                    label: "Online Sales",
                    data: 19
                }], $scope.pieChart.options = {
                series: {
                    pie: {
                        show: !0
                    }
                },
                legend: {
                    show: !0
                },
                grid: {
                    hoverable: !0,
                    clickable: !0
                },
                colors: ["#60CD9B", "#66B5D7", "#EEC95A", "#E87352"],
                tooltip: !0,
                tooltipOpts: {
                    content: "%p.0%, %s",
                    defaultTheme: !1
                }
            }, $scope.donutChart = {}, $scope.donutChart.data = [{
                    label: "Download Sales",
                    data: 12
                }, {
                    label: "In-Store Sales",
                    data: 30
                }, {
                    label: "Mail-Order Sales",
                    data: 20
                }, {
                    label: "Online Sales",
                    data: 19
                }], $scope.donutChart.options = {
                series: {
                    pie: {
                        show: !0,
                        innerRadius: .5
                    }
                },
                legend: {
                    show: !0
                },
                grid: {
                    hoverable: !0,
                    clickable: !0
                },
                colors: ["#60CD9B", "#66B5D7", "#EEC95A", "#E87352"],
                tooltip: !0,
                tooltipOpts: {
                    content: "%p.0%, %s",
                    defaultTheme: !1
                }
            }, $scope.donutChart2 = {}, $scope.donutChart2.data = [{
                    label: "Download Sales",
                    data: 12
                }, {
                    label: "In-Store Sales",
                    data: 30
                }, {
                    label: "Mail-Order Sales",
                    data: 20
                }, {
                    label: "Online Sales",
                    data: 19
                }, {
                    label: "Direct Sales",
                    data: 15
                }], $scope.donutChart2.options = {
                series: {
                    pie: {
                        show: !0,
                        innerRadius: .5
                    }
                },
                legend: {
                    show: !1
                },
                grid: {
                    hoverable: !0,
                    clickable: !0
                },
                colors: ["#1BB7A0", "#39B5B9", "#52A3BB", "#619CC4", "#6D90C5"],
                tooltip: !0,
                tooltipOpts: {
                    content: "%p.0%, %s",
                    defaultTheme: !1
                }
            }
        }]).controller("flotChartCtrl.realtime", ["$scope", function() {
        }]).controller("sparklineCtrl", ["$scope", function($scope) {
            return $scope.demoData1 = {
                data: [3, 1, 2, 2, 4, 6, 4, 5, 2, 4, 5, 3, 4, 6, 4, 7],
                options: {
                    type: "line",
                    lineColor: "#fff",
                    highlightLineColor: "#fff",
                    fillColor: "#60CD9B",
                    spotColor: !1,
                    minSpotColor: !1,
                    maxSpotColor: !1,
                    width: "100%",
                    height: "150px"
                }
            }, $scope.simpleChart1 = {
                data: [3, 1, 2, 3, 5, 3, 4, 2],
                options: {
                    type: "line",
                    lineColor: "#31C0BE",
                    fillColor: "#bce0df",
                    spotColor: !1,
                    minSpotColor: !1,
                    maxSpotColor: !1
                }
            }, $scope.simpleChart2 = {
                data: [3, 1, 2, 3, 5, 3, 4, 2],
                options: {
                    type: "bar",
                    barColor: "#31C0BE"
                }
            }, $scope.simpleChart3 = {
                data: [3, 1, 2, 3, 5, 3, 4, 2],
                options: {
                    type: "pie",
                    sliceColors: ["#31C0BE", "#60CD9B", "#E87352", "#8170CA", "#EEC95A", "#60CD9B"]
                }
            }, $scope.tristateChart1 = {
                data: [1, 2, -3, -5, 3, 1, -4, 2],
                options: {
                    type: "tristate",
                    posBarColor: "#95b75d",
                    negBarColor: "#fa8564"
                }
            }, $scope.largeChart1 = {
                data: [3, 1, 2, 3, 5, 3, 4, 2],
                options: {
                    type: "line",
                    lineColor: "#674E9E",
                    highlightLineColor: "#7ACBEE",
                    fillColor: "#927ED1",
                    spotColor: !1,
                    minSpotColor: !1,
                    maxSpotColor: !1,
                    width: "100%",
                    height: "150px"
                }
            }, $scope.largeChart2 = {
                data: [3, 1, 2, 3, 5, 3, 4, 2],
                options: {
                    type: "bar",
                    barColor: "#31C0BE",
                    barWidth: 10,
                    width: "100%",
                    height: "150px"
                }
            }, $scope.largeChart3 = {
                data: [3, 1, 2, 3, 5],
                options: {
                    type: "pie",
                    sliceColors: ["#31C0BE", "#60CD9B", "#E87352", "#8170CA", "#EEC95A", "#60CD9B"],
                    width: "150px",
                    height: "150px"
                }
            }
        }])
}).call(this),
        function() {
            "use strict";
            angular.module("app.chart.directives", []).directive("gaugeChart", [function() {
                    return {
                        restrict: "A",
                        scope: {
                            data: "=",
                            options: "="
                        },
                        link: function(scope, ele) {
                            var data, gauge, options;
                            return data = scope.data, options = scope.options, gauge = new Gauge(ele[0]).setOptions(options), gauge.maxValue = data.maxValue, gauge.animationSpeed = data.animationSpeed, gauge.set(data.val)
                        }
                    }
                }]).directive("flotChart", [function() {
                    return {
                        restrict: "A",
                        scope: {
                            data: "=",
                            options: "="
                        },
                        link: function(scope, ele) {
                            var data, options, plot;
                            return data = scope.data, options = scope.options, plot = $.plot(ele[0], data, options)
                        }
                    }
                }]).directive("flotChartRealtime", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            var data, getRandomData, plot, totalPoints, update, updateInterval;
                            return data = [], totalPoints = 300, getRandomData = function() {
                                var i, prev, res, y;
                                for (data.length > 0 && (data = data.slice(1)); data.length < totalPoints; )
                                    prev = data.length > 0 ? data[data.length - 1] : 50, y = prev + 10 * Math.random() - 5, 0 > y ? y = 0 : y > 100 && (y = 100), data.push(y);
                                for (res = [], i = 0; i < data.length; )
                                    res.push([i, data[i]]), ++i;
                                return res
                            }, update = function() {
                                plot.setData([getRandomData()]), plot.draw(), setTimeout(update, updateInterval)
                            }, data = [], totalPoints = 300, updateInterval = 200, plot = $.plot(ele[0], [getRandomData()], {
                                series: {
                                    lines: {
                                        show: !0,
                                        fill: !0
                                    },
                                    shadowSize: 0
                                },
                                yaxis: {
                                    min: 0,
                                    max: 100
                                },
                                xaxis: {
                                    show: !1
                                },
                                grid: {
                                    hoverable: !0,
                                    borderWidth: 1,
                                    borderColor: "#eeeeee"
                                },
                                colors: ["#5BDDDC"]
                            }), update()
                        }
                    }
                }]).directive("sparkline", [function() {
                    return {
                        restrict: "A",
                        scope: {
                            data: "=",
                            options: "="
                        },
                        link: function(scope, ele) {
                            var data, options, sparkResize, sparklineDraw;
                            return data = scope.data, options = scope.options, sparkResize = void 0, sparklineDraw = function() {
                                return ele.sparkline(data, options)
                            }, $(window).resize(function() {
                                return clearTimeout(sparkResize), sparkResize = setTimeout(sparklineDraw, 200)
                            }), sparklineDraw()
                        }
                    }
                }]).directive("morrisChart", [function() {
                    return {
                        restrict: "A",
                        scope: {
                            data: "="
                        },
                        link: function(scope, ele, attrs) {
                            var colors, data, func, options;
                            switch (data = scope.data, attrs.type) {
                                case "line":
                                    return colors = void 0 === attrs.lineColors || "" === attrs.lineColors ? null : JSON.parse(attrs.lineColors), options = {
                                        element: ele[0],
                                        data: data,
                                        xkey: attrs.xkey,
                                        ykeys: JSON.parse(attrs.ykeys),
                                        labels: JSON.parse(attrs.labels),
                                        lineWidth: attrs.lineWidth || 2,
                                        lineColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"],
                                        resize: !0
                                    }, new Morris.Line(options);
                                case "area":
                                    return colors = void 0 === attrs.lineColors || "" === attrs.lineColors ? null : JSON.parse(attrs.lineColors), options = {
                                        element: ele[0],
                                        data: data,
                                        xkey: attrs.xkey,
                                        ykeys: JSON.parse(attrs.ykeys),
                                        labels: JSON.parse(attrs.labels),
                                        lineWidth: attrs.lineWidth || 2,
                                        lineColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"],
                                        behaveLikeLine: attrs.behaveLikeLine || !1,
                                        fillOpacity: attrs.fillOpacity || "auto",
                                        pointSize: attrs.pointSize || 4,
                                        resize: !0
                                    }, new Morris.Area(options);
                                case "bar":
                                    return colors = void 0 === attrs.barColors || "" === attrs.barColors ? null : JSON.parse(attrs.barColors), options = {
                                        element: ele[0],
                                        data: data,
                                        xkey: attrs.xkey,
                                        ykeys: JSON.parse(attrs.ykeys),
                                        labels: JSON.parse(attrs.labels),
                                        barColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"],
                                        stacked: attrs.stacked || null,
                                        resize: !0
                                    }, new Morris.Bar(options);
                                case "donut":
                                    return colors = void 0 === attrs.colors || "" === attrs.colors ? null : JSON.parse(attrs.colors), options = {
                                        element: ele[0],
                                        data: data,
                                        colors: colors || ["#0B62A4", "#3980B5", "#679DC6", "#95BBD7", "#B0CCE1", "#095791", "#095085", "#083E67", "#052C48", "#042135"],
                                        resize: !0
                                    }, attrs.formatter && (func = new Function("y", "data", attrs.formatter), options.formatter = func), new Morris.Donut(options)
                            }
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.ui.form.ctrls", []).controller("TagsDemoCtrl", ["$scope", function($scope) {
                    return $scope.tags = ["foo", "bar"]
                }]).controller("DatepickerDemoCtrl", ["$scope", function($scope) {
                    return $scope.today = function() {
                        return $scope.dt = new Date
                    }, $scope.today(), $scope.showWeeks = !0, $scope.toggleWeeks = function() {
                        return $scope.showWeeks = !$scope.showWeeks
                    }, $scope.clear = function() {
                        return $scope.dt = null
                    }, $scope.disabled = function(date, mode) {
                        return "day" === mode && (0 === date.getDay() || 6 === date.getDay())
                    }, $scope.toggleMin = function() {
                        var _ref;
                        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                            "null": new Date
                        }
                    }, $scope.toggleMin(), $scope.open = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
                    }, $scope.dateOptions = {
                        "year-format": "'yy'",
                        "starting-day": 1
                    }, $scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "shortDate"], $scope.format = $scope.formats[0]
                }]).controller("TimepickerDemoCtrl", ["$scope", function($scope) {
                    return $scope.mytime = new Date, $scope.hstep = 1, $scope.mstep = 15, $scope.options = {
                        hstep: [1, 2, 3],
                        mstep: [1, 5, 10, 15, 25, 30]
                    }, $scope.ismeridian = !0, $scope.toggleMode = function() {
                        return $scope.ismeridian = !$scope.ismeridian
                    }, $scope.update = function() {
                        var d;
                        return d = new Date, d.setHours(14), d.setMinutes(0), $scope.mytime = d
                    }, $scope.changed = function() {
                        return console.log("Time changed to: " + $scope.mytime)
                    }, $scope.clear = function() {
                        return $scope.mytime = null
                    }
                }]).controller("TypeaheadCtrl", ["$scope", function($scope) {
                    return $scope.selected = void 0, $scope.states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
                }]).controller("RatingDemoCtrl", ["$scope", function($scope) {
                    return $scope.rate = 7, $scope.max = 10, $scope.isReadonly = !1, $scope.hoveringOver = function(value) {
                        return $scope.overStar = value, $scope.percent = 100 * (value / $scope.max)
                    }, $scope.ratingStates = [{
                            stateOn: "glyphicon-ok-sign",
                            stateOff: "glyphicon-ok-circle"
                        }, {
                            stateOn: "glyphicon-star",
                            stateOff: "glyphicon-star-empty"
                        }, {
                            stateOn: "glyphicon-heart",
                            stateOff: "glyphicon-ban-circle"
                        }, {
                            stateOn: "glyphicon-heart"
                        }, {
                            stateOff: "glyphicon-off"
                        }]
                }])
        }.call(this),
        function() {
            angular.module("app.ui.form.directives", []).directive("uiRangeSlider", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            return ele.slider()
                        }
                    }
                }]).directive("uiFileUpload", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            return ele.bootstrapFileInput()
                        }
                    }
                }]).directive("uiSpinner", [function() {
                    return {
                        restrict: "A",
                        compile: function(ele) {
                            return ele.addClass("ui-spinner"), {
                                post: function() {
                                    return ele.spinner()
                                }
                            }
                        }
                    }
                }]).directive("uiWizardForm", [function() {
                    return {
                        link: function(scope, ele) {
                            return ele.steps()
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.form.validation", []).controller("wizardFormCtrl", ["$scope", function($scope) {
                    return $scope.wizard = {
                        firstName: "some name",
                        lastName: "",
                        email: "",
                        password: "",
                        age: "",
                        address: ""
                    }, $scope.isValidateStep1 = function() {
                        return console.log($scope.wizard_step1), console.log("" !== $scope.wizard.firstName), console.log("" === $scope.wizard.lastName), console.log("" !== $scope.wizard.firstName && "" !== $scope.wizard.lastName)
                    }, $scope.finishedWizard = function() {
                        return console.log("yoo")
                    }
                }]).controller("formConstraintsCtrl", ["$scope", function($scope) {
                    var original;
                    return $scope.form = {
                        required: "",
                        minlength: "",
                        maxlength: "",
                        length_rage: "",
                        type_something: "",
                        confirm_type: "",
                        foo: "",
                        email: "",
                        url: "",
                        num: "",
                        minVal: "",
                        maxVal: "",
                        valRange: "",
                        pattern: ""
                    }, original = angular.copy($scope.form), $scope.revert = function() {
                        return $scope.form = angular.copy(original), $scope.form_constraints.$setPristine()
                    }, $scope.canRevert = function() {
                        return !angular.equals($scope.form, original) || !$scope.form_constraints.$pristine
                    }, $scope.canSubmit = function() {
                        return $scope.form_constraints.$valid && !angular.equals($scope.form, original)
                    }
                }]).controller("signinCtrl", ["$scope", function($scope) {
                    var original;
                    return $scope.user = {
                        email: "",
                        password: ""
                    }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user), $scope.revert = function() {
                        return $scope.user = angular.copy(original), $scope.form_signin.$setPristine()
                    }, $scope.canRevert = function() {
                        return !angular.equals($scope.user, original) || !$scope.form_signin.$pristine
                    }, $scope.canSubmit = function() {
                        return $scope.form_signin.$valid && !angular.equals($scope.user, original)
                    }, $scope.submitForm = function() {
                        return $scope.showInfoOnSubmit = !0, $scope.revert()
                    }
                }]).controller("signupCtrl", ["$scope", function($scope) {
                    var original;
                    return $scope.user = {
                        name: "",
                        email: "",
                        password: "",
                        confirmPassword: "",
                        age: ""
                    }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user), $scope.revert = function() {
                        return $scope.user = angular.copy(original), $scope.form_signup.$setPristine(), $scope.form_signup.confirmPassword.$setPristine()
                    }, $scope.canRevert = function() {
                        return !angular.equals($scope.user, original) || !$scope.form_signup.$pristine
                    }, $scope.canSubmit = function() {
                        return $scope.form_signup.$valid && !angular.equals($scope.user, original)
                    }, $scope.submitForm = function() {
                        return $scope.showInfoOnSubmit = !0, $scope.revert()
                    }
                }]).directive("validateEquals", [function() {
                    return {
                        require: "ngModel",
                        link: function(scope, ele, attrs, ngModelCtrl) {
                            var validateEqual;
                            return validateEqual = function(value) {
                                var valid;
                                return valid = value === scope.$eval(attrs.validateEquals), ngModelCtrl.$setValidity("equal", valid), "function" == typeof valid ? valid({
                                    value: void 0
                                }) : void 0
                            }, ngModelCtrl.$parsers.push(validateEqual), ngModelCtrl.$formatters.push(validateEqual), scope.$watch(attrs.validateEquals, function(newValue, oldValue) {
                                return newValue !== oldValue ? ngModelCtrl.$setViewValue(ngModelCtrl.$ViewValue) : void 0
                            })
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.map", []).directive("uiJqvmap", [function() {
                    return {
                        restrict: "A",
                        scope: {
                            options: "="
                        },
                        link: function(scope, ele) {
                            var options;
                            return options = scope.options, ele.vectorMap(options)
                        }
                    }
                }]).controller("jqvmapCtrl", ["$scope", function($scope) {
                    var sample_data;
                    return sample_data = {
                        af: "16.63",
                        al: "11.58",
                        dz: "158.97",
                        ao: "85.81",
                        ag: "1.1",
                        ar: "351.02",
                        am: "8.83",
                        au: "1219.72",
                        at: "366.26",
                        az: "52.17",
                        bs: "7.54",
                        bh: "21.73",
                        bd: "105.4",
                        bb: "3.96",
                        by: "52.89",
                        be: "461.33",
                        bz: "1.43",
                        bj: "6.49",
                        bt: "1.4",
                        bo: "19.18",
                        ba: "16.2",
                        bw: "12.5",
                        br: "2023.53",
                        bn: "11.96",
                        bg: "44.84",
                        bf: "8.67",
                        bi: "1.47",
                        kh: "11.36",
                        cm: "21.88",
                        ca: "1563.66",
                        cv: "1.57",
                        cf: "2.11",
                        td: "7.59",
                        cl: "199.18",
                        cn: "5745.13",
                        co: "283.11",
                        km: "0.56",
                        cd: "12.6",
                        cg: "11.88",
                        cr: "35.02",
                        ci: "22.38",
                        hr: "59.92",
                        cy: "22.75",
                        cz: "195.23",
                        dk: "304.56",
                        dj: "1.14",
                        dm: "0.38",
                        "do": "50.87",
                        ec: "61.49",
                        eg: "216.83",
                        sv: "21.8",
                        gq: "14.55",
                        er: "2.25",
                        ee: "19.22",
                        et: "30.94",
                        fj: "3.15",
                        fi: "231.98",
                        fr: "2555.44",
                        ga: "12.56",
                        gm: "1.04",
                        ge: "11.23",
                        de: "3305.9",
                        gh: "18.06",
                        gr: "305.01",
                        gd: "0.65",
                        gt: "40.77",
                        gn: "4.34",
                        gw: "0.83",
                        gy: "2.2",
                        ht: "6.5",
                        hn: "15.34",
                        hk: "226.49",
                        hu: "132.28",
                        is: "12.77",
                        "in": "1430.02",
                        id: "695.06",
                        ir: "337.9",
                        iq: "84.14",
                        ie: "204.14",
                        il: "201.25",
                        it: "2036.69",
                        jm: "13.74",
                        jp: "5390.9",
                        jo: "27.13",
                        kz: "129.76",
                        ke: "32.42",
                        ki: "0.15",
                        kr: "986.26",
                        undefined: "5.73",
                        kw: "117.32",
                        kg: "4.44",
                        la: "6.34",
                        lv: "23.39",
                        lb: "39.15",
                        ls: "1.8",
                        lr: "0.98",
                        ly: "77.91",
                        lt: "35.73",
                        lu: "52.43",
                        mk: "9.58",
                        mg: "8.33",
                        mw: "5.04",
                        my: "218.95",
                        mv: "1.43",
                        ml: "9.08",
                        mt: "7.8",
                        mr: "3.49",
                        mu: "9.43",
                        mx: "1004.04",
                        md: "5.36",
                        mn: "5.81",
                        me: "3.88",
                        ma: "91.7",
                        mz: "10.21",
                        mm: "35.65",
                        na: "11.45",
                        np: "15.11",
                        nl: "770.31",
                        nz: "138",
                        ni: "6.38",
                        ne: "5.6",
                        ng: "206.66",
                        no: "413.51",
                        om: "53.78",
                        pk: "174.79",
                        pa: "27.2",
                        pg: "8.81",
                        py: "17.17",
                        pe: "153.55",
                        ph: "189.06",
                        pl: "438.88",
                        pt: "223.7",
                        qa: "126.52",
                        ro: "158.39",
                        ru: "1476.91",
                        rw: "5.69",
                        ws: "0.55",
                        st: "0.19",
                        sa: "434.44",
                        sn: "12.66",
                        rs: "38.92",
                        sc: "0.92",
                        sl: "1.9",
                        sg: "217.38",
                        sk: "86.26",
                        si: "46.44",
                        sb: "0.67",
                        za: "354.41",
                        es: "1374.78",
                        lk: "48.24",
                        kn: "0.56",
                        lc: "1",
                        vc: "0.58",
                        sd: "65.93",
                        sr: "3.3",
                        sz: "3.17",
                        se: "444.59",
                        ch: "522.44",
                        sy: "59.63",
                        tw: "426.98",
                        tj: "5.58",
                        tz: "22.43",
                        th: "312.61",
                        tl: "0.62",
                        tg: "3.07",
                        to: "0.3",
                        tt: "21.2",
                        tn: "43.86",
                        tr: "729.05",
                        tm: 0,
                        ug: "17.12",
                        ua: "136.56",
                        ae: "239.65",
                        gb: "2258.57",
                        us: "14624.18",
                        uy: "40.71",
                        uz: "37.72",
                        vu: "0.72",
                        ve: "285.21",
                        vn: "101.99",
                        ye: "30.02",
                        zm: "15.69",
                        zw: "5.57"
                    }, $scope.worldMap = {
                        map: "world_en",
                        backgroundColor: null,
                        color: "#ffffff",
                        hoverOpacity: .7,
                        selectedColor: "#666666",
                        enableZoom: !0,
                        showTooltip: !0,
                        values: sample_data,
                        scaleColors: ["#C4FFFF", "#07C0BB"],
                        normalizeFunction: "polynomial"
                    }, $scope.USAMap = {
                        map: "usa_en",
                        backgroundColor: null,
                        color: "#ffffff",
                        hoverColor: "#999999",
                        selectedColor: "#666666",
                        enableZoom: !0,
                        showTooltip: !0,
                        selectedRegion: "MO"
                    }, $scope.europeMap = {
                        map: "europe_en",
                        backgroundColor: null,
                        color: "#ffffff",
                        hoverOpacity: .7,
                        hoverColor: "#999999",
                        enableZoom: !1,
                        showTooltip: !1,
                        values: sample_data,
                        scaleColors: ["#C4FFFF", "#07C0BB"],
                        normalizeFunction: "polynomial"
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.page.ctrls", []).controller("invoiceCtrl", ["$scope", "$window", function($scope) {
                    return $scope.printInvoice = function() {
                        var originalContents, popupWin, printContents;
                        return printContents = document.getElementById("invoice").innerHTML, originalContents = document.body.innerHTML, popupWin = window.open(), popupWin.document.open(), popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/main.css" /></head><body onload="window.print()">' + printContents + "</html>"), popupWin.document.close()
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.tables", []).controller("tableCtrl", ["$scope", "$filter", function($scope, $filter) {
                    var init;
                    return $scope.stores = [{
                            ApplicationNo: "TW15000001",
                            CustomerName: "Godwin Pinto",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000002",
                            CustomerName: "Ami Shah",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000003",
                            CustomerName: "Jagannath Kinikar",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000004",
                            CustomerName: "Sagar Sodha",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000005",
                            CustomerName: "Avinash Singh",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000006",
                            CustomerName: "Anuya Parsekar",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000007",
                            CustomerName: "Milan Tamang",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000008",
                            CustomerName: "Gaurav Mishra",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000009",
                            CustomerName: "Jayant Tiwari",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000010",
                            CustomerName: "Jay Prakash",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000011",
                            CustomerName: "Nitesh Joshi",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000012",
                            CustomerName: "Surya Balan",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000013",
                            CustomerName: "Surya Balan",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000014",
                            CustomerName: "Kisan Katekar",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000015",
                            CustomerName: "Ritesh Shah",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000016",
                            CustomerName: "Justin Dsouza",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000017",
                            CustomerName: "Anna Pillai",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000018",
                            CustomerName: "Parth Singh",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000019",
                            CustomerName: "Ashish Gupta",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000020",
                            CustomerName: "Pooja Raut",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000021",
                            CustomerName: "Mansi Satikar",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000022",
                            CustomerName: "Nirav Nandu",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000023",
                            CustomerName: "Ashok Shinde",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000024",
                            CustomerName: "sagar Rawal",
                            Product: "Two Wheeler",
                            ApplicationDate: "09/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }, {
                            ApplicationNo: "TW15000019",
                            CustomerName: "Pritam Rawal",
                            Product: "Two Wheeler",
                            ApplicationDate: "08/09/2015",
                            Status: "Saved",
                            SubmittedDate: "09/09/2015"
                        }], $scope.searchKeywords = "", $scope.filteredStores = [], $scope.row = "", $scope.select = function(page) {
                        var end, start;
                        return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
                    }, $scope.onFilterChange = function() {
                        return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
                    }, $scope.onNumPerPageChange = function() {
                        return $scope.select(1), $scope.currentPage = 1
                    }, $scope.onOrderChange = function() {
                        return $scope.select(1), $scope.currentPage = 1
                    }, $scope.search = function() {
                        return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
                    }, $scope.order = function(rowName) {
                        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
                    }, $scope.numPerPageOpt = [5, 10, 15, 30, 50, 100], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageStores = [], (init = function() {
                        return $scope.search(), $scope.select($scope.currentPage)
                    })()
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.task", []).factory("taskStorage", function() {
                var DEMO_TASKS, STORAGE_ID;
                return STORAGE_ID = "tasks", DEMO_TASKS = '[ {"title": "Finish homework", "completed": true}, {"title": "Make a call", "completed": true}, {"title": "Build a snowman!", "completed": false}, {"title": "Tango! Tango! Tango!", "completed": false}, {"title": "Play games with friends", "completed": false}, {"title": "Shopping", "completed": false} ]', {
                    get: function() {
                        return JSON.parse(localStorage.getItem(STORAGE_ID) || DEMO_TASKS)
                    },
                    put: function(tasks) {
                        return localStorage.setItem(STORAGE_ID, JSON.stringify(tasks))
                    }
                }
            }).directive("taskFocus", ["$timeout", function($timeout) {
                    return {
                        link: function(scope, ele, attrs) {
                            return scope.$watch(attrs.taskFocus, function(newVal) {
                                return newVal ? $timeout(function() {
                                    return ele[0].focus()
                                }, 0, !1) : void 0
                            })
                        }
                    }
                }]).controller("taskCtrl", ["$scope", "taskStorage", "filterFilter", "$rootScope", "logger", function($scope, taskStorage, filterFilter, $rootScope, logger) {
                    var tasks;
                    return tasks = $scope.tasks = taskStorage.get(), $scope.newTask = "", $scope.remainingCount = filterFilter(tasks, {
                        completed: !1
                    }).length, $scope.editedTask = null, $scope.statusFilter = {
                        completed: !1
                    }, $scope.filter = function(filter) {
                        switch (filter) {
                            case "all":
                                return $scope.statusFilter = "";
                            case "active":
                                return $scope.statusFilter = {
                                    completed: !1
                                };
                            case "completed":
                                return $scope.statusFilter = {
                                    completed: !0
                                }
                        }
                    }, $scope.add = function() {
                        var newTask;
                        return newTask = $scope.newTask.trim(), 0 !== newTask.length ? (tasks.push({
                            title: newTask,
                            completed: !1
                        }), logger.logSuccess('New task: "' + newTask + '" added'), taskStorage.put(tasks), $scope.newTask = "", $scope.remainingCount++) : void 0
                    }, $scope.edit = function(task) {
                        return $scope.editedTask = task
                    }, $scope.doneEditing = function(task) {
                        return $scope.editedTask = null, task.title = task.title.trim(), task.title ? logger.log("Task updated") : $scope.remove(task), taskStorage.put(tasks)
                    }, $scope.remove = function(task) {
                        var index;
                        return $scope.remainingCount -= task.completed ? 0 : 1, index = $scope.tasks.indexOf(task), $scope.tasks.splice(index, 1), taskStorage.put(tasks), logger.logError("Task removed")
                    }, $scope.completed = function(task) {
                        return $scope.remainingCount += task.completed ? -1 : 1, taskStorage.put(tasks), task.completed ? $scope.remainingCount > 0 ? logger.log(1 === $scope.remainingCount ? "Almost there! Only " + $scope.remainingCount + " task left" : "Good job! Only " + $scope.remainingCount + " tasks left") : logger.logSuccess("Congrats! All done :)") : void 0
                    }, $scope.clearCompleted = function() {
                        return $scope.tasks = tasks = tasks.filter(function(val) {
                            return !val.completed
                        }), taskStorage.put(tasks)
                    }, $scope.markAll = function(completed) {
                        return tasks.forEach(function(task) {
                            return task.completed = completed
                        }), $scope.remainingCount = completed ? 0 : tasks.length, taskStorage.put(tasks), completed ? logger.logSuccess("Congrats! All done :)") : void 0
                    }, $scope.$watch("remainingCount == 0", function(val) {
                        return $scope.allChecked = val
                    }), $scope.$watch("remainingCount", function(newVal) {
                        return $rootScope.$broadcast("taskRemaining:changed", newVal)
                    })
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.ui.ctrls", []).controller("NotifyCtrl", ["$scope", "logger", function($scope, logger) {
                    return $scope.notify = function(type) {
                        switch (type) {
                            case "info":
                                return logger.log("Heads up! This alert needs your attention, but it's not super important.");
                            case "success":
                                return logger.logSuccess("Well done! You successfully read this important alert message.");
                            case "warning":
                                return logger.logWarning("Warning! Best check yo self, you're not looking too good.");
                            case "error":
                                return logger.logError("Oh snap! Change a few things up and try submitting again.")
                        }
                    }
                }]).controller("AlertDemoCtrl", ["$scope", function($scope) {
                    return $scope.alerts = [{
                            type: "success",
                            msg: "Well done! You successfully read this important alert message."
                        }, {
                            type: "info",
                            msg: "Heads up! This alert needs your attention, but it is not super important."
                        }, {
                            type: "warning",
                            msg: "Warning! Best check yo self, you're not looking too good."
                        }, {
                            type: "danger",
                            msg: "Oh snap! Change a few things up and try submitting again."
                        }], $scope.addAlert = function() {
                        var num, type;
                        switch (num = Math.ceil(4 * Math.random()), type = void 0, num) {
                            case 0:
                                type = "info";
                                break;
                            case 1:
                                type = "success";
                                break;
                            case 2:
                                type = "info";
                                break;
                            case 3:
                                type = "warning";
                                break;
                            case 4:
                                type = "danger"
                        }
                        return $scope.alerts.push({
                            type: type,
                            msg: "Another alert!"
                        })
                    }, $scope.closeAlert = function(index) {
                        return $scope.alerts.splice(index, 1)
                    }
                }]).controller("ProgressDemoCtrl", ["$scope", function($scope) {
                    return $scope.max = 200, $scope.random = function() {
                        var type, value;
                        value = Math.floor(100 * Math.random() + 10), type = void 0, type = 25 > value ? "success" : 50 > value ? "info" : 75 > value ? "warning" : "danger", $scope.showWarning = "danger" === type || "warning" === type, $scope.dynamic = value, $scope.type = type
                    }, $scope.random()
                }]).controller("AccordionDemoCtrl", ["$scope", function($scope) {
                    $scope.oneAtATime = !0, $scope.groups = [{
                            title: "Dynamic Group Header - 1",
                            content: "Dynamic Group Body - 1"
                        }, {
                            title: "Dynamic Group Header - 2",
                            content: "Dynamic Group Body - 2"
                        }, {
                            title: "Dynamic Group Header - 3",
                            content: "Dynamic Group Body - 3"
                        }], $scope.items = ["Item 1", "Item 2", "Item 3"], $scope.addItem = function() {
                        var newItemNo;
                        newItemNo = $scope.items.length + 1, $scope.items.push("Item " + newItemNo)
                    }
                }]).controller("CollapseDemoCtrl", ["$scope", function($scope) {
                    return $scope.isCollapsed = !1
                }]).controller("ModalDemoCtrl", ["$scope", "$modal", "$log", function($scope, $modal, $log) {
                    $scope.items = ["item1", "item2", "item3"], $scope.open = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "myModalContent.html",
                            controller: "ModalInstanceCtrl",
                            resolve: {
                                items: function() {
                                    return $scope.items
                                }
                            }
                        }), modalInstance.result.then(function(selectedItem) {
                            $scope.selected = selectedItem
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        })
                    }
                }]).controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "items", function($scope, $modalInstance, items) {
                    $scope.items = items, $scope.selected = {
                        item: $scope.items[0]
                    }, $scope.ok = function() {
                        $modalInstance.close($scope.selected.item)
                    }, $scope.cancel = function() {
                        $modalInstance.dismiss("cancel")
                    }
                }]).controller("PaginationDemoCtrl", ["$scope", function($scope) {
                    return $scope.totalItems = 64, $scope.currentPage = 4, $scope.maxSize = 5, $scope.setPage = function(pageNo) {
                        return $scope.currentPage = pageNo
                    }, $scope.bigTotalItems = 175, $scope.bigCurrentPage = 1
                }]).controller("TabsDemoCtrl", ["$scope", function($scope) {
                    return $scope.tabs = [{
                            title: "Dynamic Title 1",
                            content: "Dynamic content 1.  Consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at."
                        }, {
                            title: "Disabled",
                            content: "Dynamic content 2.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at.",
                            disabled: !0
                        }], $scope.navType = "pills"
                }]).controller("TreeDemoCtrl", ["$scope", function($scope) {
                    return $scope.list = [{
                            id: 1,
                            title: "Item 1",
                            items: []
                        }, {
                            id: 2,
                            title: "Item 2",
                            items: [{
                                    id: 21,
                                    title: "Item 2.1",
                                    items: [{
                                            id: 211,
                                            title: "Item 2.1.1",
                                            items: []
                                        }, {
                                            id: 212,
                                            title: "Item 2.1.2",
                                            items: []
                                        }]
                                }, {
                                    id: 22,
                                    title: "Item 2.2",
                                    items: [{
                                            id: 221,
                                            title: "Item 2.2.1",
                                            items: []
                                        }, {
                                            id: 222,
                                            title: "Item 2.2.2",
                                            items: []
                                        }]
                                }]
                        }, {
                            id: 3,
                            title: "Item 3",
                            items: []
                        }, {
                            id: 4,
                            title: "Item 4",
                            items: [{
                                    id: 41,
                                    title: "Item 4.1",
                                    items: []
                                }]
                        }, {
                            id: 5,
                            title: "Item 5",
                            items: []
                        }, {
                            id: 6,
                            title: "Item 6",
                            items: []
                        }, {
                            id: 7,
                            title: "Item 7",
                            items: []
                        }], $scope.selectedItem = {}, $scope.options = {}, $scope.remove = function(scope) {
                        scope.remove()
                    }, $scope.toggle = function(scope) {
                        scope.toggle()
                    }, $scope.newSubItem = function(scope) {
                        var nodeData;
                        nodeData = scope.$modelValue, nodeData.items.push({
                            id: 10 * nodeData.id + nodeData.items.length,
                            title: nodeData.title + "." + (nodeData.items.length + 1),
                            items: []
                        })
                    }
                }]).controller("MapDemoCtrl", ["$scope", "$http", "$interval", function($scope, $http, $interval) {
                    var i, markers;
                    for (markers = [], i = 0; 8 > i; )
                        markers[i] = new google.maps.Marker({
                            title: "Marker: " + i
                        }), i++;
                    $scope.GenerateMapMarkers = function() {
                        var d, lat, lng, loc, numMarkers;
                        for (d = new Date, $scope.date = d.toLocaleString(), numMarkers = Math.floor(4 * Math.random()) + 4, i = 0; numMarkers > i; )
                            lat = 43.66 + Math.random() / 100, lng = -79.4103 + Math.random() / 100, loc = new google.maps.LatLng(lat, lng), markers[i].setPosition(loc), markers[i].setMap($scope.map), i++
                    }, $interval($scope.GenerateMapMarkers, 2e3)
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.ui.directives", []).directive("uiTime", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            var checkTime, startTime;
                            return startTime = function() {
                                var h, m, s, t, time, today;
                                return today = new Date, h = today.getHours(), m = today.getMinutes(), s = today.getSeconds(), m = checkTime(m), s = checkTime(s), time = h + ":" + m + ":" + s, ele.html(time), t = setTimeout(startTime, 500)
                            }, checkTime = function(i) {
                                return 10 > i && (i = "0" + i), i
                            }, startTime()
                        }
                    }
                }]).directive("uiWeather", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele, attrs) {
                            var color, icon, skycons;
                            return color = attrs.color, icon = Skycons[attrs.icon], skycons = new Skycons({
                                color: color,
                                resizeClear: !0
                            }), skycons.add(ele[0], icon), skycons.play()
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.ui.services", []).factory("logger", [function() {
                    var logIt;
                    return toastr.options = {
                        closeButton: !0,
                        positionClass: "toast-bottom-right",
                        timeOut: "3000"
                    }, logIt = function(message, type) {
                        return toastr[type](message)
                    }, {
                        log: function(message) {
                            logIt(message, "info")
                        },
                        logWarning: function(message) {
                            logIt(message, "warning")
                        },
                        logSuccess: function(message) {
                            logIt(message, "success")
                        },
                        logError: function(message) {
                            logIt(message, "error")
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app", ["ngFileUpload", "ngCookies", "ngRoute", "ngAnimate", "ui.bootstrap", "easypiechart", "mgo-angular-wizard", "textAngular", "ui.tree", "ngMap", "ngTagsInput", "app.ui.ctrls", "app.ui.directives", "app.ui.services", "app.controllers", "app.directives", "app.form.validation", "app.ui.form.ctrls", "app.ui.form.directives", "app.tables", "app.map", "app.task", "app.localization", "app.chart.ctrls", "app.chart.directives", "app.page.ctrls"])
                    .config(["$routeProvider", function($routeProvider) {
                            return $routeProvider.when("/", {
                                redirectTo: "pages/signin"
                            }).when("/presanctiondashboard", {
                                templateUrl: "views/presanctiondashboard.html"
                            }).when("/preDashboard", {
                                templateUrl: "views/preDashboard.html"
                            }).when("/lpc", {
                                templateUrl: "views/lpcDashboard.html"
                            }).when("/lpcApplication", {
                                templateUrl: "views/sangam/lpc/lpc.html"
                            }).when("/ui/typography", {
                                templateUrl: "views/ui/typography.html"
                            }).when("/ui/buttons", {
                                templateUrl: "views/ui/buttons.html"
                            }).when("/ui/icons", {
                                templateUrl: "views/ui/icons.html"
                            }).when("/ui/grids", {
                                templateUrl: "views/ui/grids.html"
                            }).when("/ui/widgets", {
                                templateUrl: "views/ui/widgets.html"
                            }).when("/ui/components", {
                                templateUrl: "views/ui/components.html"
                            }).when("/ui/timeline", {
                                templateUrl: "views/ui/timeline.html"
                            }).when("/ui/nested-lists", {
                                templateUrl: "views/ui/nested-lists.html"
                            }).when("/ui/pricing-tables", {
                                templateUrl: "views/ui/pricing-tables.html"
                            }).when("/forms/elements", {
                                templateUrl: "views/forms/elements.html"
                            }).when("/forms/layouts", {
                                templateUrl: "views/forms/layouts.html"
                            }).when("/forms/validation", {
                                templateUrl: "views/forms/validation.html"
                            }).when("/forms/wizard", {
                                templateUrl: "views/forms/wizard.html"
                            }).when("/maps/gmap", {
                                templateUrl: "views/maps/gmap.html"
                            }).when("/maps/jqvmap", {
                                templateUrl: "views/maps/jqvmap.html"
                            }).when("/tables/static", {
                                templateUrl: "views/tables/static.html"
                            }).when("/tables/responsive", {
                                templateUrl: "views/tables/responsive.html"
                            }).when("/tables/dynamic", {
                                templateUrl: "views/tables/dynamic.html"
                            }).when("/charts/others", {
                                templateUrl: "views/charts/charts.html"
                            }).when("/charts/morris", {
                                templateUrl: "views/charts/morris.html"
                            }).when("/charts/flot", {
                                templateUrl: "views/charts/flot.html"
                            }).when("/mail/inbox", {
                                templateUrl: "views/mail/inbox.html"
                            }).when("/mail/compose", {
                                templateUrl: "views/mail/compose.html"
                            }).when("/mail/single", {
                                templateUrl: "views/mail/single.html"
                            }).when("/pages/features", {
                                templateUrl: "views/pages/features.html"
                            }).when("/pages/signin", {
                                templateUrl: "views/pages/signin.html"
                            }).when("/pages/signup", {
                                templateUrl: "views/pages/signup.html"
                            }).when("/pages/forgot", {
                                templateUrl: "views/pages/forgot-password.html"
                            }).when("/pages/lock-screen", {
                                templateUrl: "views/pages/lock-screen.html"
                            }).when("/pages/profile", {
                                templateUrl: "views/pages/profile.html"
                            }).when("/404", {
                                templateUrl: "views/pages/404.html"
                            }).when("/pages/500", {
                                templateUrl: "views/pages/500.html"
                            }).when("/pages/blank", {
                                templateUrl: "views/pages/blank.html"
                            }).when("/pages/invoice", {
                                templateUrl: "views/pages/invoice.html"
                            }).when("/pages/services", {
                                templateUrl: "views/pages/services.html"
                            }).when("/pages/about", {
                                templateUrl: "views/pages/about.html"
                            }).when("/pages/contact", {
                                templateUrl: "views/pages/contact.html"
                            }).when("/tasks", {
                                templateUrl: "views/tasks/tasks.html"
                            }).when("/new_application", {
                                templateUrl: "views/sangam/new_application.html"
                            }).when("/pre_sancton", {
                                templateUrl: "views/sangam/new_application.html"

                            }).when("/postSanction", {
                                templateUrl: "views/sangam/postsanction.html"
                            }).when("/postsanctiondashboard", {
                                templateUrl: "views/postsanctiondashboard.html"
                            }).when("/presanction/sourcing", {
                                templateUrl: "views/sangam/presanction/sourcing.html"
                            }).when("/presanction/applicantdetail", {
                                templateUrl: "views/sangam/presanction/applicantdetail.html"
                            }).when("/presanction/addressdetail", {
                                templateUrl: "views/sangam/presanction/addressdetail.html"
                            }).when("/presanction/occupationincome", {
                                templateUrl: "views/sangam/presanction/occupationincome.html"
                            }).when("/presanction/bankdetail", {
                                templateUrl: "views/sangam/presanction/bankdetail.html"
                            }).when("/presanction/assetdetail", {
                                templateUrl: "views/sangam/presanction/assetdetail.html"
                            }).when("/presanction/loandetail", {
                                templateUrl: "views/sangam/presanction/loandetail.html"
                            }).when("/presanction/document", {
                                templateUrl: "views/sangam/presanction/document.html"


                            }).when("/postsanction/sourcing", {
                                templateUrl: "views/sangam/postsanction/sourcing.html"
                            }).when("/postsanction/bankdetail", {
                                templateUrl: "views/sangam/postsanction/bankdetail.html"
                            }).when("/postsanction/disbursementdetail", {
                                templateUrl: "views/sangam/postsanction/disbursementdetail.html"
                            }).when("/postsanction/loandetail", {
                                templateUrl: "views/sangam/postsanction/loandetail.html"
                            }).when("/postsanction/insurancedetail", {
                                templateUrl: "views/sangam/postsanction/insurancedetail.html"
                            }).when("/postsanction/assetinsurance", {
                                templateUrl: "views/sangam/postsanction/assetinsurance.html"
                            }).when("/postsanction/referencedetail", {
                                templateUrl: "views/sangam/postsanction/referencedetail.html"
                            }).when("/postsanction/instrumentdetail", {
                                templateUrl: "views/sangam/postsanction/instrumentdetail.html"
                            }).when("/postsanction/assetdetail", {
                                templateUrl: "views/sangam/postsanction/assetdetail.html"
                            }).when("/postsanction/charges", {
                                templateUrl: "views/sangam/postsanction/charges.html"
                             }).when("/postsanction/documentdetail", {
                                templateUrl: "views/sangam/postsanction/document.html"


                            }).when("/lpcdashboard", {
                                templateUrl: "views/lpcdashboard.html"
                            }).when("/lpc/lpc", {
                                templateUrl: "views/sangam/lpc/lpc.html"
                            }).otherwise({
                                redirectTo: "/404"
                            })
                        }]).run(function($rootScope, $location, $window, $cookies) {

// $cookies.c = 0;
                $rootScope.isDisabledPreSanction = false;
                $rootScope.userId = $cookies.i ? $cookies.i : "";
                $rootScope.userName = $cookies.n ? $cookies.n : "";
                $rootScope.lastLoginTime = $cookies.l ? $cookies.l : "";
                $rootScope.profileName = $cookies.r ? $cookies.r : "";
                $rootScope.loginEntity = $cookies.e ? $cookies.e : "0";
                $rootScope.profileId = $cookies.p ? $cookies.p : "";
                $rootScope.applicationId = $cookies.a ? $cookies.a : 0;
                $rootScope.progress = $cookies.c ? $cookies.c : 0;
                if ($rootScope.profileId == 1) {
                    $rootScope.isOtpAllowed = true;
                    $rootScope.isGenAppId = true;
                }
                if ($rootScope.profileId == 2) {

                }
                $cookies.t = Date.now();
                $rootScope.$on('$routeChangeSuccess', function(ev, next, curr) {
                    if (next.$$route.originalPath == "/new_application") {
                        // $rootScope.setCookies(0,0);
                        // crear

                    }

                });
            })
        }.call(this),
        function() {
            angular.module("app.directives", []).directive("imgHolder", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            return Holder.run({
                                images: ele[0]
                            })
                        }
                    }
                }]).directive("customBackground", function() {
                return {
                    restrict: "A",
                    controller: ["$scope", "$element", "$location", function($scope, $element, $location) {
                            var addBg, path;
                            return path = function() {
                                return $location.path()
                            }, addBg = function(path) {
                                switch ($element.removeClass("body-home body-special body-tasks body-lock"), path) {
                                    case "/":
                                        return $element.addClass("body-home");
                                    case "/404":
                                    case "/pages/500":
                                    case "/pages/signin":
                                    case "/pages/signup":
                                    case "/pages/forgot":
                                        return $element.addClass("body-special");
                                    case "/pages/lock-screen":
                                        return $element.addClass("body-special body-lock");
                                    case "/tasks":
                                        return $element.addClass("body-tasks")
                                }
                            }, addBg($location.path()), $scope.$watch(path, function(newVal, oldVal) {
                                return newVal !== oldVal ? addBg($location.path()) : void 0
                            })
                        }]
                }
            }).directive("uiColorSwitch", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            return ele.find(".color-option").on("click", function(event) {
                                var $this, hrefUrl, style;
                                if ($this = $(this), hrefUrl = void 0, style = $this.data("style"), "loulou" === style)
                                    hrefUrl = "styles/main.css", $('link[href^="styles/main"]').attr("href", hrefUrl);
                                else {
                                    if (!style)
                                        return !1;
                                    style = "-" + style, hrefUrl = "styles/main" + style + ".css", $('link[href^="styles/main"]').attr("href", hrefUrl)
                                }
                                return event.preventDefault()
                            })
                        }
                    }
                }]).directive("toggleMinNav", ["$rootScope", function($rootScope) {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            var $content, $nav, $window, Timer, app, updateClass;
                            return app = $("#app"), $window = $(window), $nav = $("#nav-container"), $content = $("#content"), ele.on("click", function(e) {
                                return app.hasClass("nav-min") ? app.removeClass("nav-min") : (app.addClass("nav-min"), $rootScope.$broadcast("minNav:enabled")), e.preventDefault()
                            }), Timer = void 0, updateClass = function() {
                                var width;
                                return width = $window.width(), 768 > width ? app.removeClass("nav-min") : void 0
                            }, $window.resize(function() {
                                var t;
                                return clearTimeout(t), t = setTimeout(updateClass, 300)
                            })
                        }
                    }
                }]).directive("collapseNav", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            var $a, $aRest, $lists, $listsRest, app;
                            return $lists = ele.find("ul").parent("li"), $lists.append('<i class="fa fa-caret-right icon-has-ul"></i>'), $a = $lists.children("a"), $listsRest = ele.children("li").not($lists), $aRest = $listsRest.children("a"), app = $("#app"), $a.on("click", function(event) {
                                var $parent, $this;
                                return app.hasClass("nav-min") ? !1 : ($this = $(this), $parent = $this.parent("li"), $lists.not($parent).removeClass("open").find("ul").slideUp(), $parent.toggleClass("open").find("ul").slideToggle(), $parent.children("i").removeClass("fa fa-caret-right"), $parent.children("i").toggleClass("fa fa-caret-down"), event.preventDefault())
                            }), $aRest.on("click", function() {
                                return $lists.removeClass("open").find("ul").slideUp()
                            }), scope.$on("minNav:enabled", function() {
                                return $lists.removeClass("open").find("ul").slideUp()
                            })
                        }
                    }
                }]).directive("highlightActive", [function() {
                    return {
                        restrict: "A",
                        controller: ["$scope", "$element", "$attrs", "$location", function($scope, $element, $attrs, $location) {
                                var highlightActive, links, path;
                                return links = $element.find("a"), path = function() {
                                    return $location.path()
                                }, highlightActive = function(links, path) {
                                    return path = "#" + path, angular.forEach(links, function(link) {
                                        var $li, $link, href;
                                        return $link = angular.element(link), $li = $link.parent("li"), href = $link.attr("href"), $li.hasClass("active") && $li.removeClass("active"), 0 === path.indexOf(href) ? $li.addClass("active") : void 0
                                    })
                                }, highlightActive(links, $location.path()), $scope.$watch(path, function(newVal, oldVal) {
                                    return newVal !== oldVal ? highlightActive(links, $location.path()) : void 0
                                })
                            }]
                    }
                }]).directive("toggleOffCanvas", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele) {
                            return ele.on("click", function() {
                                return $("#app").toggleClass("on-canvas")
                            })
                        }
                    }
                }]).directive("slimScroll", [function() {
                    return {
                        restrict: "A",
                        link: function(scope, ele, attrs) {
                            return ele.slimScroll({
                                height: attrs.scrollHeight || "100%"
                            })
                        }
                    }
                }]).directive("goBack", [function() {
                    return {
                        restrict: "A",
                        controller: ["$scope", "$element", "$window", function($scope, $element, $window) {
                                return $element.on("click", function() {
                                    return $window.history.back()
                                })
                            }]
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.localization", []).factory("localize", ["$http", "$rootScope", "$window", function($http, $rootScope, $window) {
                    var localize;
                    return localize = {
                        language: "",
                        url: void 0,
                        resourceFileLoaded: !1,
                        successCallback: function(data) {
                            return localize.dictionary = data, localize.resourceFileLoaded = !0, $rootScope.$broadcast("localizeResourcesUpdated")
                        },
                        setLanguage: function(value) {
                            return localize.language = value.toLowerCase().split("-")[0], localize.initLocalizedResources()
                        },
                        setUrl: function(value) {
                            return localize.url = value, localize.initLocalizedResources()
                        },
                        buildUrl: function() {
                            return localize.language || (localize.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase(), localize.language = localize.language.split("-")[0]), "i18n/resources-locale_" + localize.language + ".js"
                        },
                        initLocalizedResources: function() {
                            var url;
                            return url = localize.url || localize.buildUrl(), $http({
                                method: "GET",
                                url: url,
                                cache: !1
                            }).success(localize.successCallback).error(function() {
                                return $rootScope.$broadcast("localizeResourcesUpdated")
                            })
                        },
                        getLocalizedString: function(value) {
                            var result, valueLowerCase;
                            return result = void 0, localize.dictionary && value ? (valueLowerCase = value.toLowerCase(), result = "" === localize.dictionary[valueLowerCase] ? value : localize.dictionary[valueLowerCase]) : result = value, result
                        }
                    }
                }]).directive("i18n", ["localize", function(localize) {
                    var i18nDirective;
                    return i18nDirective = {
                        restrict: "EA",
                        updateText: function(ele, input, placeholder) {
                            var result;
                            return result = void 0, "i18n-placeholder" === input ? (result = localize.getLocalizedString(placeholder), ele.attr("placeholder", result)) : input.length >= 1 ? (result = localize.getLocalizedString(input), ele.text(result)) : void 0
                        },
                        link: function(scope, ele, attrs) {
                            return scope.$on("localizeResourcesUpdated", function() {
                                return i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder)
                            }), attrs.$observe("i18n", function(value) {
                                return i18nDirective.updateText(ele, value, attrs.placeholder)
                            })
                        }
                    }
                }]).controller("LangCtrl", ["$scope", "localize", function($scope, localize) {
                    return $scope.lang = "English", $scope.setLang = function(lang) {
                        switch (lang) {
                            case "English":
                                localize.setLanguage("EN-US");
                                break;
                            case "Español":
                                localize.setLanguage("ES-ES");
                                break;
                            case "日本語":
                                localize.setLanguage("JA-JP");
                                break;
                            case "中文":
                                localize.setLanguage("ZH-TW");
                                break;
                            case "Deutsch":
                                localize.setLanguage("DE-DE");
                                break;
                            case "français":
                                localize.setLanguage("FR-FR");
                                break;
                            case "Italiano":
                                localize.setLanguage("IT-IT");
                                break;
                            case "Portugal":
                                localize.setLanguage("PT-BR");
                                break;
                            case "Русский язык":
                                localize.setLanguage("RU-RU");
                                break;
                            case "한국어":
                                localize.setLanguage("KO-KR")
                        }
                        return $scope.lang = lang
                    }, $scope.getFlag = function() {
                        var lang;
                        switch (lang = $scope.lang) {
                            case "English":
                                return "flags-american";
                            case "Español":
                                return "flags-spain";
                            case "日本語":
                                return "flags-japan";
                            case "中文":
                                return "flags-china";
                            case "Deutsch":
                                return "flags-germany";
                            case "français":
                                return "flags-france";
                            case "Italiano":
                                return "flags-italy";
                            case "Portugal":
                                return "flags-portugal";
                            case "Русский язык":
                                return "flags-russia";
                            case "한국어":
                                return "flags-korea"
                        }
                    }
                }])
        }.call(this),
        function() {
            "use strict";
            angular.module("app.controllers", [])
                    .config(function($httpProvider) {
                        $httpProvider.responseInterceptors.push('myHttpInterceptor');
                        var spinnerFunction = function(data, headersGetter) {
                            // todo start the spinner here
                            // alert('start spinner');

                            $('#preloader').show();
                            return data;
                        };
                        $httpProvider.defaults.transformRequest.push(spinnerFunction);
                    })
                    // register the interceptor as a service, intercepts ALL
                    // angular ajax http calls
                    .factory('myHttpInterceptor', function($q, $window,$rootScope) {
                        return function(promise) {
                            return promise.then(function(response) {
                                // do something on success
                                // todo hide the spinner
                                // alert('stop spinner');
                                //console.log(response);
                                //console.log("--"+response.data.status);
                                if(response.data.status=="500"){
                                window.location=$rootScope.urlDomain;
                                }
                                $('#preloader').hide();
                                return response;
                            }, function(response) {
                                // do something on error
                                // todo hide the spinner
                                // alert('stop spinner');
                                //console.log(response);
                                //console.log("--"+response.status);
                                if(response.data.status=="500"){
                                window.location=$rootScope.urlDomain;
                                }
                                $('#preloader').hide();
                                return $q.reject(response);
                            });
                        };
                    }).controller("AppCtrl", ["$rootScope", "$scope", "$location", "$http", "$cookies", "$modal", "$log", function($rootScope, $scope, $location, $http, $cookies, $modal, $log) {

                    
                    $scope.logout = function() {
                        var data = {
                            logout:"Y"
                        }
                        var callURL = $rootScope.urlDomain + "j_spring_security_logout";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                              	//alert("Are you sure, you want to proceed for LPC");
                                $location.path('/pages/signin');
                        }).error(function(error) {
                            console.log(error);
                        });
                    }

//                  $rootScope.urlDomain = "http://10.3.3.221:8080/Sangam/";
                    $rootScope.urlDomain = "http://localhost:9082/Sangam/";
                    $rootScope.isOtpAllowed = false;
                    $rootScope.setCookies = function(appId, progress) {
                        $cookies.a = appId;
                        $rootScope.applicationId = appId;
                        $cookies.c = progress;
                        $rootScope.progress = progress;
                    }

                    $rootScope.openOtp = function() {
                        $rootScope.userOtp = "";
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "otp.html",
                            controller: "OtpModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "OTP";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
//                                alert("verify");
                                $rootScope.verifyOTP();
                            } else {
//                                alert("resend");
                                $rootScope.sendOTP();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $rootScope.doLpc = function doLpc(isVehicleDelivered) {


                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            isVehicleDelivered: isVehicleDelivered
                        }
                        var callURL = $rootScope.urlDomain + "doLpc.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == '200') {
//                            	alert("Are you sure, you want to proceed for LPC");
                                $location.path('/lpcApplication');
                            } else {
                                alert(response.msg);
                            }

                        }).error(function(error) {
                            console.log(error);
                        });


                    }

                    $rootScope.sendOTP = function sendOTP() {
                    	$rootScope.userOtp = "";
                        var callURL = $rootScope.urlDomain + "sendOtp.json";

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        $http({
                            method: "post",
                            url: callURL,
                            // transformRequest: transformRequestAsFormPost,
                            data: data
                        }).success(function(response, status, headers, config)
                        {
                            console.log(response);
                            if (response.status == "200") {
                                alert(response.msg);
                            } else {
                                alert(response.msg);
                            }
                        }).error(function(data, status, headers, config)
                        {
                            alert("Error while requesting");
                        });

                    }
                    $rootScope.userOtp = "";
                    $rootScope.verifyOTP = function verifyOTP() {
//                    	$rootScope.userOtp = "";
                        //alert('Test OTP');
                        var callURL = $rootScope.urlDomain + "verifyOtp.json";
                        console.log("otp " + $rootScope.userOtp);
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            userOtp: $rootScope.userOtp
                        };
                        $http({
                            method: "post",
                            url: callURL,
                            // transformRequest: transformRequestAsFormPost,
                            data: data
                        }).success(function(response, status, headers, config)
                        {
                            console.log(response);
                            if (response.status == "200") {
                                alert(response.msg);
                            } else {
                                alert(response.msg);
                            }
                        }).error(function(data, status, headers, config)
                        {
                            alert("Error while requesting");
                        });

                    }

                    return $scope.isSpecificPage = function() {
                        var path;
                        return path = $location.path(), _.contains(["/404", "/pages/500", "/pages/login", "/pages/signin", "/pages/signin1", "/pages/signin2", "/pages/signup", "/pages/signup1", "/pages/signup2", "/pages/forgot", "/pages/lock-screen"], path)
                    }, $scope.main = {
                        brand: "Square",
                        name: "Lisa Doe"
                    }
                }]).controller("NavCtrl", ["$rootScope", "$scope", "taskStorage", "filterFilter", function($rootScope, $scope, taskStorage, filterFilter) {
                    var tasks;
                    return tasks = $scope.tasks = taskStorage.get(), $scope.taskRemainingCount = filterFilter(tasks, {
                        completed: !1
                    }).length, $scope.$on("taskRemaining:changed", function(event, count) {
                        return $scope.taskRemainingCount = count
                    });
                }]).controller("DashboardCtrl", ["$cookies", "$rootScope", "$scope", "$filter", "$http", "$location", function($cookies, $rootScope, $scope, $filter, $http, $location) {
                    var init;
// $rootScope.progress = 1;
                    $scope.preloadDashboard = function preload() {
                        $rootScope.setCookies(0, 0);
                        var callURL = $rootScope.urlDomain + "dashboard.json";
                        var data = {
                            userId: $rootScope.userId
                        };
                        console.log($rootScope.userId + " " + callURL + " " + data);
                        $http({
                            method: "post",
                            url: callURL,
                            data: data
                        }).success(function(response, status, headers, config)
                        {
                            console.log(response);

                            if (response.status == "200") {
                                $scope.stores = response.lstDashboardVO,
                                        $scope.searchKeywords = "", $scope.filteredStores = [], $scope.row = "", $scope.select = function(page) {
                                    var end, start;
                                    $scope.currentPage = page;
                                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
                                }, $scope.onFilterChange = function() {
                                    return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
                                }, $scope.onNumPerPageChange = function() {
                                    return $scope.select(1), $scope.currentPage = 1
                                }, $scope.onOrderChange = function() {
                                    return $scope.select(1), $scope.currentPage = 1
                                }, $scope.search = function() {
                                    return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
                                }, $scope.order = function(rowName) {
                                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
                                }, $scope.numPerPageOpt = [5, 10, 15, 30, 50, 100], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageStores = [], (init = function() {
                                    return $scope.search(), $scope.select($scope.currentPage)
                                })()

                            } else {
                                alert(response.msg);
                            }
                        }).error(function(data, status, headers, config)
                        {
                            alert("Error while requesting");
                        });
                    };
                    $scope.showform = function showform(appId, progress, status) {
                        $cookies.a = appId;
                        $rootScope.applicationId = appId;
                        $cookies.c = progress;
                        $rootScope.progress = progress;
                        if (status == "SAVED") {
                            $rootScope.isDisabledPreSanction = false;
                        } else {
                            $rootScope.isDisabledPreSanction = true;
                        }

                        $location.path('/presanction/sourcing');
                    };
                }]).controller("login", ["$cookies", "$rootScope", "$scope", "$http", "$location", function($cookies, $rootScope, $scope, $http, $location) {



                    $scope.preloadLogin = function() {



                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }

                        var callURL = $rootScope.urlDomain + "sec/EntityLogin.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {

                                $scope.entityArr = [{value: "0", description: 'Select Entity'}];




                                angular.forEach(response.loginVO.lstEntityVO, function(branch, index) {
                                    var tempObject = {value: branch.value, description: branch.description};
                                    $scope.entityArr.push(tempObject);
                                });

                                $scope.selectentity = $scope.entityArr[0].value;

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
$scope.password="";

                    };

                    var callURL = $rootScope.urlDomain + "sec/login.json";
                    $scope.login = function login() {
                        if ($scope.captcha == null || $scope.captcha== "") {
                            alert("Please enter Verification Text");
                            return false;
                        }
if ($scope.selectentity == null || $scope.selectentity == "0") {
                            alert("Please Select Entity");
                            return false;
                        } else {
                            $rootScope.loginEntity = $scope.selectentity;
                        }

                        var iterationCount = 1000;
                        var keySize = 128;
                        var iv = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
                        var salt = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);

                        var passphrase = "";
                        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                        for (var i = 0; i < 16; i++)
                            passphrase += possible.charAt(Math.floor(Math.random() * possible.length));

                        var plaintext = $scope.password;
                        var aesUtil = new AesUtil(keySize, iterationCount);
                        var ciphertext = aesUtil.encrypt(salt, iv, passphrase, plaintext);
                        // alert(salt + " | " + iv);

                        var encrypt = new JSEncrypt();
                        encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA37kQNR2cQQzvL4xyvrRn8vPhECCoL1cJ2e5gwvC/JGeLaQ44G/lJQ+p8qFXE55tum3SBSGa6uiPm7rbBlyD0G6qcNx+lfAbqFd2Cw/VgEhHOmqbuCfxbViiafaUzZ5GQgMNxXaZZM3vsBN3N1yqK2i8K6i4kyMpeMVm+6qyMmqQyBt+GRkwDZDL3I659VgcE7RiwPJlLupZ+EVumetMGIxIr1VhQpj9kIJEWzvS51Ss8hUiS/1x2mu+VfYWKndtU5hkOPHQ1m20Vv2uXvse4g99mADOYdV6U6FBLa1VFUTT7osg6zM8+t5fPM5d4kv5s3Ecp+clMLeAAkaVuybcgsQIDAQAB");

                        var encryptedUser = encodeURIComponent($scope.username);
                        var encryptedPass = encodeURIComponent(ciphertext);
                        var RSAEncAESkey = encodeURIComponent(encrypt.encrypt(salt + "|" + iv));
                        var passPhrase = encodeURIComponent(passphrase);
$scope.username="";
$scope.password="XXXX";
//alert("-");
document.getElementById('loginusername').value="";
document.getElementById('loginpassword').value="XXXX";


                        var data = {
                            j_username: encryptedUser,
                            j_password: encryptedPass,
                            j_phase: passPhrase,
                            j_key: RSAEncAESkey,
                            captcha:$scope.captcha
                        };
//                        var data = {
//                            j_username: $scope.username,
//                            j_password: $scope.password
//                        };
                        $http({
                            method: "post",
                            url: callURL,
                            // transformRequest: transformRequestAsFormPost,
                            data: data
                        }).success(function(response, status, headers, config)
                        {
                            if (response.status == "200") {

                                $cookies.i = response.userId;
                                // $cookies.n = response.userName;
                                $cookies.l = response.lastLoginTime;
                                $cookies.p = response.profileId;
                                $cookies.e = $rootScope.loginEntity;
                                $cookies.a = 0;
                                $cookies.n = response.userDisplayName;
                                $cookies.r = response.profileDescription;
                                $rootScope.userId = response.userId;
                                // $rootScope.userName=response.userName;
                                $rootScope.userName = response.userDisplayName;
                                $rootScope.lastLoginTime = response.lastLoginTime;
                                $rootScope.profileName = response.profileDescription;
                                $rootScope.profileId = response.profileId;
                                if ($rootScope.profileId == 1) {
                                    $rootScope.isOtpAllowed = true;
                                    $rootScope.isGenAppId = true;
                                } else {
                                    $rootScope.isOtpAllowed = false;
                                    $rootScope.isGenAppId = false;
                                }

                                $location.path('/presanctiondashboard');
                            } else {
                                alert(response.msg);
                            }

                            // $scope.simpleGetCallResult = logResult("GET
                            // SUCCESS", data,
                            // status, headers, config);
                        }).error(function(data, status, headers, config)
                        {
                            alert("Error while requesting");
                            // $scope.simpleGetCallResult = logResult("GET
                            // ERROR", data,
                            // status, headers, config);
                        });
                        return false;
                    };
                }]).controller("LoanCtrl", ["$rootScope", "$location", "$scope", "$filter", "$http", "$modal", "$log", "$route", function($rootScope, $location, $scope, $filter, $http, $modal, $log, $route) {
                    var init;

                    $scope.formReload = function() {
                        $route.reload();
                    };

                    $scope.saveCheck = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitLoanDetails();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    
                   

                    $scope.calculateAssetCost = function() {
                        if ($scope.ourParseInt($scope.exshowroom)) {
                            $scope.exshowroom = parseInt($scope.exshowroom);
                        } else {
                            $scope.exshowroom = 0;
                        }

                        if ($scope.ourParseInt($scope.insurance)) {
                            $scope.insurance = parseInt($scope.insurance);
                        } else {
                            $scope.insurance = 0;
                        }

                        if ($scope.ourParseInt($scope.registration)) {
                            $scope.registration = parseInt($scope.registration);
                        } else {
                            $scope.registration = 0;
                        }

                        if ($scope.ourParseInt($scope.othersloandetails)) {
                            $scope.othersloandetails = parseInt($scope.othersloandetails);
                        } else {
                            $scope.othersloandetails = 0;
                        }

                        $scope.assetcost = Math.round(($scope.exshowroom + $scope.insurance + $scope.registration + $scope.othersloandetails) * 100) / 100;
                        if (isNaN($scope.assetcost)) {
                            $scope.assetcost = 0;
                        }

                        return $scope.assetcost;
                    }


                    $scope.calculateAppliedMarginMoney = function() {
                        if ($scope.ourParseInt($scope.assetcost)) {
                            $scope.assetcost = parseInt($scope.assetcost);
                        } else {
                            $scope.assetcost = 0;
                        }

                        if ($scope.ourParseInt($scope.appliedloanamount)) {
                            $scope.appliedloanamount = parseInt($scope.appliedloanamount);
                        } else {
                            $scope.appliedloanamount = 0;
                        }



                        $scope.appliedmarginmoney = Math.round(($scope.assetcost - $scope.appliedloanamount) * 100) / 100;
                        if (isNaN($scope.appliedmarginmoney)) {
                            $scope.appliedmarginmoney = 0;
                        }

                        return $scope.appliedmarginmoney;
                    }


                    $scope.calculateEMI = function() {
//                        if ($scope.ratepercent == null || $scope.ratepercent == "") {
//                            $scope.ratepercent = $scope.ratepercent;
//                            
//                        } 
//                        else {
//                            $scope.ratepercent = 0;
//                        }

                        if ($scope.ourParseInt($scope.appliedtenure)) {
                            $scope.appliedtenure = parseInt($scope.appliedtenure);
                        } else {
                            $scope.appliedtenure = 0;
                        }

                        if ($scope.ourParseInt($scope.appliedloanamount)) {
                            $scope.appliedloanamount = parseInt($scope.appliedloanamount);
                        } else {
                            $scope.appliedloanamount = 0;
                        }


                        var r = $scope.ratepercent / 100 / 12;
                        var LoanPeriod = $scope.appliedtenure;
                        $scope.loanemiloandetails = Math.ceil(Math.round(($scope.appliedloanamount * (r * Math.pow((1 + r), LoanPeriod) / (Math.pow((1 + r), LoanPeriod) - 1))) * 100) / 100);
//                        Math.ceil($scope.loanemiloandetails);
                        if (isNaN($scope.loanemiloandetails)) {
                            $scope.loanemiloandetails = 0;
                        }

                        return $scope.loanemiloandetails;
                    }

                    $scope.calculateAppliedltv = function() {

// if ($scope.ourParseInt($scope.appliedloanamount)) {
// $scope.appliedloanamount = parseInt($scope.appliedloanamount);
// } else {
// $scope.appliedloanamount = 0;
// }
//
// if ($scope.ourParseInt($scope.advinstall)) {
// $scope.advinstall = parseInt($scope.advinstall);
// } else {
// $scope.advinstall = 0;
// }
//
// if ($scope.ourParseInt($scope.assetcost)) {
// $scope.assetcost = parseInt($scope.assetcost);
// } else {
// $scope.assetcost = 0;
// }

                        $scope.appliedltv = Math.round((($scope.ourParseIntAn($scope.appliedloanamount) - $scope.ourParseIntAn($scope.advinstall) * $scope.ourParseIntAn($scope.loanemiloandetails)) / $scope.ourParseIntAn($scope.assetcost) * 100) * 100) / 100;


                        if (isNaN($scope.appliedltv)) {
                            $scope.appliedltv = 0;
                        }
                        return $scope.appliedltv;
                    }
                    
                    

                    $scope.ourParseIntAn = function(fieldValue) {
                        var intReg = /^[0-9]+$/;
                        if (intReg.test(fieldValue)) {
                            return parseInt(fieldValue);
                        } else {
                            return 0;
                        }

                    }
                    $scope.ourParseInt = function(fieldValue) {
                        var intReg = /^[0-9]+$/;
                        return intReg.test(fieldValue);
                    }
                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }
                    
//                    if($scope.advinstall == null || $scope.advinstall == "") {
//                    	$scope.advinstall=0;
//                    }
                    
                    
                    
                    $scope.checkDecimalValue = function(checkValue) {
                        var value = /^[0-9]{1,}(\.[0-9]{1,2})?$/;
                        return value.test(checkValue);
                    }
                    $scope.preloadLoanDetails = function preload() {
                        $scope.FrequencyArr = [{frequencyId: "0", frequencyDesc: "Select Frequency"},
                            {frequencyId: "MONTHLY", frequencyDesc: "MONTHLY"}];
                        $scope.selectfrequency = $scope.FrequencyArr[0].frequencyId;
                        $scope.selectfrequency = "MONTHLY";
                        $scope.insurancepremiumcollectionArr = [{id: "0", desc: "Select Insurance Premium Collected"},
                            {id: "Upfront", desc: "Upfront"},
                            {id: "Funded", desc: "Funded"},
                            {id: "Not Applicable", desc: "Not Applicable"}];
                        $scope.insurancepremiumcollection = $scope.insurancepremiumcollectionArr[0].id;
                        var callURL = $rootScope.urlDomain + "FetchLoanData.json";
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {

                                console.log(response.loanVO);

                                $scope.loanVO = response.loanVO;
                                
                                $scope.advinstall = 0;

                                if (response.data != null) {
                                    $scope.exshowroom = response.data.exShowroom;
                                    $scope.insurance = response.data.insurance;
                                    $scope.registration = response.data.registration;
                                    $scope.othersloandetails = response.data.others;
                                    $scope.assetcost = response.data.assetCost;
                                    $scope.appliedloanamount = response.data.appliedLoanAmount;
                                    $scope.appliedmarginmoney = response.data.appliedMarginMoney;
                                    $scope.appliedtenure = response.data.appliedTenureMonths;
                                    $scope.advinstall = response.data.advInstl.toString();
                                    $scope.ratepercent = response.data.rate;
                                    $scope.loanemiloandetails = response.data.loanEmi;
                                    $scope.selectfrequency = response.data.frequency;
                                    $scope.appliedltv = response.data.appliedLtv,
                                            $scope.insurancepremiumcollection = response.data.insurancePremiumCollected;
                                }

                            } else {

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    }

                    $scope.validateLoanDetailsForm = function() {

                        if ($scope.exshowroom == null || $scope.exshowroom == "") {
                            alert("Please Enter Exshowroom ");
                        } else if (!$scope.checkDecimal($scope.exshowroom)) {
                            alert("Decimal values are not allowed for Exshowroom");
                        } else if ($scope.insurance != null && $scope.insurance != "" && $scope.insurance.length > 0 && !$scope.checkDecimal($scope.insurance)) {
                            alert("Special character and Decimal Value are not allowed for Insurance");
                        }
//                        else if (!$scope.checkDecimal($scope.insurance)) {
//                            alert("Decimal values are not allowed for Insurance");
//                        } 
                        else if ($scope.registration != null && $scope.registration != "" && $scope.registration.length > 0 && !$scope.checkDecimal($scope.registration)) {
                            alert("Special character and Decimal Value are not allowed for Registration");
                        }
//                        else if (!$scope.checkDecimal($scope.registration)) {
//                            alert("Decimal values are not allowed for Registration");
//                        }
                        else if ($scope.othersloandetails != null && $scope.othersloandetails != "" && $scope.othersloandetails.length > 0 && !$scope.checkDecimal($scope.othersloandetails)) {
                            alert("Special character and Decimal Value are not allowed for Othersloandetails");
                        }
//                        else if (!$scope.checkDecimal($scope.othersloandetails)) {
//                            alert("Decimal values are not allowed for Othersloandetails");
//                        } 
                        else if ($scope.assetcost == null || $scope.assetcost == "") {
                            alert("Please Enter Asset Cost");
                        } else if ($scope.appliedloanamount == null || $scope.appliedloanamount == "") {
                            alert("Please Enter Appliedloanamount");
                        } else if (!$scope.checkDecimal($scope.appliedloanamount)) {
                            alert("Decimal values are not allowed for Appliedloanamount");
                        } else if ($scope.appliedloanamount > $scope.assetcost) {
                            alert("Applied Loan Amount should not be greater than total Asset Cost");
                        } else if ($scope.appliedmarginmoney == null || $scope.appliedmarginmoney == "") {
                            alert("Please Enter Appliedmarginmoney");
                        } else if ($scope.appliedtenure == null || $scope.appliedtenure == "") {
                            alert("Please Enter Appliedtenure");
                        } else if (!$scope.checkDecimal($scope.appliedtenure)) {
                            alert("Decimal values are not allowed for Appliedtenure");
                        } else if ($scope.advinstall != null && $scope.advinstall != "" && $scope.advinstall.length > 0 && !$scope.checkDecimal($scope.advinstall)) {
                        	
                            alert("Decimal values are not allowed for Advinstall");
                        }
//                        } else if (!$scope.checkDecimal($scope.advinstall)) {
//                            alert("Decimal values are not allowed for Advinstall");
//                        }
                        else if ($scope.advinstall >= $scope.appliedtenure) {
                            alert("Advance Installment is not allowed more than Applied Tenure");
                        } else if ($scope.ratepercent == null || $scope.ratepercent == "") {
                            alert("Please Enter Rate percentage");
                        }
                        else if (!$scope.checkDecimalValue($scope.ratepercent)) {
                            alert("Only Numbers are allowed for Ratepercent!\nRatepercent allowed two digit after decimal");
                        }
                        else if ($scope.loanemiloandetails == null || $scope.loanemiloandetails == "") {
                            alert("Please Enter Loan Emi ");
//                        } else if ($scope.selectfrequency == null || $scope.selectfrequency == "") {
//                            alert("Please Select Select Frequency");
                        } else if ($scope.appliedltv == null || $scope.appliedltv == "") {
                            alert("Please Enter Appliedltv");
                        } else if ($scope.insurancepremiumcollection == null || $scope.insurancepremiumcollection == "0" || $scope.insurancepremiumcollection == "") {
                            alert("Please Select Insurance Premium Collection");
                        } else if ($scope.appliedloanamount < $scope.loanVO.minFinance || $scope.appliedloanamount > $scope.loanVO.maxFinance) {
                            alert("Loan amount is allowed between " + $scope.loanVO.minFinance + " and  " + $scope.loanVO.maxFinance);
                        } else if ($scope.ratepercent < $scope.loanVO.minRate || $scope.ratepercent > $scope.loanVO.maxRate) {
                            alert("Rate should is allowed between " + $scope.loanVO.minRate + " and  " + $scope.loanVO.maxRate);
                        } else if ($scope.appliedtenure < $scope.loanVO.minTenure || $scope.appliedtenure > $scope.loanVO.maxTenure) {
                            alert("Tenure is allowed between " + $scope.loanVO.minTenure + " and  " + $scope.loanVO.maxTenure);
                        }
                        else {

                            $scope.saveCheck();

                        }
                    }

                    $scope.submitLoanDetails = function submitLoanDetails() {

                        var callURL = $rootScope.urlDomain + "SaveLoanData.json";
                        
	                    if ($scope.ourParseInt($scope.advinstall)) {
                        $scope.advinstall = parseInt($scope.advinstall);
                    } else {
                        $scope.advinstall = 0;
                    }
                        
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            exshowroom: $scope.exshowroom,
                            insurance: $scope.insurance,
                            registration: $scope.registration,
                            othersloandetails: $scope.othersloandetails,
                            assetcost: $scope.assetcost,
                            appliedloanamount: $scope.appliedloanamount,
                            appliedmarginmoney: $scope.appliedmarginmoney,
                            appliedtenure: $scope.appliedtenure,
                            advinstall: $scope.advinstall,
                            ratepercent: $scope.ratepercent,
                            loanemiloandetails: $scope.loanemiloandetails,
                            selectfrequency: $scope.selectfrequency,
                            appliedltv: $scope.appliedltv,
                            insurancepremiumcollection: $scope.insurancepremiumcollection
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            if (response.status == "200") {
                                if ($rootScope.progress < 7) {
                                    $rootScope.progress = 7;
                                }
                                $location.path('/presanction/document');
                                $rootScope.applicationId = response.applicationId;
                            } else {
                                alert(response.msg);
                            }
// console.log(response);
                        }).error(function(error) {
                            console.log(error);
                        });


                    };

                }]).controller("OtpModalInstanceCtrl", [
                "$scope", "$modalInstance", "message",
                function($scope, $modalInstance, message) {
                    $scope.message = message,
                            $scope.verifyHtml = function() {
                                $modalInstance.close(true)
                            },
                            $scope.resendHtml = function() {
                                $modalInstance.close(false)
                            }, $scope.cancel = function() {
                        $modalInstance.dismiss("cancel")
                    }
                }
            ]).controller("ConfirmationModalInstanceCtrl", [
                "$scope", "$modalInstance", "message",
                function($scope, $modalInstance, message) {
                    $scope.message = message,
                            $scope.save = function() {
                                $modalInstance.close(true)
                            }, $scope.cancel = function() {
                        $modalInstance.dismiss("cancel")
                    }
                }
            ]).controller("SourcingCtrl", [
                "$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", "$route",
                function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log, $route) {

                    $scope.formReload = function() {
                        //alert("e");
                        $route.reload();
                    };

                    $scope.setSchId = function(schemeId) {
                        angular.forEach($scope.descriptionSchemeArr, function(scheme, index) {
                            if (scheme.schemeId == schemeId) {
                                $scope.schId = scheme.schId;
                            }
                        });
                    };

                    $scope.saveCheck = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitSourcingForm();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    $scope.openNavigation = function() {
                        $("#presanction-li").click();
                    };
                    var isExistingData = false;
                    var sourceingData = {};
                    $scope.preloadSourcing = function() {

                        $scope.openNavigation();

                        $scope.descSchemeArr = [{schemeId: "0", schemeDesc: 'Select Scheme', schId: ''}];
                        $scope.promotionArr = [{promotionId: "0", promotionDesc: 'Select Promotion Code', schemeId: '0'}];
                        $scope.descSchemeGroupArr = [{schId: "0", schDesc: 'Select Scheme Group', schGroupId: ''}];
                        $scope.selectBranchArr = [{branchId: "0", branchName: 'Select Branch'}];
                        $scope.dstArr = [{dstId: "0", dstName: 'Select DST'}];
                        $scope.dsaArr = [{brokerId: "0", brokerName: 'Select DSA'}];
                        $scope.supplierArr = [{supplierId: "0", supplierName: 'Select Supplier'}];
                        $scope.channelcodeArr = [{value: "0", description: 'Select Channel Code', promotionId:"0"}];
                        $scope.sourcecodeArr = [{value: "0", description: 'Select Source Code'}];
                        $scope.labelcodeArr = [{value: "0", description: 'Select Label Code'}];
                        $scope.entityArr = [{value: "0", description: 'Select Entity'}];
                        $scope.productArr = [{value: "0", description: 'Select Product'}];
                        $scope.stateList = "";
                        $scope.dstList = "";
                        $scope.dsaList = "";
                        $scope.supplierList = "";
                        /*godwin*/
                        $scope.branchList = "";
                        $scope.lstPromotions="";
                        $scope.lstChannelCode="";
                        $scope.descriptionSchemeArr="";
                        /*godwin*/
                        $scope.selectProduct = $scope.productArr[0].value;
                        $scope.selectbranch = $scope.selectBranchArr[0].branchId;
                        $scope.selectdst = $scope.dstArr[0].dstId;
                        $scope.selectdsa = $scope.dsaArr[0].brokerId;
                        $scope.supplier = $scope.supplierArr[0].supplierId;



                        $scope.selectentity = $rootScope.loginEntity;


                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            entityId: $rootScope.loginEntity
                        }
                        var callURL = $rootScope.urlDomain + "FetchSourcingData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);

                            if (response.status == "200") {

                                $scope.datereciept = response.sourcingDataVO.date;

//                                angular.forEach(response.sourcingDataVO.lstScheme, function(scheme, index) {
//                                    var tempObject = {schemeId: scheme.schemeId, schemeDesc: scheme.schemeDesc, schId: scheme.schId};
//                                    $scope.descSchemeArr.push(tempObject);
//                                });

                                $scope.descriptionSchemeArr = response.sourcingDataVO.lstScheme;
                                //$scope.selectschemedescription = $scope.descriptionSchemeArr[0].schemeId;
                                /*godwin*/
                                $scope.lstSchemeGroup = response.sourcingDataVO.lstSchemeGroup;
                                $scope.lstPromotions = response.sourcingDataVO.lstPromotion;
                                //alert($scope.lstPromotions.length);
                                $scope.branchList = response.sourcingDataVO.lstBranch;
                                $scope.stateList = response.sourcingDataVO.lstState;
                                $scope.selectProduct = $scope.productArr[0].value;
                                angular.forEach(response.sourcingDataVO.lstProduct, function(product, index) {
                                    var tempObject = {value: product.value, description: product.description};
                                    $scope.productArr.push(tempObject);
                                });
                                /*godwin*/
                                /*angular.forEach(response.sourcingDataVO.lstBranch, function(branch, index) {
                                 var tempObject = {branchId: branch.branchId, branchName: branch.branchName};
                                 $scope.selectBranchArr.push(tempObject);
                                 });*/
                                /*godwin*/
                                $scope.lstChannelCode=response.sourcingDataVO.lstChannelCode;
//                                angular.forEach(response.sourcingDataVO.lstChannelCode, function(branch, index) {
//                                    var tempObject = {value: branch.value, description: branch.description};
//                                    $scope.channelcodeArr.push(tempObject);
//                                });
//                                $scope.selectchannelcode = $scope.channelcodeArr[0].value;
                                angular.forEach(response.sourcingDataVO.lstSourceCode, function(branch, index) {
                                    var tempObject = {value: branch.value, description: branch.description};
                                    $scope.sourcecodeArr.push(tempObject);
                                });
                                $scope.sourcecodeArr = $scope.sourcecodeArr;
                                $scope.selectsource = $scope.sourcecodeArr[0].value;
                                angular.forEach(response.sourcingDataVO.lstLabelCode, function(branch, index) {
                                    var tempObject = {value: branch.value, description: branch.description};
                                    $scope.labelcodeArr.push(tempObject);
                                });
                                $scope.selectlabelcode = $scope.labelcodeArr[0].value;
                                angular.forEach(response.sourcingDataVO.lstEntity, function(branch, index) {
                                    var tempObject = {value: branch.value, description: branch.description};
                                    $scope.entityArr.push(tempObject);
                                });


                                $scope.dstList = response.sourcingDataVO.lstDst;
                                $scope.dsaList = response.sourcingDataVO.lstBroker;
                                $scope.supplierList = response.sourcingDataVO.lstSupplier ? response.sourcingDataVO.lstSupplier : [];
                                if (response.data != null) {
                                    sourceingData = response.data;
                                    $scope.datereciept = $filter('date')(sourceingData.receiptDate, 'dd-MM-yyyy');
                                    $scope.applicationformno = sourceingData.applicationFormNo;
                                    //$scope.selectschemedescription = sourceingData.schemeId;
                                    $scope.schId = sourceingData.schId;
                                    $scope.sourcestateId = sourceingData.stateId;
                                    //$scope.selectchannelcode = sourceingData.channelCode;
                                    $scope.selectsource = sourceingData.source;
                                    $scope.selectlabelcode = sourceingData.labelCode;
                                    $scope.selectentity = sourceingData.entity;
                                    $scope.createdDate = $filter('date')(sourceingData.createdDate, 'dd-MM-yyyy');
                                    $scope.modDate = $filter('date')(sourceingData.modDate, 'dd-MM-yyyy');
                                    $scope.preSubmittedDate = $filter('date')(sourceingData.preSubmittedDate, 'dd-MM-yyyy');
                                    $scope.selectProduct = sourceingData.product;
                                    $scope.selectbranch = sourceingData.branchCode;
                                    $scope.selectdst = sourceingData.dstId;
                                    $scope.selectdsa = sourceingData.brokerId;
                                    $scope.supplier = sourceingData.supplierId;
                                    $scope.status = sourceingData.status;
                                    isExistingData = true;

                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    /*new code start*/
                    // $scope.branchList
                    $scope.$watch('selectProduct', function() {
                        //alert(isExistingData);
                        
                        if ($scope.descSchemeGroupArr.length > 1) {
                            $scope.descSchemeGroupArr.splice(1, $scope.descSchemeGroupArr.length);
                        }
                        $scope.selectschemegroup = $scope.descSchemeGroupArr[0].schId;
                        angular.forEach($scope.lstSchemeGroup, function(schemeGroup, index) {
                            // alert(branch.productCode);
                            // alert($scope.selectProduct);
                            if (schemeGroup.lsmNpmCode == $scope.selectProduct) {
                                var tempObject = {schId: schemeGroup.schId, schDesc: schemeGroup.schDesc, schGroupId: schemeGroup.schemeGroupId};
                                $scope.descSchemeGroupArr.push(tempObject);
                            }

                        });
                        
                        if ($scope.selectBranchArr.length > 1) {
                            $scope.selectBranchArr.splice(1, $scope.selectBranchArr.length);
                        }
                        $scope.selectbranch = $scope.selectBranchArr[0].branchId;
                        angular.forEach($scope.branchList, function(branch, index) {
                            // alert(branch.productCode);
                            // alert($scope.selectProduct);
                            if (branch.productCode == $scope.selectProduct) {
                                var tempObject = {branchId: branch.branchId, branchName: branch.branchName};
                                $scope.selectBranchArr.push(tempObject);
                            }

                        });



                        //alert("before inside true");
                        if (isExistingData) {
                            //alert("inside true");
                            $scope.selectbranch = sourceingData.branchCode;
                            $scope.selectschemegroup = sourceingData.schGroupId;
                            
                        }



                    }, true);



                    $scope.$watch('selectschemegroup', function() {
                        //alert(isExistingData);
                        if ($scope.descSchemeArr.length > 1) {
                            $scope.descSchemeArr.splice(1, $scope.descSchemeArr.length);
                        }
                        $scope.selectschemedescription = $scope.descSchemeArr[0].schemeId;
                        //alert($scope.descriptionSchemeArr.length);
                        angular.forEach($scope.descriptionSchemeArr, function(scheme, index) {
                            // alert(branch.productCode);
                            // alert($scope.selectProduct);
                            //alert(scheme.schId+"_"+$scope.selectschemegroup);
                            if (scheme.schId == $scope.selectschemegroup) {
                                var tempObject = {schemeId: scheme.schemeId, schemeDesc: scheme.schemeDesc, schId: scheme.schId};
                                $scope.descSchemeArr.push(tempObject);
                            }

                        });
                        
                        



                        //alert("before inside true");
                        if (isExistingData) {
                            //alert("inside true");
                            $scope.selectschemedescription = sourceingData.schemeId;
                            
                        }



                    }, true);
                    
                    
                    
                    $scope.$watch('selectschemedescription', function() {
                        //alert(isExistingData);
                        if ($scope.promotionArr.length > 1) {
                            $scope.promotionArr.splice(1, $scope.promotionArr.length);
                        }
                       
                        $scope.selectpromotion = $scope.promotionArr[0].schemeId;
                        //alert($scope.descriptionSchemeArr.length);
                        angular.forEach($scope.lstPromotions, function(promotion, index) {
                            
                            if (promotion.schemeid == $scope.selectschemedescription) {
                                var tempObject = {promotionId: promotion.promotionid, promotionDesc: promotion.promotiondesc, schemeId: promotion.schemeid};
                                $scope.promotionArr.push(tempObject);
                            }

                        });
                        
                        



                        //alert("before inside true");
                        if (isExistingData) {
                            //alert("inside true");
                            $scope.selectpromotion = sourceingData.promotionId;
                            
                        }



                    }, true);
                    
                    
                    
                    $scope.$watch('selectpromotion', function() {
                        //alert(isExistingData);
                        if ($scope.channelcodeArr.length > 1) {
                            $scope.channelcodeArr.splice(1, $scope.channelcodeArr.length);
                        }
                       
                        $scope.selectchannelcode = $scope.channelcodeArr[0].value;
                        //alert($scope.descriptionSchemeArr.length);
                        angular.forEach($scope.lstChannelCode, function(channel, index) {
                            
                            if (channel.promotionid == $scope.selectpromotion) {
                                var tempObject = {value: channel.value, description: channel.description, promotionId:channel.promotionid};
                                $scope.channelcodeArr.push(tempObject);
                            }

                        });
                        
                        



                        //alert("before inside true");
                        if (isExistingData) {
                            //alert("inside true");
                            $scope.selectchannelcode = sourceingData.channelCode;
                            
                        }



                    }, true);



                    /*new code end*/
                    /*code modified*/
                    $scope.$watch('selectbranch', function() {
                        //alert("here");

                        if ($scope.dstArr.length > 1) {
                            $scope.dstArr.splice(1, $scope.dstArr.length);
                        }
                        $scope.selectdst = $scope.dstArr[0].dstId;



                        angular.forEach($scope.dstList, function(dst, index) {
                            if (dst.branchId == $scope.selectbranch) {
                                var tempObject = {dstId: dst.dstId, dstName: dst.dstName};
                                $scope.dstArr.push(tempObject);
                            }

                        });
                        if (isExistingData == true) {
                            $scope.selectdst = sourceingData.dstId;
                        }

                        var foundBranch = false;
                        angular.forEach($scope.stateList, function(state, index) {
                            // alert(branch.productCode);
                            // alert($scope.selectProduct);
                            if (state.branchId == $scope.selectbranch) {
                                $scope.sourcestate = state.stateDesc;
                                $scope.sourcestateId = state.stateId;
                                foundBranch = true;
                            }
                        });
                        if (foundBranch == false) {
                            $scope.sourcestate = "";
                            $scope.sourcestateId = "0";
                        }

                        if ($scope.dsaArr.length > 1) {
                            $scope.dsaArr.splice(1, $scope.dsaArr.length);
                        }
                        $scope.selectdsa = $scope.dsaArr[0].brokerId;
                        angular.forEach($scope.dsaList, function(dsa, index) {
                            if (dsa.branchId == $scope.selectbranch) {
                                var tempObject = {brokerId: dsa.brokerId, brokerName: dsa.brokerName};
                                $scope.dsaArr.push(tempObject);
                            }

                        });
                        if (isExistingData == true) {
                            $scope.selectdsa = sourceingData.brokerId;
                        }
                        if ($scope.supplierArr.length > 1) {
                            $scope.supplierArr.splice(1, $scope.supplierArr.length);
                        }
                        $scope.supplier = $scope.supplierArr[0].supplierId;
                        angular.forEach($scope.supplierList, function(supplier, index) {
                            if (supplier.branchId == $scope.selectbranch) {
                                var tempObject = {supplierId: supplier.supplierId, supplierName: supplier.supplierName};
                                $scope.supplierArr.push(tempObject);
                            }

                        });

                        if (isExistingData == true) {
                            $scope.supplier = sourceingData.supplierId;
                            isExistingData = false;
                        }
                        //}
// else {
// $scope.dstArr = [{dstId: "0", dstName: 'Select DST'}];
// $scope.dsaArr = [{brokerId: "0", brokerName: 'Select DSA'}];
// $scope.supplierArr = [{supplierId: "0", supplierName: 'Select Supplier'}];
//                            
// }
                    }, true);

                    $scope.validateSourcingForm = function() {
                        if (!$scope.isGenAppId && ($scope.applicationformno == null || $scope.applicationformno == ""))
                        {
                            alert("Please Fill Application Form No");
                        } else if (
                                $scope.selectProduct == null || $scope.selectProduct == "0")
                        {
                            alert("Please Select Product");
                        }  else if (
                                $scope.selectschemegroup == null || $scope.selectschemegroup == "0")
                        {
                            alert("Please Select Scheme Group");
                        } else if (
                                $scope.selectschemedescription == null || $scope.selectschemedescription == "0")
                        {
                            alert("Please Select Scheme");
                        } else if (
                                $scope.selectpromotion == null || $scope.selectpromotion == "0")
                        {
                            alert("Please Select Promotion Code");
                        } else if ($scope.selectchannelcode == null || $scope.selectchannelcode == "0")
                        {
                            alert("Please Select Channel Code");
                        } else if ($scope.selectbranch == null || $scope.selectbranch == "0")
                        {
                            alert("Please Select Branch");
                        } else if ($scope.selectdst == null || $scope.selectdst == "0")
                        {
                            alert("Please Select DST");
                        }
                        else if ($scope.selectdsa == null || $scope.selectdsa == "0")
                        {
                            alert("Please Select DSA");
                        }
                        else if ($scope.supplier == null || $scope.supplier == "0")
                        {
                            alert("Please Select Supplier");
                        } else if ($scope.selectsource == null || $scope.selectsource == "0")
                        {
                            alert("Please Select Source");
                        }
                        //else if (
                        //      $scope.selectlabelcode== null || $scope.selectlabelcode == "null" || $scope.selectlabelcode == "" || $scope.selectlabelcode == "0")
                        //                       {
                        //                         alert("Please Select Label Code");
                        //                   } 
                        //else if (
                        //      $scope.selectlabelcode== null || $scope.selectlabelcode == "null" || $scope.selectlabelcode == "" || $scope.selectlabelcode == "0")
                        //                       {
                        //                         alert("Please Select Label Code");
                        //                   } 
//                            else if ($scope.selectlabelcode == null || $scope.selectlabelcode == "0")
//                        {
//                            alert("Please Select Label Code");
//                        }
                        else {
                            //alert($scope.selectlabelcode);
                            $scope.saveCheck();
                        }
                    }
                    $scope.submitSourcingForm = function() {

                        var callURL = $rootScope.urlDomain + "SaveSourcingData.json";
                        var data = {
                            datereciept: $scope.datereciept,
                            applicationFormNo: $scope.applicationformno,
                            selectschemedescription: $scope.selectschemedescription,
                            selectschemegroup: $scope.selectschemegroup,
                            selectpromotion: $scope.selectpromotion,
                            schId: $scope.schId,
                            selectbranch: $scope.selectbranch,
                            selectdst: $scope.selectdst,
                            sourcestate: $scope.sourcestateId,
                            selectdsa: $scope.selectdsa,
                            supplier: $scope.supplier,
                            selectchannelcode: $scope.selectchannelcode,
                            selectsource: $scope.selectsource,
                            selectlabelcode: $scope.selectlabelcode,
                            selectentity: $scope.selectentity,
                            selectProduct: $scope.selectProduct,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                //$cookies.a = data.applicationId;
                                //$rootScope.applicationId = data.applicationId;

                                if ($rootScope.progress < 1) {
                                    $rootScope.progress = 1;
                                }
                                $rootScope.setCookies(data.applicationId, $rootScope.progress);
                                if (data.status == "200" && data.isNewApplication) {
                                    alert("Application Id : " + $rootScope.applicationId + "  " + "Application Form No : " + data.applicationFormNo);
                                }

                                // $rootScope.openForm(1);
                                $location.path('/presanction/applicantdetail');
// alert(msg);
                            } else {
                                alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }




                }]).controller("ApplicantDetailsCtrl", ["$rootScope", "$cookies", "$scope", "$http", "$location", "$filter", "$modal", "$log", "$route", function($rootScope, $cookies, $scope, $http, $location, $filter, $modal, $log, $route) {


                    $scope.formReload = function() {
                        $route.reload();
                    };

                    $scope.saveCheck = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitApplicantDetailsForm();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $scope.today = function() {
                        var dtt = new Date;
                        var todayy = new Date(dtt.getFullYear() - 18, dtt.getMonth(), dtt.getDate());
                        return $scope.dt = todayy
                    };
                    $scope.showWeeks = !0;
                    $scope.toggleWeeks = function() {
                        return $scope.showWeeks = !$scope.showWeeks
                    };
                    $scope.clear = function() {
                        return $scope.dt = null
                    };
                    $scope.today();
                    var minnDate = new Date($scope.today().getFullYear() - 100, $scope.today().getMonth(), $scope.today().getDate());
                    $scope.minDate = minnDate;
                    $scope.disabled = function(date, mode) {
                        // return "day" === mode && (0 === date.getDay() || 6
                        // === date.getDay())
                        return "day" === mode && ($scope.dt < date)
                    };
                    $scope.toggleMin = function() {
                        var _ref;
                        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                            "null": $scope.today()
                        }
                    };
                    $scope.toggleMin();
                    $scope.open = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
                    };
                    var maxDate = $scope.today();
                    $scope.dateOptions = {
                        "year-format": "'yyyy'",
                        "starting-day": 1,
                        "show-weeks": false,
                        "datepicker-mode": 'year',
                        "year-range": 50,
                        "format-day-title": 'MMMM',
                        "format-month-title": 'yyyy',
                        "maxDate": maxDate

                    };

                    $scope.dob = null;
                    $scope.dob = maxDate;

                    $scope.formats = ["dd-MM-yyyy", "yyyy/MM/dd", "shortDate"];
                    $scope.format = $scope.formats[0];

                    $scope.coApplicantsArr = [];
                    $scope.guarantorArr = [];
                    $scope.deleteApplications = [];
                    $scope.isValidePAN = 'N';

                    $scope.$watch('coApplicantsArr', function(newVal, oldVal) {
                        angular.forEach(newVal, function(newValue, index) {
                            if (newValue.selectexistingcustomer == "0" || newValue.selectexistingcustomer == "N") {
                                newValue.existinglan = "";
                                //alert(newValue.existinglan);
                            }
                        });
                    }, true);

                    $scope.$watch('guarantorArr', function(newVal, oldVal) {
                        angular.forEach(newVal, function(newValue, index) {
                            if (newValue.selectexistingcustomer == "0" || newValue.selectexistingcustomer == "N") {
                                newValue.existinglan = "";
                                //alert(newValue.existinglan);
                            }
                        });
                    }, true);

                    $scope.coApplicants = function() {
                        $scope.coApplicantsArr.push({applicantId: "", selectekyccustomer: "N", selecttitle: "0", firstname: null,
                            middlename: null, lastname: null, selectmaritalstatus: "0", spousename: null,
                            fathername: null, applicationtype: "C", selectfinancialstatus: "0", selectrelation: "0",
                            dob: maxDate, selectgender: "0", uniquenumber: null, uniquenumberconfirm: null, panno: null,
                            pannoconfirm: null, voterid: null, voteridconfirm: null, passportno: null,
                            passportnoconfirm: null, drivinglicenceno: null, drivinglicencenoconfirm: null, selectoccupation: "0",
                            selectcustomercategory: "0", selectresidentialstatus: "0", selectexistingcustomer: "0",
                            existinglan: null, isValidePAN: "N", panTitle: null, panFirstName: null,
                            panMiddleName: null, panLastName: null});
                    };
                    $scope.guarantor = function() {
                        $scope.guarantorArr.push({applicantId: "", selectekyccustomer: "N", selecttitle: "0", firstname: null,
                            middlename: null, lastname: null, selectmaritalstatus: "0", spousename: null,
                            fathername: null, applicationtype: "G", selectfinancialstatus: "0", selectrelation: "0",
                            dob: maxDate, selectgender: "0", uniquenumber: null, uniquenumberconfirm: null, panno: null,
                            pannoconfirm: null, voterid: null, voteridconfirm: null, passportno: null,
                            passportnoconfirm: null, drivinglicenceno: null, drivinglicencenoconfirm: null, selectoccupation: "0",
                            selectcustomercategory: "0", selectresidentialstatus: "0", selectexistingcustomer: "0",
                            existinglan: null, isValidePAN: "N", panTitle: null, panFirstName: null,
                            panMiddleName: null, panLastName: null});
                        // $scope.$apply();
                    };

                    $scope.deleteCoApplicants = function(index) {
                        $scope.deleteApplications.push({applicantId: $scope.coApplicantsArr[index].applicantId});
                        $scope.coApplicantsArr.splice(index, 1);

                    }
                    $scope.deleteGuarantor = function(index) {
                        $scope.deleteApplications.push({applicantId: $scope.guarantorArr[index].applicantId});
                        $scope.guarantorArr.splice(index, 1);
                    }

                    $scope.verifyPan = function(applicantType, index, firstName, lastName, panNo, panNoConfirm) {
                        if (panNo == null || panNo == "") {
                            alert("Please enter PAN No.");
                            return;
                        }
                        if (panNoConfirm == null || panNoConfirm == "") {
                            alert("Please enter PAN No. Confirm");
                            return;
                        }

                        if (panNo != panNoConfirm) {
                            alert("PAN No. and PAN No. Confirm do not match");
                            return;
                        }

                        if (!$scope.validatePanNo(panNo))
                        {
                            alert("Please Enter Valid Pan Number");
                            return;
                        }

                        var data = {userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            firstName: firstName,
                            lastName: lastName,
                            pannoConfirm: panNo
                        };
                        var callURL = $rootScope.urlDomain + "VerifyPan.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            if (response.status == "200" && response.panInfoVO != null) {
                                if (applicantType == 'A') {
                                    $scope.isValidePAN = response.panInfoVO.isValidePAN;
                                    $scope.panTitle = response.panInfoVO.panTitle;
                                    $scope.panFirstName = response.panInfoVO.panFirstName;
                                    $scope.panMiddleName = response.panInfoVO.panMiddleName;
                                    $scope.panLastName = response.panInfoVO.panLastName;
                                } else if (applicantType == 'C') {
                                    $scope.coApplicantsArr[index].isValidePAN = response.panInfoVO.isValidePAN;
                                    $scope.coApplicantsArr[index].panTitle = response.panInfoVO.panTitle;
                                    $scope.coApplicantsArr[index].panFirstName = response.panInfoVO.panFirstName;
                                    $scope.coApplicantsArr[index].panMiddleName = response.panInfoVO.panMiddleName;
                                    $scope.coApplicantsArr[index].panLastName = response.panInfoVO.panLastName;
                                } else if (applicantType == 'G') {
                                    $scope.guarantorArr[index].isValidePAN = response.panInfoVO.isValidePAN;
                                    $scope.guarantorArr[index].panTitle = response.panInfoVO.panTitle;
                                    $scope.guarantorArr[index].panFirstName = response.panInfoVO.panFirstName;
                                    $scope.guarantorArr[index].panMiddleName = response.panInfoVO.panMiddleName;
                                    $scope.guarantorArr[index].panLastName = response.panInfoVO.panLastName;
                                }
                                alert(response.msg);
                            } else {
                                alert(response.msg);
                            }

                            console.log(response);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }


// $scope.isSingle = false;
// $scope.$watch('panno', function() {
// $scope.isValidePAN = 'N'
// });

                    $scope.$watch('selectexistingcustomer', function() {
                        //alert(isExistingData);
                        if ($scope.selectexistingcustomer == "N" || $scope.selectexistingcustomer == "0")
                            $scope.existinglan = "";
                    }, true);



                    $scope.preloadApplicantDetails = function() {
                    	
                    	$scope.defaultConstitution = "I";
                    	
                        $scope.ekycArr = [{ekycValue: "0", ekycDesc: 'Select EKYC'}, {ekycValue: "Y", ekycDesc: 'Yes'}, {ekycValue: "N", ekycDesc: 'No'}];
                        $scope.financialstatusArr = [{value: "0", desc: 'Select Financial Applicant'}, {value: "Y", desc: 'Yes'}, {value: "N", desc: 'No'}];
                        $scope.existingCustomerArr = [{value: "0", desc: 'Select Existing Customer'}, {value: "Y", desc: 'Yes'}, {value: "N", desc: 'No'}];
                        $scope.titleArr = [{titleValue: "0", titleDesc: 'Select Title'}];
                        $scope.constitutionArr = [{constitutionValue: "0", constitutionDesc: 'Select Constitution'}];
                        $scope.maritalArr = [{maritalValue: "0", maritalDesc: 'Select Marital Status'}];
                        $scope.relationArr = [{relationValue: "0", relationDesc: 'Select Relation'}];
                        $scope.genderArr = [{genderValue: "0", genderDesc: 'Select Gender'}];
                        $scope.occupationArr = [{constId: "0", constdesc: 'Select Occupation'}];
                        $scope.customercategoryArr = [{customerCatgId: "0", customerCatgDesc: 'Select Customer Category'}];
                        $scope.residentialstatusArr = [{value: "0", desc: 'Select Residential Status'}];
                        $scope.selectekyccustomer = 'N';


                        var data = {userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        var callURL = $rootScope.urlDomain + "FetchApplicantData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);

                            $scope.selectfinancialstatus = $scope.financialstatusArr[0].value;
                            $scope.selectexistingcustomer = $scope.existingCustomerArr[0].value;


                            angular.forEach(response.applicantDetailVO.lstTitle, function(branch, index) {
                                var tempObject = {titleValue: branch.value, titleDesc: branch.description};
                                $scope.titleArr.push(tempObject);
                            });

                            $scope.selecttitle = $scope.titleArr[0].titleValue;
                            
                            
                            angular.forEach(response.applicantDetailVO.lstConstitution, function(constitution, index) {
                                var tempObject = {constitutionValue: constitution.value, constitutionDesc: constitution.description};
                                $scope.constitutionArr.push(tempObject);
                            });

                            $scope.constitution = $scope.defaultConstitution;

                            angular.forEach(response.applicantDetailVO.lstMarital, function(branch, index) {
                                var tempObject = {maritalValue: branch.value, maritalDesc: branch.description};
                                $scope.maritalArr.push(tempObject);
                            });

                            $scope.selectmaritalstatus = $scope.maritalArr[0].maritalValue;

                            angular.forEach(response.applicantDetailVO.lstRelation, function(relation, index) {
                                var tempObject = {relationValue: relation.value, relationDesc: relation.description};
                                $scope.relationArr.push(tempObject);
                            });



                            angular.forEach(response.applicantDetailVO.lstGender, function(branch, index) {
                                var tempObject = {genderValue: branch.value, genderDesc: branch.description};
                                $scope.genderArr.push(tempObject);
                            });

                            $scope.selectgender = $scope.genderArr[0].genderValue;

                            angular.forEach(response.applicantDetailVO.lstOccupation, function(occupationInfo, index) {
                                var tempObject = {constId: occupationInfo.constId, constdesc: occupationInfo.constdesc};
                                $scope.occupationArr.push(tempObject);
                            });

                            $scope.selectoccupation = $scope.occupationArr[0].constId;

                            angular.forEach(response.applicantDetailVO.lstCustomerCategory, function(CustomerCategory, index) {
                                var tempObject = {customerCatgId: CustomerCategory.customerCatgId, customerCatgDesc: CustomerCategory.customerCatgDesc};
                                $scope.customercategoryArr.push(tempObject);
                            });

                            $scope.selectcustomercategory = $scope.customercategoryArr[0].customerCatgId;

                            angular.forEach(response.applicantDetailVO.lstResidentialStatus, function(residentialStatus, index) {
                                var tempObject = {value: residentialStatus.value, desc: residentialStatus.description};
                                $scope.residentialstatusArr.push(tempObject);
                            });

                            $scope.selectresidentialstatus = $scope.residentialstatusArr[0].value;

//                            $scope.passportnoconfirm ="";


                            if (response.primaryApplicant != null) {
                                $scope.applicantId = response.primaryApplicant.applicantsId,
                                        $scope.selectekyccustomer = response.primaryApplicant.isEkycCustomer,
                                        $scope.selecttitle = response.primaryApplicant.title,
                                        $scope.constitution = response.primaryApplicant.constitution,
                                        $scope.firstname = response.primaryApplicant.firstName,
                                        $scope.middlename = response.primaryApplicant.middleName,
                                        $scope.lastname = response.primaryApplicant.lastName,
                                        $scope.selectmaritalstatus = response.primaryApplicant.maritalStatus,
                                        $scope.spousename = $scope.nullValidation(response.primaryApplicant.spousesName),
                                        $scope.fathername = response.primaryApplicant.fatherName,
                                        $scope.applicationtype = response.primaryApplicant.applicationType,
                                        $scope.selectfinancialstatus = response.primaryApplicant.financialStatus + "",
                                        $scope.selectrelation = response.primaryApplicant.relation,
                                        $scope.dob = response.primaryApplicant.dob,
                                        $scope.selectgender = response.primaryApplicant.gender,
                                        $scope.uniquenumber = $scope.nullValidation(response.primaryApplicant.uid),
                                        $scope.uniquenumberconfirm = $scope.nullValidation(response.primaryApplicant.uid),
                                        $scope.panno = $scope.nullValidation(response.primaryApplicant.panNo),
                                        $scope.pannoconfirm = $scope.nullValidation(response.primaryApplicant.panNo),
                                        $scope.voterid = $scope.nullValidation(response.primaryApplicant.voterIdNo),
                                        $scope.voteridconfirm = $scope.nullValidation(response.primaryApplicant.voterIdNo),
                                        $scope.passportno = $scope.nullValidation(response.primaryApplicant.passportNo),
                                        $scope.passportnoconfirm = $scope.nullValidation(response.primaryApplicant.passportNo),
                                        $scope.drivinglicenceno = $scope.nullValidation(response.primaryApplicant.drivingLicenseNo),
                                        $scope.drivinglicencenoconfirm = $scope.nullValidation(response.primaryApplicant.drivingLicenseNo),
                                        $scope.selectoccupation = response.primaryApplicant.constId,
                                        $scope.selectcustomercategory = response.primaryApplicant.customerCatgId,
                                        $scope.selectresidentialstatus = response.primaryApplicant.residentialStatus,
                                        $scope.selectexistingcustomer = response.primaryApplicant.isExistingCustomer,
                                        //issue 0
                                        $scope.existinglan = $scope.nullValidation(response.primaryApplicant.existingLan),
                                        $scope.isValidePAN = response.primaryApplicant.panIsValide,
                                        $scope.panTitle = $scope.nullValidation(response.primaryApplicant.panTitle),
                                        $scope.panFirstName = $scope.nullValidation(response.primaryApplicant.panFirstName),
                                        $scope.panMiddleName = $scope.nullValidation(response.primaryApplicant.panMiddleName),
                                        $scope.panLastName = $scope.nullValidation(response.primaryApplicant.panLastName)
                            }


                            if (response.lstCoApplicantsDetails != null && response.lstCoApplicantsDetails.length > 0) {
                                //$scope.coApplicantsArr = [];

                                angular.forEach(response.lstCoApplicantsDetails, function(coApplicantsDetails, index) {
                                    var tempObjectCoApp = {
                                        applicantId: coApplicantsDetails.applicantsId,
                                        selectekyccustomer: coApplicantsDetails.isEkycCustomer,
                                        selecttitle: coApplicantsDetails.title,
                                        constitution : coApplicantsDetails.constitution,
                                        firstname: coApplicantsDetails.firstName,
                                        middlename: $scope.nullValidation(coApplicantsDetails.middleName),
                                        lastname: coApplicantsDetails.lastName,
                                        selectmaritalstatus: coApplicantsDetails.maritalStatus,
                                        spousename: $scope.nullValidation(coApplicantsDetails.spousesName),
                                        fathername: coApplicantsDetails.fatherName,
                                        applicationtype: coApplicantsDetails.applicantType,
                                        selectfinancialstatus: coApplicantsDetails.financialStatus,
                                        selectrelation: coApplicantsDetails.relation,
                                        dob: coApplicantsDetails.dob,
                                        selectgender: coApplicantsDetails.gender,
                                        uniquenumber: $scope.nullValidation(coApplicantsDetails.uid),
                                        uniquenumberconfirm: $scope.nullValidation(coApplicantsDetails.uid),
                                        panno: $scope.nullValidation(coApplicantsDetails.panNo),
                                        pannoconfirm: $scope.nullValidation(coApplicantsDetails.panNo),
                                        voterid: $scope.nullValidation(coApplicantsDetails.voterIdNo),
                                        voteridconfirm: $scope.nullValidation(coApplicantsDetails.voterIdNo),
                                        passportno: $scope.nullValidation(coApplicantsDetails.passportNo),
                                        passportnoconfirm: $scope.nullValidation(coApplicantsDetails.passportNo),
                                        drivinglicenceno: $scope.nullValidation(coApplicantsDetails.drivingLicenseNo),
                                        drivinglicencenoconfirm: $scope.nullValidation(coApplicantsDetails.drivingLicenseNo),
                                        selectoccupation: coApplicantsDetails.constId,
                                        selectcustomercategory: coApplicantsDetails.customerCatgId,
                                        selectresidentialstatus: coApplicantsDetails.residentialStatus,
                                        selectexistingcustomer: coApplicantsDetails.isExistingCustomer,
                                        //issue 0
                                        existinglan: $scope.nullValidation(coApplicantsDetails.existingLan),
                                        isValidePAN: coApplicantsDetails.panIsValide,
                                        panTitle: $scope.nullValidation(coApplicantsDetails.panTitle),
                                        panFirstName: $scope.nullValidation(coApplicantsDetails.panFirstName),
                                        panMiddleName: $scope.nullValidation(coApplicantsDetails.panMiddleName),
                                        panLastName: $scope.nullValidation(coApplicantsDetails.panLastName)
                                    };
                                    $scope.coApplicantsArr.push(tempObjectCoApp);
                                });

                            }

                            if (response.lstGuarantor != null && response.lstGuarantor.length > 0) {
                                // $scope.guarantorArr = [];

                                angular.forEach(response.lstGuarantor, function(Guarantor, index) {
                                    var tempObjectGuar = {
                                        applicantId: Guarantor.applicantsId,
                                        selectekyccustomer: Guarantor.isEkycCustomer,
                                        selecttitle: Guarantor.title,
                                        constitution : Guarantor.constitution,
                                        firstname: Guarantor.firstName,
                                        middlename: $scope.nullValidation(Guarantor.middleName),
                                        lastname: Guarantor.lastName,
                                        selectmaritalstatus: Guarantor.maritalStatus,
                                        spousename: $scope.nullValidation(Guarantor.spousesName),
                                        fathername: Guarantor.fatherName,
                                        applicationtype: Guarantor.applicantType,
                                        selectfinancialstatus: Guarantor.financialStatus,
                                        selectrelation: Guarantor.relation,
                                        dob: Guarantor.dob,
                                        selectgender: Guarantor.gender,
                                        uniquenumber: $scope.nullValidation(Guarantor.uid),
                                        uniquenumberconfirm: $scope.nullValidation(Guarantor.uid),
                                        panno: $scope.nullValidation(Guarantor.panNo),
                                        pannoconfirm: $scope.nullValidation(Guarantor.panNo),
                                        voterid: $scope.nullValidation(Guarantor.voterIdNo),
                                        voteridconfirm: $scope.nullValidation(Guarantor.voterIdNo),
                                        passportno: $scope.nullValidation(Guarantor.passportNo),
                                        passportnoconfirm: $scope.nullValidation(Guarantor.passportNo),
                                        drivinglicenceno: $scope.nullValidation(Guarantor.drivingLicenseNo),
                                        drivinglicencenoconfirm: $scope.nullValidation(Guarantor.drivingLicenseNo),
                                        selectoccupation: Guarantor.constId,
                                        selectcustomercategory: Guarantor.customerCatgId,
                                        selectresidentialstatus: Guarantor.residentialStatus,
                                        selectexistingcustomer: Guarantor.isExistingCustomer,
                                        //issue 0
                                        existinglan: $scope.nullValidation(Guarantor.existingLan),
                                        isValidePAN: $scope.nullValidation(Guarantor.panIsValide),
                                        panTitle: $scope.nullValidation(Guarantor.panTitle),
                                        panFirstName: $scope.nullValidation(Guarantor.panFirstName),
                                        panMiddleName: $scope.nullValidation(Guarantor.panMiddleName),
                                        panLastName: $scope.nullValidation(Guarantor.panLastName)
                                    };
                                    $scope.guarantorArr.push(tempObjectGuar);
                                });

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };
                    $scope.isUniqueNumberidEmpty = function(uniquenumber) {
                        if (uniquenumber != null && uniquenumber != "") {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.isPannoEmpty = function(panno) {
                        if (panno != null && panno != "") {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.isVoteridEmpty = function(voterid) {
//                    	alert("hi");
//                    	alert(voterid);
//                    	alert(voterid != null);
//                    	alert(voterid != "");
                        if (voterid != null && voterid != "") {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.isPassportnoEmpty = function(passportno) {
                        if (passportno != null && passportno != "") {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.isDrivinglicencenoidEmpty = function(drivinglicenceno) {
                        if (drivinglicenceno != null && drivinglicenceno != "") {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.isIdProofValid = function(uniquenumber, panno, voterid, passportno, drivinglicenceno) {

                        var retVal = false;
                        var retVal1, retVal2, retVal3, retVal4, retVal5;
                        // alert($scope.uniquenumber);

                        retVal1 = $scope.isUniqueNumberidEmpty(uniquenumber);
                        retVal2 = $scope.isPannoEmpty(panno);
                        retVal3 = $scope.isVoteridEmpty(voterid);
                        retVal4 = $scope.isPassportnoEmpty(passportno);
                        retVal5 = $scope.isDrivinglicencenoidEmpty(drivinglicenceno);

                        return retVal = retVal1 || retVal2 || retVal3 || retVal4 || retVal5;


                    };
                    $scope.validatePanNo = function(panNo) {
                        var panPat = /^([a-zA-Z]{3})([A,a,B,a,C,c,F,f,H,h,J,j,L,l,P,p,R,r]{1})([a-zA-Z]{1})(\d{4})([a-zA-Z]{1})$/;
                        return panPat.test(panNo);
                    }
                    $scope.nullValidation = function(val) {
                        if (angular.isUndefined(val) || val === null || val === "null") {
                            return "";
                        } else {
                            return val;
                        }
                    }

                    $scope.checkName = function(name) {
                        var regEx = /^[a-zA-Z'.,]{0,}$/;
                        return regEx.test(name);
                    }

                    $scope.checkNameWithSpace = function(name) {
                        var regEx = /^[a-zA-Z'.,\s]{0,}$/;
                        return regEx.test(name);
                    }
                    $scope.validateNumber = function(checkAmount) {
                        var reg = /^[^0\W][\d]*$/;
                        return reg.test(checkAmount);
                    }
                    
                    $scope.checkPassportId = function(passId) {
                        var regEx = /^([a-zA-z]{1}\d{7}$)/;
                        return regEx.test(passId);
                    }
                    
                    $scope.checkVoterId = function(voterId) {
                        var regEx = /^([a-zA-Z]{2}[a-zA-z\d\/]{1}[\d]{0,})$/;
                        return regEx.test(voterId);
                    }
                    
                    /*
                     * $scope.calculateAge = function calculateAge(birthday) { //
                     * birthday is a date var ageDifMs = Date.now() -
                     * birthday.getTime(); var ageDate = new Date(ageDifMs); //
                     * miliseconds from epoch return
                     * Math.abs(ageDate.getUTCFullYear() - 1970); }
                     */

                    $scope.submitApplicantDetailsForm = function() {


                        var callURL = $rootScope.urlDomain + "SaveApplicantData.json";
                        var _date = $filter('date')($scope.dob, 'dd-MM-yyyy');

                        var applicantsArr = [{
                                applicantId: $scope.applicantId,
                                selectekyccustomer: $scope.selectekyccustomer,
                                selecttitle: $scope.selecttitle,
                                constitution: $scope.constitution,
                                firstname: $scope.firstname,
                                middlename: $scope.middlename,
                                lastname: $scope.lastname,
                                selectmaritalstatus: $scope.selectmaritalstatus,
                                spousename: $scope.spousename,
                                fathername: $scope.fathername,
                                applicationtype: "P",
                                selectfinancialstatus: $scope.selectfinancialstatus,
                                selectrelation: "",
                                dob: _date,
                                selectgender: $scope.selectgender,
                                uniquenumberconfirm: $scope.uniquenumber,
                                pannoconfirm: $scope.panno,
                                voteridconfirm: $scope.voterid,
                                passportnoconfirm: $scope.passportno,
                                drivinglicencenoconfirm: $scope.drivinglicenceno,
                                selectoccupation: $scope.selectoccupation,
                                selectcustomercategory: $scope.selectcustomercategory,
                                selectresidentialstatus: $scope.selectresidentialstatus,
                                selectexistingcustomer: $scope.selectexistingcustomer,
                                existinglan: $scope.existinglan,
                                isValidePAN: $scope.isValidePAN,
                                panTitle: $scope.panTitle,
                                panFirstName: $scope.panFirstName,
                                panMiddleName: $scope.panMiddleName,
                                panLastName: $scope.panLastName

                            }];



                        if ($scope.coApplicantsArr.length) {
                            angular.forEach($scope.coApplicantsArr, function(coApplicantsInfo, index) {
                                $scope.coApplicantsArr[index].dob = $filter('date')(coApplicantsInfo.dob, 'dd-MM-yyyy');
                            });
                        }

                        if ($scope.guarantorArr.length) {
                            angular.forEach($scope.guarantorArr, function(guarantorsInfo, index) {
                                $scope.guarantorArr[index].dob = $filter('date')(guarantorsInfo.dob, 'dd-MM-yyyy');
                            });
                        }

                        applicantsArr = $.merge(applicantsArr, $scope.coApplicantsArr);
                        applicantsArr = $.merge(applicantsArr, $scope.guarantorArr);



                        console.log(applicantsArr);
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantsArr: applicantsArr,
                            deleteApplications: $scope.deleteApplications};

                        // alert(data);
                        $http({
                            method: "POST",
                            url: callURL,
                            // transformRequest: transformRequestAsFormPost,
                            data: data
                        }).success(function(data) {
                            console.log(data.status + " :  ");
                            if (data.status == "200") {
                                //$cookies.a = data.applicationId;
                                //$rootScope.applicationId = data.applicationId;

                                if ($rootScope.progress < 2) {
                                    $rootScope.progress = 2;
                                }
                                $rootScope.setCookies(data.applicationId, $rootScope.progress);
                                $location.path('/presanction/addressdetail');
                            } else {
                                alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });

                    }


                    $scope.innerValidateApplicantDetailsForm = function(applicantElement, applicantType) {

                        var isReadyToSubmit = true;

                        if (applicantElement.constitution == null || applicantElement.constitution == "" || applicantElement.constitution == "0")
                        {
                            alert("Please Select Constitution");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectekyccustomer == null || applicantElement.selectekyccustomer == "")
                        {
                            alert("Please Select Ekyc Customer");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selecttitle == null || applicantElement.selecttitle == "0")
                        {
                            isReadyToSubmit = false;
                            alert("Please Select Title");

                        }
                        else if (applicantElement.firstname == null || applicantElement.firstname == "")
                        {
                            alert("Please Fill First Name");
                            isReadyToSubmit = false;
                        } else if (!$scope.checkName(applicantElement.firstname))
                        {
                            alert("Number and Special charecter not allowed for  First Name ");
                            isReadyToSubmit = false;
                        } else if (applicantElement.middlename != null && applicantElement.middlename.length > 0 && !$scope.checkName(applicantElement.middlename))
                        {
                            alert("Number and Special charecter not allowed for Middle Name");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.lastname == null || applicantElement.lastname == "")
                        {
                            alert("Please Fill Last Name");
                            isReadyToSubmit = false;
                        } else if (!$scope.checkName(applicantElement.lastname))
                        {
                            alert("Number and Special charecter not allowed for  Last Name ");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectmaritalstatus == null || applicantElement.selectmaritalstatus == "0")
                        {
                            alert("Please Select Marital Status");
                            isReadyToSubmit = false;
                        } else if (applicantElement.selectmaritalstatus == 'M' &&
                                (applicantElement.spousename != null && applicantElement.spousename != "" && applicantElement.spousename.length > 0 &&
                                        (!$scope.checkNameWithSpace(applicantElement.spousename))))
                        {
                            alert("Number and Special charecter not allowed for Spouse Name");
                            isReadyToSubmit = false;
                        }
//                         else if (applicantElement.selectmaritalstatus == 'M' && (!$scope.fatherName(applicantElement.spousename)))
//                         {
//                             alert("Number and Special charecter not allowed for Spouse Name");
//                             isReadyToSubmit = false;
//                         }
                        else if (applicantElement.fathername == null || applicantElement.fathername == "")
                        {
                            alert("Please Fill Father Name");
                            isReadyToSubmit = false;
                        } else if (!$scope.checkNameWithSpace(applicantElement.fathername))
                        {
                            alert("Number and Special charecter not allowed for  Father Name ");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectfinancialstatus == null || applicantElement.selectfinancialstatus == "0")
                        {
                            alert("Please Select Financial Applicant");
                            isReadyToSubmit = false;
                        } 
                        else if (applicantType != 'P' && applicantElement.selectrelation == null || applicantElement.selectrelation == "0")
                        {
                            alert("Please Select Relation");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.dob == null || applicantElement.dob == "")
                        {
                            alert("Please Select Valid Date of Birth");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectgender == null || applicantElement.selectgender == "0")
                        {
                            alert("Please Select Gender");
                            isReadyToSubmit = false;
                        } else if (applicantElement.selectoccupation == null || applicantElement.selectoccupation == "0")
                        {
                            alert("Please Select Occupation");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectcustomercategory == null || applicantElement.selectcustomercategory == "0")
                        {
                            alert("Please Select Customer Category");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectresidentialstatus == null || applicantElement.selectresidentialstatus == "0")
                        {
                            alert("Please Select Residential Status");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectexistingcustomer == null || applicantElement.selectexistingcustomer == "0")
                        {
                            alert("Please Select Existing Customer");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectexistingcustomer == 'Y' && applicantElement.existinglan == null)
                        {
                            alert("Please Fill Loan Account Number");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectexistingcustomer == 'Y' && isNaN(applicantElement.existinglan))
                        {
                            alert("Loan Account Number Must Be a Number");
                            isReadyToSubmit = false;
                        } else if (applicantElement.selectexistingcustomer == 'Y' && !$scope.validateNumber(applicantElement.existinglan))
                        {
                            alert("Loan Account Number should not start with 0 and it should not contain Decimal value,Alphabets and Special Characters");
                            isReadyToSubmit = false;
                        }
                        else if (applicantElement.selectexistingcustomer == 'Y' && applicantElement.existinglan.length != 7)
                        {
                            alert("Loan Account Number Must Be a 7 digit number");
                            isReadyToSubmit = false;
                        }
                        else if (!$scope.isIdProofValid(applicantElement.uniquenumber, applicantElement.panno, applicantElement.voterid, applicantElement.passportno, applicantElement.drivinglicenceno)) {
                            alert(" please fill atleast one id proof");
                            isReadyToSubmit = false;
                        } else {

                            if (applicantElement.uniquenumber != null && applicantElement.uniquenumber != "" && isReadyToSubmit) {

                                if (isNaN(applicantElement.uniquenumber))
                                {
                                    alert(" Aadhaar Number Must Be a Number");
                                    isReadyToSubmit = false;
                                }
                                else if (applicantElement.uniquenumber.length != 12)
                                {
                                    alert("Please Enter 12 Digit Aadhaar Number");
                                    isReadyToSubmit = false;
                                }

                            }

                            if (applicantElement.uniquenumber !== applicantElement.uniquenumberconfirm && isReadyToSubmit)
                            {
                                alert("Please Match Aadhaar Number");
                                isReadyToSubmit = false;
                            }

                            if (applicantElement.panno != null && applicantElement.panno != "" && isReadyToSubmit) {

                                if (!$scope.validatePanNo(applicantElement.panno))
                                {
                                    alert("Please Enter Valid Pan Number");
                                    isReadyToSubmit = false;
                                }
                                else if (applicantElement.isValidePAN == "N") {
                                    alert("PAN No not verified for Applicant");
                                    isReadyToSubmit = false;
                                }



                            }

                            if (applicantElement.panno !== applicantElement.pannoconfirm && isReadyToSubmit)
                            {
                                alert("Please Match Pan Card Number");
                                isReadyToSubmit = false;
                            }
                            
                            if (applicantElement.voterid != null && applicantElement.voterid != "" && isReadyToSubmit) {
                            	
                            	 if (applicantElement.voterid.length < 10)
                                {
                                    alert("Voter Id Should contain minimum 10 digit ");
                                    isReadyToSubmit = false;
                                }
                            	
                            	 else if (!$scope.checkVoterId(applicantElement.voterid))
                                {
                                    alert(" Please enter valid Voter Id");
                                    isReadyToSubmit = false;
                                }
                                

                            }

                            if (applicantElement.voterid !== applicantElement.voteridconfirm && isReadyToSubmit)
                            {
                                alert("Please Match Voter ID Card Number");
                                isReadyToSubmit = false;
                            }
                            
                            if (applicantElement.passportno != null && applicantElement.passportno != "" && isReadyToSubmit) {
                            	
                            	
                            	 if (applicantElement.passportno.length < 7)
                                {
                                    alert("Passport Number Should contain minimum  7 digit ");
                                    isReadyToSubmit = false;
                                }
                            	
                            	 else if (!$scope.checkPassportId(applicantElement.passportno))
                                {
                                    alert(" Please enter valid Passport Number");
                                    isReadyToSubmit = false;
                                }
                                

                            }
//                            if (applicantElement.passportnoconfirm != null && applicantElement.passportnoconfirm != "" && isReadyToSubmit) {
//                            	
//                            	
//                           	 if (applicantElement.passportnoconfirm.length < 7)
//                               {
//                                   alert("Passport Number Should contain minimum  7 digit ");
//                                   isReadyToSubmit = false;
//                               }
//                           	
//                           	 else if (!$scope.checkPassportId(applicantElement.passportnoconfirm))
//                               {
//                                   alert(" Please enter valid Passport Number");
//                                   isReadyToSubmit = false;
//                               }
//                               
//
//                           }
                            
                            if (applicantElement.passportno !== applicantElement.passportnoconfirm && isReadyToSubmit)
                            {
                                alert("Please Match Passport Number");
                                isReadyToSubmit = false;
                            }

                            if (applicantElement.drivinglicenceno !== applicantElement.drivinglicencenoconfirm && isReadyToSubmit)
                            {
                                alert("Please Match Driving Licence");
                                isReadyToSubmit = false;
                            }


                            if (applicantElement.existinglan == null)
                            {
                                applicantElement.existinglan = "";
                            }

                        }

                        return isReadyToSubmit;

                    }

                    $scope.validateApplicantDetailsForm = function() {

                        var isReadyToSubmit = true;
                        isReadyToSubmit = $scope.innerValidateApplicantDetailsForm($scope,'P');


                        if (isReadyToSubmit) {

                            angular.forEach($scope.coApplicantsArr, function(coApplicants, index) {

                                if (isReadyToSubmit) {
                                    isReadyToSubmit = $scope.innerValidateApplicantDetailsForm(coApplicants,'C');
                                }

                            });

                        }
                        if (isReadyToSubmit) {

                            angular.forEach($scope.guarantorArr, function(guarantor, index) {

                                if (isReadyToSubmit) {
                                    isReadyToSubmit = $scope.innerValidateApplicantDetailsForm(guarantor,'G');
                                }

                            });

                        }

                        if (isReadyToSubmit) {
                            $scope.saveCheck();
                        }


                    }


                }]).controller("AddressDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$modal", "$log", "$route", function($cookies, $rootScope, $scope, $http, $location, $modal, $log, $route) {


                    $scope.formReload = function() {
                        $route.reload();
                    };



                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitAddressForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $scope.PermanentChangeValue = function(index) {
                        // alert(index);
                        if ($scope.addressParentArr[index].permanent.sameAs == "O") {
                            $scope.addressParentArr[index].permanent.isFormShow = true;
                        } else {
                            $scope.addressParentArr[index].permanent.isFormShow = false;
                        }
                    }

                    $scope.OfficeChangeValue = function(index) {
                        if ($scope.addressParentArr[index].office.sameAs == "O") {
                            $scope.addressParentArr[index].office.isFormShow = true;
                        } else {
                            $scope.addressParentArr[index].office.isFormShow = false;
                        }
                    }

                    $scope.addressParentArr = [];

                    $scope.ownershipArr = [{value: "0", description: 'Select Ownership Status'}];
                    $scope.stateArr = [{stateId: "0", stateDesc: 'Select State'}];

                    $scope.cityDefObj = {cityId: "0", cityName: 'Select City'};
                    $scope.pinCodeDefObj = {zipcode: "0", zipdesc: 'Select Pin Code'};
                    $scope.cityList = "";
                    $scope.pincodeList = "";

                    $scope.preloadAddressDetails = function() {




                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        var callURL = $rootScope.urlDomain + "FetchAddressData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data

                        }).success(function(response) {
                            console.log(response);
                            angular.forEach(response.addressDetailVO.lstOwnertype, function(ownership, index) {
                                var tempObject = {value: ownership.value, description: ownership.description};
                                $scope.ownershipArr.push(tempObject);
                            });

                            angular.forEach(response.addressDetailVO.lstState, function(state, index) {
                                var tempObject = {stateId: state.stateId, stateDesc: state.stateDesc};
                                $scope.stateArr.push(tempObject);
                            });
                            //$scope.selectstate = $scope.stateArr[0].stateId;
                            $scope.cityList = response.addressDetailVO.lstCity;
                            $scope.pincodeList = response.addressDetailVO.lstPincode;
                            if (response.data != null) {

                                var allResArr = "";
                                var isExistingData = false;
                                angular.forEach(response.data, function(item, parentIndex) {

                                    var current = {};
                                    var NA = {};
                                    var permanent = {};
                                    var office = {};
                                    var mailingaddressTemp = '';
                                    var communicationnoTemp = '';
                                    current.ownertype = $scope.ownershipArr[0].value;
                                    current.selectstate = $scope.stateArr[0].stateId;
                                    permanent.ownertype = $scope.ownershipArr[0].value;
                                    permanent.selectstate = $scope.stateArr[0].stateId;
                                    office.ownertype = $scope.ownershipArr[0].value;
                                    office.selectstate = $scope.stateArr[0].stateId;
                                    allResArr = item.lstAddressDetails;


                                    current.cityArr = [];
                                    current.cityArr.push($scope.cityDefObj);
                                    current.selectcity = current.cityArr[0].cityId;
                                    permanent.cityArr = [];
                                    permanent.cityArr.push($scope.cityDefObj);
                                    permanent.selectcity = permanent.cityArr[0].cityId;
                                    office.cityArr = [];
                                    office.cityArr.push($scope.cityDefObj);
                                    office.selectcity = office.cityArr[0].cityId;

                                    current.pinCodeArr = [];
                                    current.pinCodeArr.push($scope.pinCodeDefObj);
                                    current.selectpincode = current.pinCodeArr[0].zipcode;

                                    permanent.pinCodeArr = [];
                                    permanent.pinCodeArr.push($scope.pinCodeDefObj);
                                    permanent.selectpincode = permanent.pinCodeArr[0].zipcode;

                                    office.pinCodeArr = [];
                                    office.pinCodeArr.push($scope.pinCodeDefObj);
                                    office.selectpincode = office.pinCodeArr[0].zipcode;

                                    angular.forEach(item.lstAddressDetails, function(addressElement, index) {

                                        isExistingData = true;

                                        if (addressElement.addressType == 'C') {

                                            current.addressId = addressElement.addressId;
                                            current.ownertype = addressElement.ownertype;
                                            current.addressline1 = addressElement.addressLine1;
                                            current.addressline2 = addressElement.addressLine2;
                                            current.addressline3 = addressElement.addressLine3;
                                            current.addressline4 = addressElement.addressLine4;
                                            current.selectstate = addressElement.state;
                                            angular.forEach($scope.cityList, function(city, index) {
                                                if (city.stateId == current.selectstate) {
                                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                    current.cityArr.push(tempObject);
                                                }
                                            });

                                            current.selectcity = addressElement.city;
                                            angular.forEach($scope.pincodeList, function(pin, index) {
                                                if (pin.cityId == current.selectcity) {
                                                    var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                    current.pinCodeArr.push(tempObject);
                                                }
                                            });
                                            current.selectpincode = addressElement.pincode;
                                            current.stdcode = addressElement.stdCode;
                                            current.phone1 = addressElement.phone1;
                                            current.mobile = addressElement.mobile;
                                            current.landmark = addressElement.landmark;
                                            current.currentaddressyears = addressElement.yearsAtCurrentAddress + "";
                                            current.currentaddressmonths = addressElement.monthsAtCurrentAddress + "";
                                            current.cityyears = addressElement.yearsAtCity + "";
                                            current.citymonths = addressElement.monthsAtCity + "";
                                            current.emailid = addressElement.emailId;
                                            current.addressType = addressElement.addressType;
                                            current.sameAs = addressElement.sameAs;

                                            if (addressElement.isCommunicationNum == 'Y') {
                                                communicationnoTemp = 'C';
                                            }
                                            if (addressElement.isMailingAddress == 'Y') {
                                                mailingaddressTemp = 'C';
                                            }

                                        } else if (addressElement.addressType == 'P') {

                                            permanent.addressId = addressElement.addressId;
                                            permanent.ownertype = addressElement.ownertype;
                                            permanent.addressline1 = addressElement.addressLine1;
                                            permanent.addressline2 = addressElement.addressLine2;
                                            permanent.addressline3 = addressElement.addressLine3;
                                            permanent.addressline4 = addressElement.addressLine4;
                                            permanent.selectstate = addressElement.state;
                                            angular.forEach($scope.cityList, function(city, index) {
                                                if (city.stateId == permanent.selectstate) {
                                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                    permanent.cityArr.push(tempObject);
                                                    //console.log("permanent"+city.cityId);
                                                }
                                            });
                                            console.log("here office" + addressElement.city);
                                            permanent.selectcity = addressElement.city;
                                            angular.forEach($scope.pincodeList, function(pin, index) {
                                                if (pin.cityId == permanent.selectcity) {
                                                    var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                    permanent.pinCodeArr.push(tempObject);
                                                }
                                            });
                                            permanent.selectpincode = addressElement.pincode;
                                            permanent.stdcode = addressElement.stdCode;
                                            permanent.phone1 = addressElement.phone1;
                                            permanent.mobile = addressElement.mobile;
                                            permanent.landmark = addressElement.landmark;
                                            permanent.currentaddressyears = addressElement.yearsAtCurrentAddress;
                                            permanent.currentaddressmonths = addressElement.monthsAtCurrentAddress;
                                            permanent.cityyears = addressElement.yearsAtCity;
                                            permanent.citymonths = addressElement.monthsAtCity;
                                            permanent.emailid = addressElement.emailId;
                                            permanent.addressType = addressElement.addressType;
                                            if (addressElement.sameAs == null) {
                                                permanent.sameAs = 'O';
                                            } else {
                                                permanent.sameAs = addressElement.sameAs;
                                            }

                                            if (addressElement.isCommunicationNum == 'Y') {
                                                communicationnoTemp = 'P';
                                            }
                                            if (addressElement.isMailingAddress == 'Y') {
                                                mailingaddressTemp = 'P';
                                            }


                                        } else if (addressElement.addressType == 'F') {

                                            office.addressId = addressElement.addressId;
                                            office.ownertype = addressElement.ownertype;
                                            office.addressline1 = addressElement.addressLine1;
                                            office.addressline2 = addressElement.addressLine2;
                                            office.addressline3 = addressElement.addressLine3;
                                            office.addressline4 = addressElement.addressLine4;
                                            office.selectstate = addressElement.state;
                                            angular.forEach($scope.cityList, function(city, index) {
                                                if (city.stateId == office.selectstate) {
                                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                    office.cityArr.push(tempObject);
                                                }
                                            });
                                            office.selectcity = addressElement.city;
                                            angular.forEach($scope.pincodeList, function(pin, index) {
                                                if (pin.cityId == office.selectcity) {
                                                    var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                    office.pinCodeArr.push(tempObject);
                                                }
                                            });
                                            office.selectpincode = addressElement.pincode;
                                            office.stdcode = addressElement.stdCode;
                                            office.phone1 = addressElement.phone1;
                                            office.mobile = addressElement.mobile;
                                            office.landmark = addressElement.landmark;
                                            office.currentaddressyears = addressElement.yearsAtCurrentAddress;
                                            office.currentaddressmonths = addressElement.monthsAtCurrentAddress;
                                            office.cityyears = addressElement.yearsAtCity;
                                            office.citymonths = addressElement.monthsAtCity;
                                            office.emailid = addressElement.emailId;
                                            office.addressType = addressElement.addressType;
                                            if (addressElement.sameAs == null) {
                                                office.sameAs = 'O';
                                            } else {
                                                office.sameAs = addressElement.sameAs;
                                            }

                                            if (addressElement.isCommunicationNum == 'Y') {
                                                communicationnoTemp = 'F';
                                            }
                                            if (addressElement.isMailingAddress == 'Y') {
                                                mailingaddressTemp = 'F';
                                            }

                                        }
                                    });




                                    $scope.$watch('addressParentArr', function(newVal, oldVal) {
                                        //alert(newVal);
                                        //      console.log(newVal[0].permanent.selectstate);
                                        angular.forEach(newVal, function(addr, index) {
                                            console.log("-" + addr.current.selectstate + "-" + oldVal[index].current.selectstate);
                                            if (addr.current.selectstate != oldVal[index].current.selectstate) {
                                                //alert(addr.current.selectstate);
                                                if (addr.current.cityArr.length > 1) {
                                                    addr.current.cityArr.splice(1, addr.current.cityArr.length);
                                                }
                                                addr.current.selectcity = addr.current.cityArr[0].cityId;
                                                angular.forEach($scope.cityList, function(city, index) {
                                                    if (city.stateId == addr.current.selectstate) {
                                                        var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                        addr.current.cityArr.push(tempObject);
                                                    }
                                                });
                                                console.log(addr.current.pinCodeArr.length);
                                                if (addr.current.pinCodeArr.length > 1) {
                                                    addr.current.pinCodeArr.splice(1, addr.current.pinCodeArr.length);
                                                    addr.current.selectpincode = addr.current.pinCodeArr[0].zipcode;
                                                }

                                            }
                                            if (addr.permanent.selectstate != oldVal[index].permanent.selectstate) {
                                                if (addr.permanent.cityArr.length > 1) {
                                                    addr.permanent.cityArr.splice(1, addr.permanent.cityArr.length);
                                                }
                                                addr.permanent.selectcity = addr.permanent.cityArr[0].cityId;
                                                angular.forEach($scope.cityList, function(city, index) {
                                                    if (city.stateId == addr.permanent.selectstate) {
                                                        var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                        addr.permanent.cityArr.push(tempObject);
                                                    }
                                                });
                                                if (addr.permanent.pinCodeArr.length > 1) {
                                                    addr.permanent.pinCodeArr.splice(1, addr.permanent.pinCodeArr.length);
                                                    addr.permanent.selectpincode = addr.permanent.pinCodeArr[0].zipcode;
                                                }

                                                //alert(addr.permanent.selectstate);
                                            }
                                            if (addr.office.selectstate != oldVal[index].office.selectstate) {
                                                //alert(addr.permanent.selectstate);
                                                if (addr.office.cityArr.length > 1) {
                                                    addr.office.cityArr.splice(1, addr.current.cityArr.length);
                                                }
                                                addr.office.selectcity = addr.office.cityArr[0].cityId;
                                                angular.forEach($scope.cityList, function(city, index) {
                                                    if (city.stateId == addr.office.selectstate) {
                                                        var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                                        addr.office.cityArr.push(tempObject);
                                                    }
                                                });
                                                if (addr.office.pinCodeArr.length > 1) {
                                                    addr.office.pinCodeArr.splice(1, addr.office.pinCodeArr.length);
                                                    addr.office.selectpincode = addr.office.pinCodeArr[0].zipcode;
                                                }
                                            }

                                            if (addr.current.selectcity != oldVal[index].current.selectcity) {
                                                //alert(addr.current.selectstate);
                                                if (addr.current.pinCodeArr.length > 1) {
                                                    addr.current.pinCodeArr.splice(1, addr.current.pinCodeArr.length);
                                                }
                                                addr.current.selectpincode = addr.current.pinCodeArr[0].zipcode;
                                                angular.forEach($scope.pincodeList, function(pin, index) {
                                                    if (pin.cityId == addr.current.selectcity) {
                                                        var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                        addr.current.pinCodeArr.push(tempObject);
                                                    }
                                                });
                                            }

                                            if (addr.permanent.selectcity != oldVal[index].permanent.selectcity) {
                                                //alert(addr.current.selectstate);
                                                if (addr.permanent.pinCodeArr.length > 1) {
                                                    addr.permanent.pinCodeArr.splice(1, addr.permanent.pinCodeArr.length);
                                                }
                                                addr.permanent.selectpincode = addr.permanent.pinCodeArr[0].zipcode;
                                                angular.forEach($scope.pincodeList, function(pin, index) {
                                                    if (pin.cityId == addr.permanent.selectcity) {
                                                        var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                        addr.permanent.pinCodeArr.push(tempObject);
                                                    }
                                                });
                                            }

                                            if (addr.office.selectcity != oldVal[index].office.selectcity) {
                                                //alert(addr.current.selectstate);
                                                if (addr.office.pinCodeArr.length > 1) {
                                                    addr.office.pinCodeArr.splice(1, addr.office.pinCodeArr.length);
                                                }
                                                addr.office.selectpincode = addr.office.pinCodeArr[0].zipcode;
                                                angular.forEach($scope.pincodeList, function(pin, index) {
                                                    if (pin.cityId == addr.office.selectcity) {
                                                        var tempObject = {zipcode: pin.zipcode, zipdesc: pin.pinCode + " - " + pin.zipdesc};
                                                        addr.office.pinCodeArr.push(tempObject);
                                                    }
                                                });
                                            }

                                        });
                                    }, true);

//commented by godwin
                                    // $scope.setCity(current);
                                    //$scope.setCity(permanent);
//                                    $scope.setCity(office);
//
//                                    $scope.setPinCode(current);
//                                    $scope.setPinCode(permanent);
//                                    $scope.setPinCode(office);

                                    var compleateApplicantType = "";

                                    if (item.applicantType == "P") {
                                        compleateApplicantType = " Applicant";
                                    } else if (item.applicantType == "C") {
                                        compleateApplicantType = "Co-Applicant";
                                    } else if (item.applicantType == "G") {
                                        compleateApplicantType = "Gaurantor";
                                    }

                                    var applicantAddressObj = {
                                        applicantId: item.applicantId,
                                        applicantName: item.applicantName,
                                        applicantType: compleateApplicantType,
                                        mailingaddress: mailingaddressTemp,
                                        communicationno: communicationnoTemp,
                                        current: current,
                                        permanent: permanent,
                                        office: office
                                    };


                                    $scope.addressParentArr.push(applicantAddressObj);

                                    $scope.PermanentChangeValue(parentIndex);
                                    $scope.OfficeChangeValue(parentIndex);

                                });
                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };




                    $scope.setCity = function(addObj) {
                        addObj.cityArr = [];

                        addObj.cityArr.push($scope.cityDefObj);

                        if (addObj.selectstate != $scope.stateArr[0].stateId) {
                            angular.forEach($scope.cityList, function(city, index) {
                                if (city.stateId == addObj.selectstate) {
                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                    addObj.cityArr.push(tempObject);
                                }
                            });
                        }
                    }

                    $scope.setPinCode = function(addObj) {
                        addObj.pinCodeArr = [];
                        addObj.pinCodeArr.push($scope.pinCodeDefObj);

                        if (addObj.selectcity != addObj.cityArr[0].cityId) {
                            angular.forEach($scope.pincodeList, function(pinCode, index) {
                                if (pinCode.cityId == addObj.selectcity) {
                                    var tempObject = {zipcode: pinCode.zipcode, zipdesc: pinCode.pinCode + " - " + pinCode.zipdesc};
                                    addObj.pinCodeArr.push(tempObject);
                                }
                            });
                        }
                    }

                    $scope.validateEmail = function(email) {

                        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return re.test(email);
                    }
                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }

                    $scope.checkMonth = function(monthsValue) {
                        var months = /^(0?[0-9]|1[011])$/;
                        return months.test(monthsValue);
                    }

                    $scope.checkAddress = function(validAddress) {
                        var address = /^[a-zA-Z\s\d\/`~!@#$%^&*()-_+=.,?;:'"{}]+$/;
                        return address.test(validAddress);
                    }

                    $scope.checkCity = function(city) {
                        var regEx = /^[a-zA-Z.,]+$/;
                        return regEx.test(city);
                    }

                    $scope.validateAddress = function(addressObj) {


                        if (addressObj.ownertype == null || addressObj.ownertype == "0") {
                            alert("Please Select Ownershipstatus");
                            return false;
                        } else if (addressObj.addressline1 == null || addressObj.addressline1 == "") {
                            alert("Pleses Fill Address Line 1");
                            return false;
                        } else if (addressObj.addressline1.length > 40) {
                            alert("Address line  Exceded ");
                            return false;
                        } else if (!$scope.checkAddress(addressObj.addressline1)) {
                            alert("Address line 1 should not content special charecter ");
                            return false;
                        } else if (addressObj.addressline2 != null && addressObj.addressline2 != "" && !$scope.checkAddress(addressObj.addressline2)) {
                            alert("Address line 2 should not content special charecter");
                            return false;
                        }
//                        else if (!$scope.checkAddress(addressObj.addressline2)) {
//                            alert("Address line 2 should not content special charecter ");
//                        }
                        else if (addressObj.addressline3 != null && addressObj.addressline3 != "" && !$scope.checkAddress(addressObj.addressline3)) {
                            alert("Address line 3 should not content special charecter");
                            return false;
                        }
//                        else if (addressObj.addressline3.length > 40) {
//                            alert("Address line  Exceded ");
//                        }
//                        else if (!$scope.checkAddress(addressObj.addressline3)) {
//                            alert("Address line 3 should not content special charecter ");
//                        }
                        else if (addressObj.addressline4 != null && addressObj.addressline4 != "" && !$scope.checkAddress(addressObj.addressline4)) {
                            alert("Address line 4 should not content special charecter");
                            return false;
                        }
//                        else if (addressObj.addressline4.length > 40) {
//                            alert("Address line  Exceded ");
//                        }
//                        else if (!$scope.checkAddress(addressObj.addressline4)) {
//                            alert("Address line 4 should not content special charecter ");
//                        } 
                        else if (addressObj.selectstate == null || addressObj.selectstate == "0") {
                            alert("Please Select State");
                            return false;
                        } else if (addressObj.selectcity == null || addressObj.selectcity == "0") {
                            alert("Please Select City ");
                            return false;
                        } 
//                        else if (!$scope.checkCity(addressObj.selectcity)) {
//                            alert("Number,Special Character are not allowed for City ");
//                            return false;
//                        }
                        else if (addressObj.selectpincode == null || addressObj.selectpincode == "0") {
                            alert("Please Select PinCode ");
                            return false;
                        } else if (addressObj.stdcode == null || addressObj.stdcode == "") {
                            alert("Please Enter STD code");
                            return false;
                        } else if (isNaN(addressObj.stdcode)) {
                            alert("STD Code Must Be a Number");
                            return false;
                        } else if (addressObj.stdcode.length > 5) {
                            alert("Please Enter Valid STD Code");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.stdcode)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for STD code");
                            return false;
                        } else if (addressObj.phone1 == null || addressObj.phone1 == "") {
                            alert("Please Enter Phone Number");
                            return false;
                        } else if (isNaN(addressObj.phone1)) {
                            alert("Phone Number content only Number");
                            return false;
                        } else if (addressObj.phone1.charAt(0) == "1" || addressObj.phone1.charAt(0) == "+") {
                            alert("Phone Number should not start from 1");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.phone1)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for phone");
                            return false;
                        }

                        var sum = addressObj.stdcode.length + addressObj.phone1.length;

                        if (sum != 11) {
                            alert("STD code + Phone field should be exactly 11 digit only ");
                            return false;
                        } else if (addressObj.mobile == null || addressObj.mobile == "") {
                            alert("Please Enter Mobile Number");
                            return false;
                        } else if (isNaN(addressObj.mobile)) {
                            alert("Please Enter Valid Mobile Number");
                            return false;
                        } else if (addressObj.mobile.length < 10) {
                            alert(" Mobile Number should content 10 digits");
                            return false;
                        } else if (addressObj.mobile.charAt(0) == "1" || addressObj.mobile.charAt(0) == "+") {
                            alert("Mobile Number should not start from 1");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.mobile)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for Mobile");
                            return false;
                        } else if (addressObj.landmark == null || addressObj.landmark == "") {
                            alert("Please Enter LandMark");
                            return false;
                        } else if (addressObj.currentaddressyears == null || addressObj.currentaddressyears == "") {
                            alert("Pleses Enter Years at Current Address  ");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.currentaddressyears)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for Years at Current Address");
                            return false;
                        }
                        else if (addressObj.currentaddressmonths == null || addressObj.currentaddressmonths == "") {
                            alert("Pleses Enter Months at Current Address ");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.currentaddressmonths)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for Months at Current Address");
                            return false;
                        } else if (!$scope.checkMonth(addressObj.currentaddressmonths)) {
                            alert("Current Address months are not allowed more than 11  ");
                            return false;
                        } else if (addressObj.currentaddressyears == "0" && addressObj.currentaddressmonths == "0") {
                            alert("Please Fill Years and  Months at Current Address as zero is not allowed for both field ");
                            return false;
                        }
                        else if (addressObj.cityyears == null || addressObj.cityyears == "") {
                            alert("Pleses Enter Years at  City ");
                            return false;
                        } else if ((addressObj.currentaddressyears * 12 + parseInt(addressObj.currentaddressmonths)) > (addressObj.cityyears * 12 + parseInt(addressObj.citymonths))) {
                            alert("Years at Current Address is not allowed more than Years at  City ");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.cityyears)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for Years at  City");
                            return false;
                        }
                        else if (addressObj.citymonths == null || addressObj.citymonths == "") {
                            alert("Pleses Enter City months");
                            return false;
                        } else if (!$scope.checkDecimal(addressObj.citymonths)) {
                            alert("Alphabets,Special Character and Decimal value are not allowed for Months at City ");
                            return false;
                        } else if (!$scope.checkMonth(addressObj.citymonths)) {
                            alert("City months are not allowed more than 11  ");
                            return false;
                        } else if (addressObj.cityyears == "0" && addressObj.citymonths == "0") {
                            alert("Please Fill Years and Months at City as zero is not allowed for both field");
                            return false;
                        }
                        else if (addressObj.emailid == null || addressObj.emailid == "") {
                            alert("Please Enter EmailId");
                            return false;
                        } else if (!$scope.validateEmail(addressObj.emailid)) {
                            alert("Please Enter valid EmailId");
                            return false;
                        } else {
                            return true;
                        }
                    }
                    $scope.validateAddressDetailsform = function(index) {

                        if (!$scope.validateAddress($scope.addressParentArr[index].current)) {

                        } else if ($scope.addressParentArr[index].permanent.sameAs == null || $scope.addressParentArr[index].permanent.sameAs == "") {
                            alert("Please Select Permanent address");
                        } else if ($scope.addressParentArr[index].office.sameAs == null || $scope.addressParentArr[index].office.sameAs == "") {
                            alert("Please Select Office address");
                        } else if ($scope.addressParentArr[index].mailingaddress == null || $scope.addressParentArr[index].mailingaddress == "") {
                            alert("Please Select Mailing Address");
                        } else if ($scope.addressParentArr[index].communicationno == null || $scope.addressParentArr[index].communicationno == "") {
                            alert("Please Select Primary Contact Number");
                        } 
                        else if ($scope.addressParentArr[index].permanent.sameAs == 'O' && !$scope.validateAddress($scope.addressParentArr[index].permanent)) {
                            alert("Please Select Permanent Address");
                        } else if ($scope.addressParentArr[index].office.sameAs == 'O' && !$scope.validateAddress($scope.addressParentArr[index].office)) {
                            alert("Please Select Office Address");
                        } else {

                            $scope.saveCheck(index);

                        }
                    }
                    $scope.submitAddressForm = function(index) {
                        var addressData = [];
                        var temp;
                        var permentAddressId = '';
                        var officeAddressId = '';

                        permentAddressId = $scope.addressParentArr[index].permanent.addressId;

                        if ($scope.addressParentArr[index].permanent.sameAs == 'C') {
                            temp = angular.copy($scope.addressParentArr[index].current);
                            $scope.addressParentArr[index].permanent = temp;
                            $scope.addressParentArr[index].permanent.sameAs = 'C';
                        } else if ($scope.addressParentArr[index].permanent.sameAs == 'E') {
                            $scope.addressParentArr[index].permanent.sameAs = 'E';

                        }

                        $scope.addressParentArr[index].permanent.addressType = 'P';
                        $scope.addressParentArr[index].permanent.addressId = permentAddressId;
                        addressData.push($scope.addressParentArr[index].permanent);

                        officeAddressId = $scope.addressParentArr[index].office.addressId;

                        if ($scope.addressParentArr[index].office.sameAs == 'C') {
                            temp = angular.copy($scope.addressParentArr[index].current);
                            $scope.addressParentArr[index].office = temp;
                            $scope.addressParentArr[index].office.sameAs = 'C';
                        } else if ($scope.addressParentArr[index].office.sameAs == 'P') {
                            temp = angular.copy($scope.addressParentArr[index].permanent);
                            $scope.addressParentArr[index].office = temp;
                            $scope.addressParentArr[index].office.sameAs = 'P';
                        } else if ($scope.addressParentArr[index].office.sameAs == 'E') {
                            $scope.addressParentArr[index].office.sameAs = 'E';
                        } else if ($scope.addressParentArr[index].office.sameAs == 'N') {
                            $scope.addressParentArr[index].office.sameAs = 'N';
                        } 

                        $scope.addressParentArr[index].office.addressId = officeAddressId;
                        $scope.addressParentArr[index].office.addressType = 'F';
                        addressData.push($scope.addressParentArr[index].office);


                        $scope.addressParentArr[index].current.addressType = "C";
                        $scope.addressParentArr[index].current.sameAs = "";
                        addressData.push($scope.addressParentArr[index].current);

                        var callURL = $rootScope.urlDomain + "SaveAddressData.json";
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantId: $scope.addressParentArr[index].applicantId,
                            addressArr: addressData,
                            mailingAddress: $scope.addressParentArr[index].mailingaddress,
                            communicationno: $scope.addressParentArr[index].communicationno

                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            // transformRequest: transformRequestAsFormPost,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                //$cookies.a = data.applicationId;
                                //$rootScope.applicationId = data.applicationId;

                                if ($rootScope.progress < 3) {
                                    $rootScope.progress = 3;
                                }
                                $rootScope.setCookies(data.applicationId, $rootScope.progress);
                                // $rootScope.preallowform = 4;
                                alert("Success");
// $rootScope.openForm(3);
//                                $location.path('/presanction/occupationincome');
                            } else {
                                alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }






                }]).controller("Occupation_IncomeCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$modal", "$log", "$route", function($cookies, $rootScope, $scope, $http, $location, $modal, $log, $route) {

                	
                    $scope.formReload = function() {
                        $route.reload();
                    };

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.OccupationIncomesubmit(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    var incomeCounter = 0;
                    var obligationCounter = 0;
                    //$scope.incomeObj = {indexx:$scope.incomeCounter++, incomeId: "", incomeFreq: "", incomeHead: "", amount: "",selectincomefrequency: "0",selectincomehead: "0"};
                    //var incomeObj = {incomeId: "", incomeFreq: "", incomeHead: "", amount: "",selectincomefrequency: "0",selectincomehead: "0"};
                    //$scope.obligationObj = {indexx:obligationCounter++,obligationId: "", emiAmount: "", noOfBounces: ""};

                    $scope.occupatioIncomeArr = [];
                    $scope.addIncomeDetails = function(index) {
                        //alert(1);
                        $scope.occupationArr[index].incomeArr.push({indexx: incomeCounter++, incomeId: "", incomeFreq: "", incomeHead: "", amount: "", selectincomefrequency: "0", selectincomehead: "0"});
                        //console.log($scope.occupationArr);
                    }

                    $scope.$watch('occupationArr', function() {
                        console.log($scope.occupationArr);

                    }, true);


                    $scope.deleteIncomeDetails = function(parentIndex, index) {
                        $scope.occupationArr[parentIndex].deleteIncome.push({incomeId: $scope.occupationArr[parentIndex].incomeArr[index].incomeId});
                        $scope.occupationArr[parentIndex].incomeArr.splice(index, 1);
                    }


                    $scope.addObligationDetails = function(index) {
                        $scope.occupationArr[index].obligationArr.push({indexx: obligationCounter++, obligationId: "", emiAmount: "", noOfBounces: ""});
                    }
                    $scope.deleteObligationDetails = function(parentIndex, index) {
                        $scope.occupationArr[parentIndex].deleteObligation.push({obligationId: $scope.occupationArr[parentIndex].obligationArr[index].obligationId});
                        $scope.occupationArr[parentIndex].obligationArr.splice(index, 1);
                    }
                    $scope.occupationArr = [];
                    
                    
                    $scope.employerNameArr = [{employerId: "0", employerName: 'Select Employer Name'}];
                    $scope.IncomeFrequencyArr = [{value: "0", description: 'Select Income Frequency'}];
                    $scope.OccupationIncomeArr = [{value: "0", description: 'Select Income Head'}];
                    
                    $scope.nullValidation = function(val) {
                        if (angular.isUndefined(val) || val === null || val === "null") {
                            return "";
                        } else {
                            return val;
                        }
                    }

                    $scope.preloadOccupationIncome = function() {

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchOccupationData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.sts == "200") {


                                
                                angular.forEach(response.occupationIncomeVO.lstEmployer, function(employer, index) {
                                    var tempObject = {employerId: employer.employerId, employerName: employer.employerName};
                                    $scope.employerNameArr.push(tempObject);
                                });
//                                $scope.selectemployername = $scope.employerNameArr[0].employerId;
                                angular.forEach(response.addIncomeVO.lstIncfreq, function(IncomeFrequency, index) {
                                    var tempObject = {value: IncomeFrequency.value, description: IncomeFrequency.description};
                                    $scope.IncomeFrequencyArr.push(tempObject);
                                });
//                                $scope.selectincomefrequency = $scope.IncomeFrequencyArr[0].value;
                                angular.forEach(response.addIncomeVO.lstIncome, function(OccupationIncome, index) {
                                    var tempObject = {value: OccupationIncome.value, description: OccupationIncome.description};
                                    $scope.OccupationIncomeArr.push(tempObject);
                                });
                                //$scope.selectincomehead = $scope.OccupationIncomeArr[0].value;

                                if (response.data != null) {
                                    //alert(1);
                                    angular.forEach(response.data, function(item, index) {
                                    	var bussinessnatureArr=[];
                                    	if(item.occupation == 'SEP') {
                                    		bussinessnatureArr = [{natureofbusinessid: "0", natureofbusiness: 'Select Nature of Business'}];
                                    	} else if (item.occupation == 'SAL') {
                                    		bussinessnatureArr = [{natureofbusinessid: "0", natureofbusiness: 'Select Nature of Work'}];
                                    	}
                                    	
                                    	angular.forEach(response.occupationIncomeVO.lstBussinessNature, function(bussinessnature, index) {
                                            var tempObject = {natureofbusinessid: bussinessnature.natureofbusinessid, natureofbusiness: bussinessnature.natureofbusiness};
                                            bussinessnatureArr.push(tempObject);
                                        });
//                                        $scope.selectnatureofwork = $scope.bussinessnatureArr[0].natureofbusinessid;
                                    	
                                        $scope.incomeArr = [];
                                        if (item.lstIncome != null && item.lstIncome.length > 0) {
                                            //alert(1);
                                            angular.forEach(item.lstIncome, function(incomeDetails, index) {

                                                var incomeData = {
                                                    indexx: incomeCounter++,
                                                    incomeId: incomeDetails.incomeId,
                                                    selectincomefrequency: incomeDetails.incomeFreq,
                                                    selectincomehead: incomeDetails.incomeHead,
                                                    amountselfemployed: parseInt(incomeDetails.amount)
                                                };
                                                $scope.incomeArr.push(incomeData);

                                            });
                                        } else {

                                            //$scope.incomeObj = {indexx:index,incomeId: "", incomeFreq: "", incomeHead: "", amount: "",selectincomefrequency: "0",selectincomehead: "0"};
                                            $scope.incomeArr.push({indexx: incomeCounter++, incomeId: "", incomeFreq: "", incomeHead: "", amount: "", selectincomefrequency: "0", selectincomehead: "0"});
                                        }

                                        var obligationArr = [];
                                        if (item.lstObligation != null && item.lstObligation.length > 0) {


                                            angular.forEach(item.lstObligation, function(obligationDetails, index) {

                                                var obligationData = {
                                                    obligationId: obligationDetails.obligationId,
                                                    emiamountselfemployed: parseInt(obligationDetails.emiAmount),
                                                    bouncesselfemployed: parseInt(obligationDetails.noOfBounces)
                                                };
                                                obligationArr.push(obligationData);
                                            });
                                        }
                                        else {
                                            obligationArr.push({indexx: obligationCounter++, obligationId: "", emiAmount: "", noOfBounces: ""});
                                        }

                                        var compleateApplicantType = "";

                                        if (item.applicantType == "P") {
                                            compleateApplicantType = "Applicant";
                                        } else if (item.applicantType == "C") {
                                            compleateApplicantType = "Co-Applicant";
                                        } else if (item.applicantType == "G") {
                                            compleateApplicantType = "Gaurantor";
                                        }

                                        if (item.dataOccupationIncome != null) {
                                            //alert(1);
                                            $scope.occupation1 = {
                                                selectnatureofwork: item.dataOccupationIncome.bussNature,
                                                presentjobstabilityyears: $scope.nullValidation(item.dataOccupationIncome.presentJobStabilityYrs + ""),
                                                presentjobstabilitymonths: $scope.nullValidation(item.dataOccupationIncome.presentJobStabilityMths + ""),
                                                selectemployername: item.dataOccupationIncome.employerId,
                                                others: $scope.nullValidation(item.dataOccupationIncome.otherEmployer),
                                                creditCardNumber: $scope.nullValidation(item.dataOccupationIncome.creditCardNo),
                                                lengthofbusinessmonths: item.dataOccupationIncome.businessLengthMths,
                                                lengthofbusinessyears: item.dataOccupationIncome.businessLengthYrs,
                                                businessname: item.dataOccupationIncome.businessName,
                                                previousjobstabilitymonths: $scope.nullValidation(item.dataOccupationIncome.previousJobMths + ""),
                                                previousjobstabilityyears: $scope.nullValidation(item.dataOccupationIncome.previousJobYrs + ""),
                                                incomeArr: $scope.incomeArr,
                                                deleteIncome: [],
                                                obligationArr: obligationArr,
                                                deleteObligation: [],
                                                applicantId: item.applicantId,
                                                applicantName: item.applicantName,
                                                showCreditCard: item.showCreditCard,
                                                applicantType: compleateApplicantType,
                                                occupation: item.occupation,
                                                bussinessnatureArr: bussinessnatureArr

                                            };
                                        } else {
                                            $scope.occupation1 = {
                                                selectnatureofwork: "0",
                                                presentjobstabilityyears: "",
                                                presentjobstabilitymonths: "",
                                                selectemployername: "0",
                                                others: "",
                                                creditCardNumber:"",
                                                lengthofbusinessmonths: "",
                                                lengthofbusinessyears: "",
                                                businessname: "",
        
                                                previousjobstabilitymonths: "",
                                                previousjobstabilityyears: "",
                                                incomeArr: $scope.incomeArr,
                                                deleteIncome: [],
                                                obligationArr: obligationArr,
                                                deleteObligation: [],
                                                applicantId: item.applicantId,
                                                applicantName: item.applicantName,
                                                showCreditCard: item.showCreditCard, 
                                                applicantType: compleateApplicantType,
                                                occupation: item.occupation,
                                                bussinessnatureArr: bussinessnatureArr

                                            };
                                        }
                                        $scope.occupationArr.push($scope.occupation1);
                                    });
                                }
                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }
                    
                    $scope.checkCreditCardNumber = function(value) {
                        var regEx = /^[1-9]+$/;
                        return regEx.test(value);
                    }

                    $scope.checkMonth = function(monthsValue) {
                        var months = /^(0?[0-9]|1[011])$/;
                        return months.test(monthsValue);
                    }

                    $scope.validateNumber = function(checkAmount) {
                        var reg = /^[^0\W][\d]*$/;
                        return reg.test(checkAmount);
                    }

                    $scope.validateOccupationIncomeForm = function(index) {

                        var occupation = $scope.occupationArr[index];
                        if (occupation.selectnatureofwork == null || occupation.selectnatureofwork == "" || occupation.selectnatureofwork == "0") {

                            alert("Please Select Nature Of Work ");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.presentjobstabilityyears == null || occupation.presentjobstabilityyears == "")) {
                            alert("Please Enter Present job stability years");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkDecimal(occupation.presentjobstabilityyears))) {
                            alert("Alphabet,special characters and Decimal value not allowed for Present job stability years");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.presentjobstabilityyears == "0" && occupation.presentjobstabilitymonths == "0")) {
                            alert("Please Fill years and months at Present job stability as zero is not allowed for both field ");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.presentjobstabilitymonths == null || occupation.presentjobstabilitymonths == "")) {
                            alert("Please Enter Present job stability months");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkDecimal(occupation.presentjobstabilitymonths))) {
                            alert("Alphabet,special characters and Decimal value not allowed for Present job stability months");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkMonth(occupation.presentjobstabilitymonths))) {
                            alert(" present job stability months not allowed more than 11");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.previousjobstabilityyears == null || occupation.previousjobstabilityyears == "")) {
                            alert("Please Enter previous job stability years");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkDecimal(occupation.previousjobstabilityyears))) {
                            alert("Alphabet,special characters and Decimal value not allowed for previous job stability years");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.previousjobstabilitymonths == null || occupation.previousjobstabilitymonths == "")) {
                            alert("Please Enter previous job stability months");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkDecimal(occupation.previousjobstabilitymonths))) {
                            alert("Alphabet,special characters and Decimal value not allowed for previous job stability months");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (!$scope.checkMonth(occupation.previousjobstabilitymonths))) {
                            alert(" Previous job stability months not allowed more than 11");
                            return false;
                        } else if (occupation.occupation == 'SAL' && (occupation.previousjobstabilityyears == "0" && occupation.previousjobstabilitymonths == "0")) {
                            alert("Please Fill years and months at previous job stability as zero is not allowed for both field");
                            return false;
                        }
                        else if (occupation.occupation == 'SAL' && (occupation.selectemployername == null || occupation.selectemployername == "0")) {
                            alert("Please Select Employer Name ");
                            return false;
                        } else if (occupation.occupation == 'SAL' && occupation.selectemployername=="500" && (occupation.others == null || occupation.others == "")) {
                            alert("Please Fill Other");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (occupation.lengthofbusinessmonths == null || occupation.lengthofbusinessmonths == "")) {
                            alert("Please Enter length of business months ");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (!$scope.checkDecimal(occupation.lengthofbusinessmonths))) {
                            alert("Alphabet,special characters and Decimal value not allowed for length of business months");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (!$scope.checkMonth(occupation.lengthofbusinessmonths))) {
                            alert(" length of business months not allowed more than 11");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (occupation.lengthofbusinessyears == null || occupation.lengthofbusinessyears == "")) {
                            alert("Please Enter length of business years ");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (!$scope.checkDecimal(occupation.lengthofbusinessyears))) {
                            alert("Alphabet,special characters and Decimal value not allowed for length of business years");
                            return false;
                        } else if (occupation.occupation == 'SEP' && (occupation.lengthofbusinessyears == "0" && occupation.lengthofbusinessmonths == "0")) {
                            alert("Please Fill years and months at length of business as zero is not allowed for both field ");
                            return false;
                        }
                        else if (occupation.occupation == 'SEP' && (occupation.businessname == null || occupation.businessname == "0")) {
                            alert("Please Enter business name ");
                            return false;
                        } else if (occupation.showCreditCard == "true" && (occupation.creditCardNumber == null || occupation.creditCardNumber == "") && !$scope.checkCreditCardNumber(occupation.creditCardNumber)) {
                        	alert("Please Enter Credit Card Number");
//                            alert("Alphabets and Special Character are not allowed for Credit Card Number\n!Credit Card Number should content digit between 1 to 9 ");
                            return false;
                        } else if (occupation.showCreditCard == "true"  && occupation.creditCardNumber.length < 15) {
                        	alert("Credit Card Number should contain minimum 15 digit");
//                            alert("Alphabets and Special Character are not allowed for Credit Card Number\n!Credit Card Number should content digit between 1 to 9 ");
                            return false;
                        }  else if (occupation.showCreditCard == "true"  && !$scope.checkCreditCardNumber(occupation.creditCardNumber)) {
//                        	alert("Please Enter Credit Card Number");
                          alert("Alphabets and Special Character are not allowed for Credit Card Number\n!Credit Card Number should contain digit between 1 to 9 ");
                          return false;
                      } 
                        



                        var tempFlag = true;
                        angular.forEach(occupation.incomeArr, function(income, index) {



                            if (income.selectincomehead == null || income.selectincomehead == "0") {
                                alert("Please Select Income Head");
                                tempFlag = false;
                                return false;
                            } else if (income.selectincomefrequency == null || income.selectincomefrequency == "0") {
                                alert("Please Select Income Frequency");
                                tempFlag = false;
                                return false;
                            } else if (income.amountselfemployed == null || income.amountselfemployed == "") {
                                alert("Please Enter Amount");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkDecimal(income.amountselfemployed)) {
                                alert("special characters, Alphabets and Decimal value are not allowed for  Amount");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.validateNumber(income.amountselfemployed)) {
                                alert(" Amount Should be greater than 0 and it should not contain any special characters and Alphabets  ");
                                tempFlag = false;
                                return false;
                            }


                        });
                        if (tempFlag) {
                            angular.forEach(occupation.obligationArr, function(obligation, index) {
                                if (obligation.emiamountselfemployed != null && obligation.emiamountselfemployed != "" && obligation.emiamountselfemployed.length > 0 && !$scope.checkDecimal(obligation.emiamountselfemployed)) {
                                    alert("Special characters , Alphabets and Decimal Value are not allowed for EMI Amount ");
                                    tempFlag = false;
                                    return false;
                                }
// else if (!$scope.checkDecimal(obligation.emiamountselfemployed)) {
// alert("Decimal value are not allowed for EMI Amount");
// tempFlag = false;
// return false;
// }
                                else if (obligation.bouncesselfemployed != null && obligation.bouncesselfemployed != "" && obligation.bouncesselfemployed.length > 0 && !$scope.checkDecimal(obligation.bouncesselfemployed)) {
                                    alert("Special characters,Alphabets and Decimal Value are not allowed for Bounces Self Employed");
                                    tempFlag = false;
                                    return false;
                                }
// else if (!$scope.checkDecimal(obligation.bouncesselfemployed)) {
// alert("Decimal value are not allowed for Bounces Self Employed");
// tempFlag = false;
// return false;
// }
                            });
                        }

                        if (tempFlag) {

                            $scope.saveCheck(index);

                        }
                    }
                    $scope.OccupationIncomesubmit = function(index) {
                        var occupation = $scope.occupationArr[index];
                        var callURL = $rootScope.urlDomain + "SaveOccupationData.json";

                        var data = {
                            selectnatureofwork: occupation.selectnatureofwork,
                            incomeArr: occupation.incomeArr,
                            deleteIncome: occupation.deleteIncome,
                            obligationArr: occupation.obligationArr,
                            deleteObligation: occupation.deleteObligation,
                            creditCardNumber: occupation.creditCardNumber,
                            presentjobstabilityyears: occupation.presentjobstabilityyears,
                            presentjobstabilitymonths: occupation.presentjobstabilitymonths,
                            selectemployername: occupation.selectemployername,
                            previousjobstabilityyears: occupation.previousjobstabilityyears,
                            previousjobstabilitymonths: occupation.previousjobstabilitymonths,
                            lengthofbusinessyears: occupation.lengthofbusinessyears,
                            lengthofbusinessmonths: occupation.lengthofbusinessmonths,
                            others: occupation.others,
                            businessname: occupation.businessname,
                            occupation: occupation.occupation,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantId: occupation.applicantId


                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                console.log(data);
                                alert("Data Saved");
                                if ($rootScope.progress < 4) {
                                    $rootScope.progress = 4;
                                }
//                                $location.path('/presanction/bankdetail');
                            } else {
                                alert(data.msg);
                            }



                        }).error(function(error) {
                            console.log(error);
                        });


                    }


                }]).controller("BankDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$modal", "$log", "$route", function($cookies, $rootScope, $scope, $http, $location, $modal, $log, $route) {



                    $scope.formReload = function() {
                        $route.reload();
                    };

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitBankForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    var bankObjCounter = 0;
                    //$scope.bankObj = {indexx:bankObjCounter, bankDetaisId: "", bankName: "", accountType: "", accountNumber: "", bankVintageYears: "", bankVintageMonths: "", avgBankBalance: ""};
                    $scope.parentBankArr = [];


                    $scope.addBankDetails = function(index) {
                        $scope.parentBankArr[index].bankArr.push({indexx: bankObjCounter++, selectaccounttype: "0", selectbankname: "0", bankDetaisId: "", bankName: "", accountType: "", accountNumber: "", bankVintageYears: "", bankVintageMonths: "", avgBankBalance: ""});
                    }

                    $scope.deleteBank = function(parentIndex, index) {

                        $scope.parentBankArr[parentIndex].deleteBankDetails.push({bankDetaisId: $scope.parentBankArr[parentIndex].bankArr[index].bankDetaisId});
                        $scope.parentBankArr[parentIndex].bankArr.splice(index, 1);
                    }

                    $scope.bankNameArr = [{value: "0", description: 'Select Bank Name'}];
                    $scope.accountTypeArr = [{value: "0", description: 'Select Account Type'}];

                    $scope.preloadBankDetails = function() {

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }

                        var callURL = $rootScope.urlDomain + "FetchBankData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.sts == "200") {

                                angular.forEach(response.bankDetailsVo.lstBank, function(bankName, index) {
                                    var tempObject = {value: bankName.value, description: bankName.description};
                                    $scope.bankNameArr.push(tempObject);
                                });
                                //alert($scope.bankNameArr[0].value);
                                $scope.selectbankname = $scope.bankNameArr[0].value;

                                angular.forEach(response.bankDetailsVo.lstAccountType, function(bankType, index) {
                                    var tempObject = {value: bankType.value, description: bankType.description};
                                    $scope.accountTypeArr.push(tempObject);
                                });

                                $scope.selectaccounttype = $scope.accountTypeArr[0].value;

                                if (response.data != null && response.data.length > 0) {

                                    $scope.parentBankArr = [];

                                    angular.forEach(response.data, function(item, index) {

                                        var bankArr = [];

                                        if (item.lstBank != null && item.lstBank.length > 0) {
                                            angular.forEach(item.lstBank, function(bankDetails, index) {


                                                var bankData = {
                                                    indexx: bankObjCounter++,
                                                    bankDetaisId: bankDetails.bankDetaisId,
                                                    selectbankname: bankDetails.bankName,
                                                    selectaccounttype: bankDetails.accountType,
                                                    accountno: bankDetails.accountNumber,
                                                    bankvintageyears: bankDetails.bankVintageYears + "",
                                                    bankvintagemonths: bankDetails.bankVintageMonths + "",
                                                    averagebankbalance: bankDetails.avgBankBalance
                                                };
                                                bankArr.push(bankData);

                                            });
                                        } else {
                                            bankArr.push({indexx: bankObjCounter++, selectaccounttype: "0", selectbankname: "0", bankDetaisId: "", bankName: "", accountType: "", accountNumber: "", bankVintageYears: "", bankVintageMonths: "", avgBankBalance: ""});
                                        }

                                        var compleateApplicantType = "";

                                        if (item.applicantType == "P") {
                                            compleateApplicantType = "Applicant";
                                        } else if (item.applicantType == "C") {
                                            compleateApplicantType = "Co-Applicant";
                                        } else if (item.applicantType == "G") {
                                            compleateApplicantType = "Gaurantor";
                                        }

                                        var bank = {
                                            bankArr: bankArr,
                                            deleteBankDetails: [],
                                            applicantId: item.applicantId,
                                            applicantName: item.applicantName,
                                            applicantType: compleateApplicantType,
                                            bank: item.bank

                                        };

                                        $scope.parentBankArr.push(bank);
                                    });
                                }
                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }

                    $scope.checkMonth = function(monthsValue) {
                        var months = /^(0?[0-9]|1[011])$/;
                        return months.test(monthsValue);
                    }

                    $scope.checkAvgBalance = function(avgBalance) {
                        var checkBalance = /^[\d\.]*$/;
                        return checkBalance.test(avgBalance);
                    }

                    $scope.validateBankDetailsForm = function(index) {

                        var parentBank = $scope.parentBankArr[index];

                        var tempFlag = true;

                        angular.forEach(parentBank.bankArr, function(bank, index) {
                            if (bank.selectbankname == null || bank.selectbankname == "0")

                            {
                                $(".error").show();
                                alert("Please Select Bank Name");
                                tempFlag = false;
                                return false;
                            } else if (bank.selectaccounttype == null || bank.selectaccounttype == "0")
                            {
                                $(".error").show();
                                alert("Please Select Account Type");
                                tempFlag = false;
                                return false;
                            } else if (bank.accountno == null || bank.accountno == "")
                            {
                                $(".error").show();
                                alert("Please Enter Account Number");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkDecimal(bank.accountno))
                            {
                                $(".error").show();
                                alert("Alphabets,Special Character and Decimal value are not allowed for Account Number");
                                tempFlag = false;
                                return false;
                            } else if (bank.bankvintageyears == null || bank.bankvintageyears == "")
                            {
                                $(".error").show();
                                alert("Please Enter Vintage Years");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkDecimal(bank.bankvintageyears))
                            {
                                $(".error").show();
                                alert("Alphabets,Special Character and Decimal value are not allowed for Vintage Years");
                                tempFlag = false;
                                return false;
                            } else if (bank.bankvintagemonths == null || bank.bankvintagemonths == "")
                            {
                                $(".error").show();
                                alert("Please Enter Vintage Months");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkDecimal(bank.bankvintagemonths))
                            {
                                $(".error").show();
                                alert("Alphabets,Special Character and Decimal value are not allowed for Vintage Months");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkMonth(bank.bankvintagemonths))
                            {
                                $(".error").show();
                                alert("Vintage Months not allowed more than 11");
                                tempFlag = false;
                                return false;
                            } else if (bank.bankvintageyears == "0" && bank.bankvintagemonths == "0")
                            {
                                $(".error").show();
                                alert("Please Fill years and months at Bank Vintage  as zero is not allowed for both field");
                                tempFlag = false;
                                return false;
                            }
                            else if (bank.averagebankbalance == null || bank.averagebankbalance == "")
                            {
                                $(".error").show();
                                alert("Please Enter Average Balance");
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkAvgBalance(bank.averagebankbalance))
                            {
                                $(".error").show();
                                alert("Alphabets and Special Charecters are not allowed for Average Balance");
                                tempFlag = false;
                                return false;
                            } else if (isNaN(bank.accountno))
                            {
                                $(".error").show();
                                alert("Please Enter Account Number");
                                tempFlag = false;
                                return false;
                            }
                            else if (isNaN(bank.bankvintageyears)) {
                                $(".error").show();
                                alert("Please Enter Bank Vintage Years in Numbers");
                                tempFlag = false;
                                return false;
                            }
                            else if (isNaN(bank.bankvintagemonths)) {
                                $(".error").show();
                                alert("Please Enter Bank Vintage Months in Numbers");
                                tempFlag = false;
                                return false;
                            } else if (isNaN(bank.averagebankbalance)) {
                                $(".error").show();
                                alert("Please Enter Average Balance in Numbers");
                                tempFlag = false;
                                return false;
                            } else
                            {
                                $(".error").hide();
                            }
                        });

                        if (tempFlag) {

                            $scope.saveCheck(index);

                        }

                    }
                    $scope.submitBankForm = function(index) {

                        var parentBank = $scope.parentBankArr[index];
                        var callURL = $rootScope.urlDomain + "SaveBankData.json";
                        var data = {
                            bankArr: parentBank.bankArr,
                            deleteBankDetails: parentBank.deleteBankDetails,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantId: parentBank.applicantId
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == 200) {
                                console.log(data);
                                alert("Data Saved");
                            $cookies.a = data.applicationId;
                            $rootScope.applicationId = data.applicationId;
                            if ($rootScope.progress < 5) {
                                $rootScope.progress = 5;
                            }
                        } else {
                                alert(data.msg);
                            }
//                          }  $location.path('/presanction/assetdetail');

// $rootScope.openForm(5);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }


                }])
                    .controller("AssetDetailsCtrl", ["$rootScope", "$cookies", "$scope", "$http", "$location", "$modal", "$log", "$route", function($rootScope, $cookies, $scope, $http, $location, $modal, $log, $route) {

                            $scope.formReload = function() {
                                $route.reload();
                            };

                            $scope.saveCheck = function() {
                                var modalInstance;
                                modalInstance = $modal.open({
                                    templateUrl: "confirmationContent.html",
                                    controller: "ConfirmationModalInstanceCtrl",
                                    resolve: {
                                        message: function() {
                                            return "Do you want to save ?";
                                        }
                                    }
                                }), modalInstance.result.then(function(isSave) {
                                    $log.info("Response: " + isSave);
                                    if (isSave) {
                                        // Save
                                        $scope.submitAssetForm();
                                    }
                                }, function() {
                                    $log.info("Modal dismissed at: " + new Date)
                                });
                            }


// $scope.$watch('selectassetmake', function() {
// $scope.selectassetcategory=$scope.selectassetmodel.catgId;
// });



                            $scope.assetmakeArr = [{makeId: "0", make: 'Select Asset Make'}];
                            $scope.assetmodelArr = [{modelId: "0", modelNo: 'Select Asset Model', catgId: '0'}];
                            $scope.manufacturerArr = [{manufacturerId: "0", manufacturerDesc: 'Select Manufacturer'}];
                            $scope.assetcategoryArr = [{catgId: "0", catgDesc: 'Select Category'}];

                            $scope.isExistingData = false;

                            $scope.selectmanufacture = "0";
                            // $scope.selectassetmake = "0";
                            // $scope.selectassetmodel = "0";

                            $scope.preloadAssetDetails = function() {

// $scope.selectassetcategory = "";
// }


                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                var callURL = $rootScope.urlDomain + "FetchAssetData.json";
// var callURL = $rootScope.urlDomain+"FetchAssetData.json";
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    console.log(response);
                                    angular.forEach(response.assetDetailVO.lstManufacturer, function(manufacturer, index) {
                                        var tempObject = {manufacturerId: manufacturer.manufacturerId, manufacturerDesc: manufacturer.manufacturerDesc};
                                        $scope.manufacturerArr.push(tempObject);
                                    });
//                                    if ($scope.manufacturerArr.length == 2) {
//                                        $scope.selectmanufacture = $scope.manufacturerArr[1].manufacturerId;
//                                    } else {
//                                        $scope.selectmanufacture = $scope.manufacturerArr[0].manufacturerId;
//                                    }

                                    $scope.lstAssetMake = response.assetDetailVO.lstAssetMake;
                                    $scope.lstAssetModel = response.assetDetailVO.lstAssetModel;

//$scope.selectassetmake = "0";
                                    //$scope.selectassetmodel = "0";
// angular.forEach(response.assetDetailVO.lstAssetMake, function(assetmake,
// index) {
// var tempObject = {modelId: assetmake.modelId, make: assetmake.make};
// $scope.assetmakeArr.push(tempObject);
// });
//
// $scope.selectassetmake = $scope.assetmakeArr[0].modelId
//
// angular.forEach(response.assetDetailVO.lstAssetModel, function(assetmodel,
// index) {
// var tempObject = {modelId: assetmodel.modelId, modelNo: assetmodel.modelNo};
// $scope.assetmodelArr.push(tempObject);
// });
//
// $scope.selectassetmodel = $scope.assetmodelArr[0].modelId

// angular.forEach(response.assetDetailVO.lstAssetCategory, function(category,
// index) {
// var tempObject = {catgId: category.catgId, catgDesc: category.catgDesc};
// $scope.assetcategoryArr.push(tempObject);
// });
// $scope.selectassetcategory = $scope.assetcategoryArr[0].catgDesc
//                           
                                    if (response.data != null) {
                                        var assetData = response.data;
                                        $scope.selectmanufacture = assetData.manufacturer;

                                        $scope.tempselectmanufacture = assetData.manufacturer;
                                        $scope.tempselectassetmake = assetData.assetMake;
// $scope.selectassetmodel= $scope.assetmodelArr[0];
                                        $scope.tempModelId = assetData.assetModel;
                                        $scope.isExistingData = true;

// angular.forEach($scope.assetmodelArr, function(model, index) {
// console.log("3");
// if(model.modelId==assetData.assetModel){
// $scope.selectassetmodel=model;
// }
// console.log($scope.selectassetmodel);
// });

// $scope.selectassetcategory = assetData.assetCategory;
                                    }


                                }).error(function(error) {
                                    console.log(error);
                                });
                            };
// $scope.datereciept=new Date();
// $scope.applicationformno = "123";


                            $scope.$watch('selectmanufacture', function() {
                                if ($scope.assetmakeArr.length > 1) {
                                    $scope.assetmakeArr.splice(1, $scope.assetmakeArr.length);
                                }
                                $scope.selectassetmake = "0";
                                $scope.selectassetmodel = $scope.assetmodelArr[0].modelId;
//    if ($scope.selectmanufacture != $scope.manufacturerArr[0].manufacturerId) {

                                angular.forEach($scope.lstAssetMake, function(assetmake, index) {
                                    if (assetmake.manufacturerId == $scope.selectmanufacture) {
                                        var tempObject = {makeId: assetmake.modelId, make: assetmake.make};
                                        $scope.assetmakeArr.push(tempObject);
                                    }
                                });
                                //  }

                                if ($scope.isExistingData) {
                                    $scope.selectassetmake = $scope.tempselectassetmake;
                                }

                            }, true);


                            $scope.$watch('selectassetmake', function() {

                                if ($scope.assetmodelArr.length > 1) {
                                    $scope.assetmodelArr.splice(1, $scope.assetmodelArr.length);
                                }
                                //alert(2);
                                $scope.selectassetmodel = "0";
                                //alert($scope.selectassetmodel);
//    if ($scope.selectassetmake != $scope.assetmakeArr[0].makeId) {
                                /*   $scope.make = '';
                                 angular.forEach($scope.lstAssetMake, function(assetmake, index) {
                                 if (assetmake.modelId == $scope.selectassetmake) {
                                 $scope.make = assetmake.make;
                                 }
                                 });*/
                                $scope.make = '';
                                angular.forEach($scope.lstAssetMake, function(assetmake, index) {
                                    if (assetmake.modelId == $scope.selectassetmake) {
                                        $scope.make = assetmake.make;
                                    }
                                });

                                angular.forEach($scope.lstAssetModel, function(assetmodel, index) {
                                    // console.log("_"+assetmodel.manufacturerId+"_"+$scope.selectmanufacture+"&"+assetmodel.modelId+"_"+$scope.selectassetmake)
                                    if (assetmodel.manufacturerId == $scope.selectmanufacture
                                            && assetmodel.makeId == $scope.make) {

                                        var tempObject = {modelId: assetmodel.modelId, modelNo: assetmodel.modelNo, catgId: assetmodel.assetCatg};
                                        $scope.assetmodelArr.push(tempObject);
                                    }
                                });

                                if ($scope.isExistingData == true) {
                                    $scope.selectassetmodel = $scope.tempModelId;
                                    $scope.isExistingData = false;
                                }
//        angular.forEach($scope.assetmodelArr, function(model, index) {
//            if (model.modelId == $scope.tempModelId) {
//                $scope.selectassetmodel = model;
//            }
//            console.log($scope.selectassetmodel);
//        });
                            }, true
                                    );

                            $scope.validateAssetForm = function() {

                                if ($scope.selectmanufacture == null || $scope.selectmanufacture == "" || $scope.selectmanufacture == "0") {
                                    alert("Please Select Manufacturer ")
                                } else if ($scope.selectassetmake == null || $scope.selectassetmake == "0") {
                                    alert("Please Select Asset Make")
                                } else if ($scope.selectassetmodel == null || $scope.selectassetmodel == "0") {
                                    alert("Please Select Asset Model")
// } else if ($scope.selectassetcategory == null || $scope.selectassetcategory
// == "0") {
// alert("please Select Asset Category");
                                } else {
                                    $scope.saveCheck();
                                }



                            }
                            $scope.submitAssetForm = function() {
                                var callURL = $rootScope.urlDomain + "SaveAssetData.json";
                                angular.forEach($scope.assetmodelArr, function(assetmodel, index) {
                                    //console.log("_"+assetmodel.manufacturerId+"_"+$scope.selectmanufacture+"&"+assetmodel.modelId+"_"+$scope.selectassetmake)
                                    $scope.cat = assetmodel.catgId;
                                    if (assetmodel.modelId == $scope.selectassetmodel) {
                                        $scope.cat = assetmodel.assetCatg;

                                    }
                                });
                                if ($scope.cat == null || $scope.cat == "") {
                                    alert("Invalid Data");
                                    return false;
                                }

                                var data = {
                                    selectmanufacture: $scope.selectmanufacture,
                                    selectassetmake: $scope.selectassetmake,
                                    selectassetmodel: $scope.selectassetmodel,
                                    selectassetcategory: $scope.cat,
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                console.log(data);
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    // transformRequest: transformRequestAsFormPost,
                                    data: data
                                }).success(function(data) {
                                    console.log(data);
                                    if (data.status == "200") {
                                        $cookies.a = data.applicationId;
                                        $rootScope.applicationId = data.applicationId;
                                        if ($rootScope.progress < 6) {
                                            $rootScope.progress = 6;
                                        }
                                        $location.path('/presanction/loandetail');

                                        // $rootScope.openForm(6);
                                        console.log(data);
                                    } else {
                                        // alert(data.msg);
                                    }


                                }).error(function(error) {
                                    console.log(error);
                                });
                            }

                        }])
                    /*
                     * .controller("new_application_controller", ["$scope",
                     * "$rootScope", "$http", "$location", function($scope,
                     * $rootScope, $http, $location) {
                     * 
                     * $rootScope.openForm = function(formNumber) { //
                     * $rootScope.progress=formNumber; if (formNumber <=
                     * $rootScope.progress || $rootScope.progress >= 3) {
                     * updateTabs(formNumber, $rootScope.progress);
                     * enableForm(formNumber); } }; $scope.selectForm =
                     * function(formNo) {
                     * 
                     * $rootScope.openForm(formNo); } var enableForm =
                     * function(formNumber) { hideForms(); if (formNumber == 0) {
                     * $('#form_sourcing').css("display", "block"); } else if
                     * (formNumber == 1) {
                     * $('#form_applicant_details').css("display", "block");
                     * $rootScope.preloadApplicantDetails(); }
                     * 
                     * else if (formNumber == 2) {
                     * $('#form_address_details').css("display", "block");
                     * $rootScope.preloadAddressDetails(); } else if (formNumber ==
                     * 3) { $('#form_occupation_income').css("display",
                     * "block"); $rootScope.preloadOccupationIncome(); } else if
                     * (formNumber == 4) {
                     * $('#form_bank_details').css("display", "block");
                     * $rootScope.preloadBankDetails(); }
                     * 
                     * else if (formNumber == 5) {
                     * $('#form_asset_details').css("display", "block");
                     * $rootScope.preloadAssetDetails(); } else if (formNumber ==
                     * 6) { $('#form_loan_details').css("display", "block");
                     * $rootScope.preloadLoan(); } else if (formNumber == 7)
                     * $('#form_documents').css("display", "block");
                     * $rootScope.preloadDocument(); }
                     * 
                     * var updateTabs = function(formNumber, progress) { if
                     * (progress == 0) { setClass($('#tab_sourcing'),
                     * 'TabEnabled'); setClass($('#tab_applicant_details'),
                     * 'TabDisabled'); setClass($('#tab_address_details'),
                     * 'TabDisabled'); setClass($('#tab_occupation_income'),
                     * 'TabDisabled'); setClass($('#tab_bank_details'),
                     * 'TabDisabled'); setClass($('#tab_asset_details'),
                     * 'TabDisabled'); setClass($('#tab_loan_details'),
                     * 'TabDisabled'); setClass($('#tab_documents'),
                     * 'TabDisabled'); } else if (progress == 1) {
                     * setClass($('#tab_sourcing'), 'TabEnabled');
                     * setClass($('#tab_applicant_details'), 'TabEnabled');
                     * setClass($('#tab_address_details'), 'TabDisabled');
                     * setClass($('#tab_occupation_income'), 'TabDisabled');
                     * setClass($('#tab_bank_details'), 'TabDisabled');
                     * setClass($('#tab_asset_details'), 'TabDisabled');
                     * setClass($('#tab_loan_details'), 'TabDisabled');
                     * setClass($('#tab_documents'), 'TabDisabled'); } else if
                     * (progress == 2) { setClass($('#tab_sourcing'),
                     * 'TabEnabled'); setClass($('#tab_applicant_details'),
                     * 'TabEnabled'); setClass($('#tab_address_details'),
                     * 'TabEnabled'); setClass($('#tab_occupation_income'),
                     * 'TabDisabled'); setClass($('#tab_bank_details'),
                     * 'TabDisabled'); setClass($('#tab_asset_details'),
                     * 'TabDisabled'); setClass($('#tab_loan_details'),
                     * 'TabDisabled'); setClass($('#tab_documents'),
                     * 'TabDisabled'); } else if (progress == 3) {
                     * setClass($('#tab_sourcing'), 'TabEnabled');
                     * setClass($('#tab_applicant_details'), 'TabEnabled');
                     * setClass($('#tab_address_details'), 'TabEnabled');
                     * setClass($('#tab_occupation_income'), 'TabEnabled');
                     * setClass($('#tab_bank_details'), 'TabEnabled');
                     * setClass($('#tab_asset_details'), 'TabEnabled');
                     * setClass($('#tab_loan_details'), 'TabEnabled');
                     * setClass($('#tab_documents'), 'TabEnabled'); } else if
                     * (progress == 4) { setClass($('#tab_sourcing'),
                     * 'TabEnabled'); setClass($('#tab_applicant_details'),
                     * 'TabEnabled'); setClass($('#tab_address_details'),
                     * 'TabEnabled'); setClass($('#tab_occupation_income'),
                     * 'TabEnabled'); setClass($('#tab_bank_details'),
                     * 'TabEnabled'); setClass($('#tab_asset_details'),
                     * 'TabEnabled'); setClass($('#tab_loan_details'),
                     * 'TabEnabled'); setClass($('#tab_documents'),
                     * 'TabEnabled'); } else if (progress == 5) {
                     * setClass($('#tab_sourcing'), 'TabEnabled');
                     * setClass($('#tab_applicant_details'), 'TabEnabled');
                     * setClass($('#tab_address_details'), 'TabEnabled');
                     * setClass($('#tab_occupation_income'), 'TabEnabled');
                     * setClass($('#tab_bank_details'), 'TabEnabled');
                     * setClass($('#tab_asset_details'), 'TabEnabled');
                     * setClass($('#tab_loan_details'), 'TabEnabled');
                     * setClass($('#tab_documents'), 'TabEnabled'); } else if
                     * (progress == 6) { setClass($('#tab_sourcing'),
                     * 'TabEnabled'); setClass($('#tab_applicant_details'),
                     * 'TabEnabled'); setClass($('#tab_address_details'),
                     * 'TabEnabled'); setClass($('#tab_occupation_income'),
                     * 'TabEnabled'); setClass($('#tab_bank_details'),
                     * 'TabEnabled'); setClass($('#tab_asset_details'),
                     * 'TabEnabled'); setClass($('#tab_loan_details'),
                     * 'TabEnabled'); setClass($('#tab_documents'),
                     * 'TabEnabled'); } else if (progress == 7) {
                     * setClass($('#tab_sourcing'), 'TabEnabled');
                     * setClass($('#tab_applicant_details'), 'TabEnabled');
                     * setClass($('#tab_address_details'), 'TabEnabled');
                     * setClass($('#tab_occupation_income'), 'TabEnabled');
                     * setClass($('#tab_bank_details'), 'TabEnabled');
                     * setClass($('#tab_asset_details'), 'TabEnabled');
                     * setClass($('#tab_loan_details'), 'TabEnabled');
                     * setClass($('#tab_documents'), 'TabEnabled'); } if
                     * (formNumber == 0) setClass($('#tab_sourcing'),
                     * 'TabActive'); if (formNumber == 1)
                     * setClass($('#tab_applicant_details'), 'TabActive'); if
                     * (formNumber == 2) setClass($('#tab_address_details'),
                     * 'TabActive'); if (formNumber == 3)
                     * setClass($('#tab_occupation_income'), 'TabActive'); if
                     * (formNumber == 4) setClass($('#tab_bank_details'),
                     * 'TabActive'); if (formNumber == 5)
                     * setClass($('#tab_asset_details'), 'TabActive'); if
                     * (formNumber == 6) setClass($('#tab_loan_details'),
                     * 'TabActive'); if (formNumber == 7)
                     * setClass($('#tab_documents'), 'TabActive'); }
                     * 
                     * 
                     * var setClass = function(view, className) {
                     * view.removeClass('TabActive').removeClass('TabDisabled').removeClass('TabEnabled');
                     * view.addClass(className); }; var hideForms = function() {
                     * $('#form_sourcing').css("display", "none");
                     * $('#form_applicant_details').css("display", "none");
                     * $('#form_address_details').css("display", "none");
                     * $('#form_occupation_income').css("display", "none");
                     * $('#form_bank_details').css("display", "none");
                     * $('#form_asset_details').css("display", "none");
                     * $('#form_loan_details').css("display", "none");
                     * $('#form_documents').css("display", "none"); } //
                     * $rootScope.sendOTP = function sendOTP() { // // var
                     * callURL = $rootScope.urlDomain + "sendOtp.json"; // var
                     * data = { // userId: $rootScope.userId, // applicationId:
                     * $rootScope.applicationId // }; // $http({ // method:
                     * "post", // url: callURL, // // transformRequest:
                     * transformRequestAsFormPost, // data: data //
                     * }).success(function(response, status, headers, config) // { //
                     * console.log(response); // if (response.status == "200") { //
                     * alert(response.msg); // } else { // alert(response.msg); // } //
                     * }).error(function(data, status, headers, config) // { //
                     * alert("Error while requesting"); // }); // } // // // //
                     * $scope.userOtp = ""; // $rootScope.verifyOTP = function
                     * verifyOTP() { // // var callURL = $rootScope.urlDomain +
                     * "verifyOtp.json"; // console.log("otp " +
                     * $scope.userOtp); // var data = { // userId:
                     * $rootScope.userId, // applicationId:
                     * $rootScope.applicationId, // userOtp: $scope.userOtp // }; //
                     * $http({ // method: "post", // url: callURL, // //
                     * transformRequest: transformRequestAsFormPost, // data:
                     * data // }).success(function(response, status, headers,
                     * config) // { // console.log(response); // if
                     * (response.status == "200") { // alert(response.msg); // }
                     * else { // alert(response.msg); // } //
                     * }).error(function(data, status, headers, config) // { //
                     * alert("Error while requesting"); // }); // }
                     * 
                     * }])
                     * 
                     */
                    .controller("DocumentCtrl", ["$rootScope", "$cookies", "$scope", "$http", "$location", "Upload", "$route",
                        function($rootScope, $cookies, $scope, $http, $location, Upload, $route) {

                            $scope.formReload = function() {
                                $route.reload();
                            };

                            $scope.preloadDocuments = function() {
                                $scope.documentDataArr = [];
                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                var callURL = $rootScope.urlDomain + "getDocumentDetails.json";
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    if (response.status == '200') {
                                        angular.forEach(response.data, function(documentData, index) {
                                            var documentDataObj = {
                                                applicationId: documentData.applicantId,
                                                applicationName: documentData.applicationName,
                                                docArr: documentData.lstDocumentVO
                                            };

                                            $scope.documentDataArr.push(documentDataObj);

                                        });
                                    }

                                }).error(function(error) {
                                    console.log(error);
                                });
                            };

                            $scope.submitPreSanctionAdd = function submitPreSanctionAdd() {
                                var callURL = $rootScope.urlDomain + "addPresanction.json";
                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    console.log(response);
                                    if (response.status == "200") {
                                        alert("Application Submitted");
                                        $location.path('/postsanctiondashboard');
                                    } else {
                                        alert(response.msg);
                                    }
                                    // console.log(response);
                                }).error(function(error) {
                                    console.log(error);
                                });
                            };


                            $scope.submitQMS = function() {

                                var callURL = $rootScope.urlDomain + "sendQms.json";
                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId,
                                };
                                $http({
                                    method: "post",
                                    url: callURL,
                                    // transformRequest:
                                    // transformRequestAsFormPost,
                                    data: data
                                }).success(function(response, status, headers, config)
                                {
                                    console.log(response);
                                    if (response.status == "200") {
                                        alert(response.msg);
                                    } else {
                                        alert(response.msg);
                                    }
                                }).error(function(data, status, headers, config)
                                {
                                    alert("Error while requesting");
                                });
                            }

// $scope.downloadDoc = function(docId, childDocId) {
// alert("downloadDoc");
// alert(childDocId + " " + docId);
// var callURL = $rootScope.urlDomain + "download.json";
// var data = {
// userId: $rootScope.userId,
// applicationId: $rootScope.applicationId,
// docId:docId,
// childDocId:childDocId
// };
// $http({
// method: "post",
// url: callURL,
// // transformRequest: transformRequestAsFormPost,
// data: data
// }).success(function(response, status, headers, config)
// {
// console.log(response);
//
// }).error(function(data, status, headers, config)
// {
// alert("Error while requesting");
// });
//
// };

                            $scope.checkOsv = function() {

                            }

                            $scope.submitUpload = function(docItem, index) {
                                if (docItem.lstChildDocVO[index].feild1 == null || docItem.lstChildDocVO[index].feild1 == "") {
                                    alert("Please fill field 1");
                                } else if (docItem.lstChildDocVO[index].feild2 == null || docItem.lstChildDocVO[index].feild2 == "") {
                                    alert("Please fill field 2");
                                } else if (!docItem.lstChildDocVO[index].osv) {
                                    alert("Please check osv");
                                }
                                else if (docItem.lstChildDocVO[index].file
                                        && !docItem.lstChildDocVO[index].file.$error) {

//									if(!docItem.lstChildDocVO[index].file.type.localeCompare("text/html")){
//								  			alert(docItem.lstChildDocVO[index].file.type +"allowed");
//											return true;
//										}else{
//											alert(docItem.lstChildDocVO[index].file.type +"Not allowed");
//											return false;
//										}




//									if(docItem.lstChildDocVO[index].file.size >= 5000){
//								  
//										alert('Size Greater than 2kb not allowed');
//										  
//										 return false; 
//										}

                                    var data = {
                                        applicationId: $rootScope.applicationId,
                                        userId: $rootScope.userId,
                                        applicantId: docItem.applicantsId,
                                        docId: docItem.docId,
                                        receivedDate: docItem.receivedDate,
                                        childDocId: docItem.lstChildDocVO[index].childDocId,
                                        field1: docItem.lstChildDocVO[index].feild1,
                                        field2: docItem.lstChildDocVO[index].feild2,
                                        osv: docItem.lstChildDocVO[index].osv,
                                        disclamerNote: docItem.disclamerNote
                                    };
                                    $scope.upload(docItem.lstChildDocVO[index].file, data);
                                } else {
                                    alert("Invalid File");
                                }
                            };
                            $scope.upload = function(file, data) {


                                Upload.upload({
                                    url: $rootScope.urlDomain + 'upload.json',
                                    fields: {'data': data},
                                    file: file
                                }).progress(function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                                }).success(function(data, status, headers, config) {
                                    alert('file ' + config.file.name + 'uploaded');
                                    console.log('file ' + config.file.name + ' uploaded. Response: ' + data);
                                }).error(function(data, status, headers, config) {
                                    console.log('error status: ' + status);
                                })
                            };
                        }])


                    /*
                     * ================== Post Sanction Start Here *
                     * ==================
                     */



                    .controller("postSanctionDashboardCtrl", ["$cookies", "$rootScope", "$scope", "$filter", "$http", "$location", function($cookies, $rootScope, $scope, $filter, $http, $location) {
                            var init;
                            $scope.preloadPostSanctionDashboard = function preload() {
                                $rootScope.setCookies(0, 0);
                                var callURL = $rootScope.urlDomain + "postSanctionDashboard.json";
                                var data = {
                                    userId: $rootScope.userId
                                };
                                console.log($rootScope.userId + " " + callURL + " " + data);
                                $http({
                                    method: "post",
                                    url: callURL,
                                    data: data
                                }).success(function(response, status, headers, config)
                                {

                                    if (response.status == "200") {
                                        $scope.stores = response.lstPostSanctionDashbordVO,
                                                $scope.searchKeywords = "", $scope.filteredStores = [], $scope.row = "", $scope.select = function(page) {
                                            var end, start;
                                            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
                                        }, $scope.onFilterChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
                                        }, $scope.onNumPerPageChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1
                                        }, $scope.onOrderChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1
                                        }, $scope.search = function() {
                                            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
                                        }, $scope.order = function(rowName) {
                                            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
                                        }, $scope.numPerPageOpt = [5, 10, 15, 30, 50, 100], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageStores = [], (init = function() {
                                            return $scope.search(), $scope.select($scope.currentPage)
                                        })()

                                    } else {
                                        alert(response.msg);
                                    }
                                }).error(function(data, status, headers, config)
                                {
                                    alert("Error while requesting");
                                });
                            };
                            $scope.showform = function showform(appId, progress) {
                                $cookies.a = appId;
                                $rootScope.applicationId = appId;
                                $cookies.c = progress;
                                $rootScope.progress = progress;
                                $location.path('/postsanction/sourcing');
                            };
                        }]).controller("PostSanctionSourcingCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionSourcingForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    
                    
                    
                    $scope.saveCheckLpc = function(isVehicleDelivered) {
                        
                        if (isVehicleDelivered == null || isVehicleDelivered == "" || isVehicleDelivered == "0") {
                            alert("Please Select Vehicle Delivered");
                        } else {

                            var modalInstance;
                            modalInstance = $modal.open({
                                templateUrl: "confirmationContent.html",
                                controller: "ConfirmationModalInstanceCtrl",
                                resolve: {
                                    message: function() {
                                        return "Do you want to proceed for lpc ?";
                                    }
                                }
                            }), modalInstance.result.then(function(isSave) {
                                $log.info("Response: " + isSave);
                                if (isSave) {
                                    // Save
                                    $rootScope.doLpc(isVehicleDelivered);
                                }
                            }, function() {
                                $log.info("Modal dismissed at: " + new Date)
                            });
                        }
                    }

                    $scope.openPostNavigation = function() {
                        $("#postsanction-li").click();
                    };

                    $scope.preloadPostSanctionSourcing = function() {

                        $scope.openPostNavigation();

                        $scope.vehicleDeliveredArr = [{value: "0", desc: 'Select Vehicle Delivered'}, {value: "Y", desc: 'Yes'}, {value: "N", desc: 'No'}];
                        $scope.isVehicleDelivered = $scope.vehicleDeliveredArr[0].value;

                        $scope.subBrokerArr = [{value: "0", description: 'Select Sub Broker'}];
                        $scope.subLocationArr = [{value: "0", description: 'Select Sub Location'}];
//                        $scope.branchArr = [{branchId: "0", branchName: 'Select Branch'}];

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchPostSanctionSourcingData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {
                                angular.forEach(response.postSanctionSourcingVO.lstsubBroker, function(broker, index) {
                                    var tempObject = {value: broker.value, description: broker.description};
                                    $scope.subBrokerArr.push(tempObject);
                                });
                                $scope.postSanctionSubBroker = $scope.subBrokerArr[0].value;

                                angular.forEach(response.postSanctionSourcingVO.lstsubLocation, function(location, index) {
                                    var tempObject = {value: location.value, description: location.description};
                                    $scope.subLocationArr.push(tempObject);
                                });
                                $scope.postSanctionSubLocation = $scope.subLocationArr[0].value;

                                if (response.data != null) {
                                    var postSourcingData = response.data;
                                    
                                   angular.forEach($scope.subBrokerArr, function(item, index) {
//                                       alert(postSourcingData.subBrokerName);
//                                       alert(item.description);
                                        if(postSourcingData.subBrokerName==item.description){
                                            $scope.postSanctionSubBroker=item;
                                        }
                                    });
                                    angular.forEach($scope.subLocationArr, function(item, index) {
//                                       alert(postSourcingData.subBrokerName);
//                                       alert(item.description);
                                        if(postSourcingData.subLocationName==item.description){
                                            $scope.postSanctionSubLocation=item;
                                        }
                                    });
//                                    $scope.postSanctionSubBroker = postSourcingData.subBroker;
//                                    
//                                    $scope.postSanctionSubLocation = postSourcingData.subLocation;
                                    
                                    $scope.postSanctionBranch = response.postSanctionSourcingVO.branchDescription;
                                    $scope.postSanctionBranchId = response.postSanctionSourcingVO.branchId;
                                    $scope.createdDate = $filter('date')(postSourcingData.createdDate, 'dd-MM-yyyy');
                                    $scope.modDate = $filter('date')(postSourcingData.modDate, 'dd-MM-yyyy');

                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };
                    $scope.validatePostSanctionSourcingForm = function() {

                        if ($scope.postSanctionSubBroker.value != '0' && ($scope.postSanctionSubLocation.value == null || $scope.postSanctionSubLocation.value == "0"))
                        {
                            alert("Please Select Sub Location");
                        }
                        else {
                            $scope.saveCheck();
                        }

                    }
                    $scope.submitPostSanctionSourcingForm = function() {



                        var callURL = $rootScope.urlDomain + "SavePostSanctionSourcingData.json";
                        
//                        alert($scope.postSanctionSubBroker.value);
//                        alert($scope.postSanctionSubBroker.description);
                        
                        var data = {
                            
                            
                            postSanctionSubBroker: $scope.postSanctionSubBroker.value,
                            postSanctionSubLocation: $scope.postSanctionSubLocation.value,
                            postSanctionSubBrokerName: $scope.postSanctionSubBroker.description,
                            postSanctionSubLocationName: $scope.postSanctionSubLocation.description,
                            postSanctionBranch: $scope.postSanctionBranchId,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                $cookies.a = data.applicationId;
                                $rootScope.applicationId = data.applicationId;
// $rootScope.progress=1;
// $rootScope.openForm(1);
                                $location.path('/postsanction/assetdetail');
// alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }




                }]).controller("postSanctionDocumentCtrl", ["$rootScope", "$cookies", "$scope", "$http", "$location", "Upload", "$route",
                        function($rootScope, $cookies, $scope, $http, $location, Upload, $route) {
                            

                            $scope.formReload = function() {
                                $route.reload();
                            };

                            $scope.preloadDocuments = function() {
                                $scope.documentDataArr = [];
                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                var callURL = $rootScope.urlDomain + "getPostDocumentDetails.json";
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    if (response.status == '200') {
                                        angular.forEach(response.data, function(documentData, index) {
                                            var documentDataObj = {
                                                applicationId: documentData.applicantId,
                                                applicationName: documentData.applicationName,
                                                docArr: documentData.lstDocumentVO
                                            };

                                            $scope.documentDataArr.push(documentDataObj);

                                        });
                                    }

                                }).error(function(error) {
                                    console.log(error);
                                });
                            };


                            $scope.submitPostSanctionAdd = function submitPostSanctionAdd() {
                                var callURL = $rootScope.urlDomain + "addPostsanction.json";
                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                };
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    console.log(response);
                                    if (response.status == "200") {
                                        alert("Application Submitted");
                                        $location.path('/postsanctiondashboard');
                                    } else {
                                        alert(response.msg);
                                    }
                                    // console.log(response);
                                }).error(function(error) {
                                    console.log(error);
                                });
                            };
//
//                            $scope.submitQMS = function() {
//
//                                var callURL = $rootScope.urlDomain + "sendQms.json";
//                                var data = {
//                                    userId: $rootScope.userId,
//                                    applicationId: $rootScope.applicationId,
//                                };
//                                $http({
//                                    method: "post",
//                                    url: callURL,
//                                    // transformRequest:
//                                    // transformRequestAsFormPost,
//                                    data: data
//                                }).success(function(response, status, headers, config)
//                                {
//                                    console.log(response);
//                                    if (response.status == "200") {
//                                        alert(response.msg);
//                                    } else {
//                                        alert(response.msg);
//                                    }
//                                }).error(function(data, status, headers, config)
//                                {
//                                    alert("Error while requesting");
//                                });
//                            }

// $scope.downloadDoc = function(docId, childDocId) {
// alert("downloadDoc");
// alert(childDocId + " " + docId);
// var callURL = $rootScope.urlDomain + "download.json";
// var data = {
// userId: $rootScope.userId,
// applicationId: $rootScope.applicationId,
// docId:docId,
// childDocId:childDocId
// };
// $http({
// method: "post",
// url: callURL,
// // transformRequest: transformRequestAsFormPost,
// data: data
// }).success(function(response, status, headers, config)
// {
// console.log(response);
//
// }).error(function(data, status, headers, config)
// {
// alert("Error while requesting");
// });
//
// };

                            $scope.checkOsv = function() {

                            }

                            $scope.submitUpload = function(docItem, index) {
                                if (docItem.lstChildDocVO[index].feild1 == null || docItem.lstChildDocVO[index].feild1 == "") {
                                    alert("Please fill field 1");
                                } else if (docItem.lstChildDocVO[index].feild2 == null || docItem.lstChildDocVO[index].feild2 == "") {
                                    alert("Please fill field 2");
                                } else if (!docItem.lstChildDocVO[index].osv) {
                                    alert("Please check osv");
                                }
                                else if (docItem.lstChildDocVO[index].file
                                        && !docItem.lstChildDocVO[index].file.$error) {

//									if(!docItem.lstChildDocVO[index].file.type.localeCompare("text/html")){
//								  			alert(docItem.lstChildDocVO[index].file.type +"allowed");
//											return true;
//										}else{
//											alert(docItem.lstChildDocVO[index].file.type +"Not allowed");
//											return false;
//										}




//									if(docItem.lstChildDocVO[index].file.size >= 5000){
//								  
//										alert('Size Greater than 2kb not allowed');
//										  
//										 return false; 
//										}

                                    var data = {
                                        applicationId: $rootScope.applicationId,
                                        userId: $rootScope.userId,
                                        applicantId: docItem.applicantsId,
                                        docId: docItem.docId,
                                        receivedDate: docItem.receivedDate,
                                        childDocId: docItem.lstChildDocVO[index].childDocId,
                                        field1: docItem.lstChildDocVO[index].feild1,
                                        field2: docItem.lstChildDocVO[index].feild2,
                                        osv: docItem.lstChildDocVO[index].osv,
                                        disclamerNote: docItem.disclamerNote
                                    };
                                    $scope.upload(docItem.lstChildDocVO[index].file, data);
                                } else {
                                    alert("Invalid File");
                                }
                            };
                            $scope.upload = function(file, data) {


                                Upload.upload({
                                    url: $rootScope.urlDomain + 'postDocUpload.json',
                                    fields: {'data': data},
                                    file: file
                                }).progress(function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                                }).success(function(data, status, headers, config) {
                                    alert('file ' + config.file.name + 'uploaded');
                                    console.log('file ' + config.file.name + ' uploaded. Response: ' + data);
                                }).error(function(data, status, headers, config) {
                                    console.log('error status: ' + status);
                                })
                            };
                        }]).controller("PostSanctionBankCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $modal, $log) {
                    // Avinash 17-12-2015//

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionBankForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                     $scope.bankNameArr = [{value: "0", description: 'Select Bank Name'}];
                     $scope.accountTypeArr = [{value: "0", description: 'Select Account Type'}];
                        
                    $scope.bankObj = {bankId: "", postSanctionSelectBankName: $scope.bankNameArr[0].value, postSanctionSelectAccountType: $scope.accountTypeArr[0].value, postSanctionAccountNo: "", postSanctionBankVintageYears: "", postSanctionBankVintageMonths: "", postSanctionAverageBankBalance: ""};
                    $scope.parentBankArr = [];


                    $scope.addBankDetails = function(index) {
                        $scope.parentBankArr[index].bankArr.push($scope.bankObj);
                    }

                    $scope.deleteBank = function(parentIndex, index) {

                        $scope.parentBankArr[parentIndex].deleteBankDetails.push({bankId: $scope.parentBankArr[parentIndex].bankArr[index].bankId});
                        $scope.parentBankArr[parentIndex].bankArr.splice(index, 1);
                    }


                    $rootScope.preloadPostSanctionBankDetails = function() {
                       
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }

                        var callURL = $rootScope.urlDomain + "FetchPostSanctionBankData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.sts == "200") {

                                angular.forEach(response.bankDetailsVo.lstBank, function(bankName, index) {
                                    var tempObject = {value: bankName.value, description: bankName.description};
                                    $scope.bankNameArr.push(tempObject);
                                });

                                $scope.postSanctionSelectBankName = $scope.bankNameArr[0].value;
                                angular.forEach(response.bankDetailsVo.lstAccountType, function(bankType, index) {
                                    var tempObject = {value: bankType.value, description: bankType.description};
                                    $scope.accountTypeArr.push(tempObject);
                                });

                                $scope.postSanctionSelectAccountType = $scope.accountTypeArr[0].value;

                                if (response.data != null && response.data.length > 0) {

                                    $scope.parentBankArr = [];

                                    angular.forEach(response.data, function(item, index) {

                                        var bankArr = [];

                                        if (item.lstBank != null && item.lstBank.length > 0) {
                                            angular.forEach(item.lstBank, function(bankDetails, index) {


                                                var bankData = {
                                                    bankId: bankDetails.bankId,
                                                    postSanctionSelectBankName: bankDetails.bankName,
                                                    postSanctionSelectAccountType: bankDetails.accountType,
                                                    postSanctionAccountNo: bankDetails.accountNumber,
                                                    postSanctionBankVintageYears: bankDetails.bankVintageYears,
                                                    postSanctionBankVintageMonths: bankDetails.bankVintageMonths,
                                                    postSanctionAverageBankBalance: bankDetails.avgBankBalance
                                                };
                                                bankArr.push(bankData);

                                            });
                                        } else {
                                            bankArr.push($scope.bankObj);
                                        }

                                        var bank = {
                                            bankArr: bankArr,
                                            deleteBankDetails: [],
                                            applicantId: item.applicantId,
                                            applicantName: item.applicantName,
                                            bank: item.bank

                                        };

                                        $scope.parentBankArr.push(bank);
                                    });
                                }
                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }

                    $scope.checkMonth = function(monthsValue) {
                        var months = /^(0?[0-9]|1[011])$/;
                        return months.test(monthsValue);
                    }

                    $scope.checkAvgBalance = function(avgBalance) {
                        var checkBalance = /^[\d\.]*$/;
                        return checkBalance.test(avgBalance);
                    }

                    $scope.validatePostSanctionBankForm = function(index) {

                        var parentBank = $scope.parentBankArr[index];

                        var tempFlag = true;

                        angular.forEach(parentBank.bankArr, function(bank, index) {
                            // if (bank.postSanctionSelectBankName == null ||
                            // bank.postSanctionSelectBankName == "")
                            //
                            // {
                            // $(".error").show();
                            // alert("Please Select Bank Name");
                            // tempFlag = false;
                            // return false;
                            // }
                            if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (bank.postSanctionSelectAccountType == null || bank.postSanctionSelectAccountType == "" || bank.postSanctionSelectAccountType == "0"))
                            {
                                $(".error").show();
                                alert("Please Select Account Type");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (bank.postSanctionAccountNo == null || bank.postSanctionAccountNo == ""))
                            {
                                $(".error").show();
                                alert("Please Enter Account Number");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (!$scope.checkDecimal(bank.postSanctionAccountNo)))
                            {
                                $(".error").show();
                                alert("Alphabets,Special Character and Decimal value are not allowed for Account Number");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (bank.postSanctionBankVintageYears == null || bank.postSanctionBankVintageYears == ""))
                            {
                                $(".error").show();
                                alert("Please Enter Vintage Years");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (!$scope.checkDecimal(bank.postSanctionBankVintageYears)))
                            {
                                $(".error").show();
                                alert("Alphabets,Special Character and Decimal Value are not allowed for  Vintage Years");
                                tempFlag = false;
                                return false;
                                } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (bank.postSanctionBankVintageMonths == null || bank.postSanctionBankVintageMonths == ""))
                            {
                                $(".error").show();
                                alert("Please Enter Vintage Months");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (!$scope.checkDecimal(bank.postSanctionBankVintageMonths)))
                            {
                                $(".error").show();
                                alert(" Alphabets,Special Character and Decimal Value are not allowed for  Vintage Months");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (!$scope.checkMonth(bank.postSanctionBankVintageMonths)))
                            {
                                $(".error").show();
                                alert(" Vintage Months not allowed more than 11");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (bank.postSanctionBankVintageYears == "0" && bank.postSanctionBankVintageMonths == "0"))
                            {
                                $(".error").show();
                                alert("Please Fill years and months at Bank Vintage  as zero is not allowed for both field");
                                tempFlag = false;
                                return false;
                            }
                            else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != 0) && (bank.postSanctionAverageBankBalance == null || bank.postSanctionAverageBankBalance == ""))
                            {
                                $(".error").show();
                                alert("Please Enter Average Balance");
                                tempFlag = false;
                                return false;
                            } else if ((bank.postSanctionSelectBankName != null && bank.postSanctionSelectBankName != "" && bank.postSanctionSelectBankName != "0") && (!$scope.checkAvgBalance(bank.postSanctionAverageBankBalance)))
                            {
                                $(".error").show();
                                alert("Alphabets and Special Charecters are not allowed for  Average Balance");
                                tempFlag = false;
                                return false;
//                            }
//                            else if (isNaN(bank.postSanctionAccountNo))
//                            {
//                                $(".error").show();
//                                alert("Please Enter Account Number in digits");
//                                tempFlag = false;
//                                return false;
//                            }
//                            else if (isNaN(bank.postSanctionBankVintageYears)) {
//                                $(".error").show();
//                                alert("Please Enter Bank Vintage Years in Numbers");
//                                tempFlag = false;
//                                return false;
//                            }
//                            else if (isNaN(bank.postSanctionBankVintageMonths)) {
//                                $(".error").show();
//                                alert("Please Enter Bank Vintage Months in Numbers");
//                                tempFlag = false;
//                                return false;
//                            } else if (isNaN(bank.postSanctionAverageBankBalance)) {
//                                $(".error").show();
//                                alert("Please Enter Average Bank Balance in Numbers");
//                                tempFlag = false;
//                                return false;
                            } else
                            {
                                $(".error").hide();
                            }
                        });

                        if (tempFlag) {
                            $scope.saveCheck(index);
                        }

                    }

                    $scope.submitPostSanctionBankForm = function(index) {
                        var parentBank = $scope.parentBankArr[index];

                        var callURL = $rootScope.urlDomain + "SavePostSanctionBankData.json";
                        var data = {
                            bankArr: parentBank.bankArr,
                            deleteBankDetails: parentBank.deleteBankDetails,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantId: parentBank.applicantId
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            $cookies.a = data.applicationId;
                            $rootScope.applicationId = data.applicationId;
                            if ($rootScope.progress < 5) {
                                $rootScope.progress = 5;
                            }

// $rootScope.openForm(5);
                            $location.path('/postsanction/insurancedetail');
                        }).error(function(error) {
                            console.log(error);
                        });

                    }

                    // Avinash 17-12-2015//

                }]).controller("postSanctionDisbursementCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {
                    // Avinash17-12-2015//

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                $scope.submitPostSanctionDisBursementForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $rootScope.preloadPostSanctionDisbursement = function() {
                        $scope.defaultPayMode = 'Q';

                        $scope.banknameArr = [{bankacc: "0", bankname: 'Select Bank Name'}];
                        $scope.paymentmodeArr = [{value: "0", description: 'Select Payment Mode'}];
                        $scope.disbursalModeArr = [{value: "0", description: 'Select Disbursal Mode'}];
                        $scope.accountNumberArr = [{bankacc: "0", bankdescription: 'Select Account No', ifscCode: "--"}];
                        $scope.dueDateArr = [{value: "0", description: 'Select Due Date'}, {value: '5', description: "5"}, {value: '7', description: "7"}];

                        $scope.dueDate = $scope.dueDateArr[0].value;

                        $scope.accountList = "";

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchDisbursementDetailsData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {                               

                                angular.forEach(response.disbursementDetailsVO.lstpaymentMode, function(paymentmode, index) {
                                    var tempObject = {value: paymentmode.value, description: paymentmode.description};
                                    $scope.paymentmodeArr.push(tempObject);
                                });
                                $scope.postSanctionSelectPaymentMode = $scope.defaultPayMode;

                                angular.forEach(response.disbursementDetailsVO.lstdisbursalMode, function(disbursalMode, index) {
                                    var tempObject = {value: disbursalMode.value, description: disbursalMode.description};
                                    $scope.disbursalModeArr.push(tempObject);
                                });
                                $scope.disbursalMode = $scope.disbursalModeArr[0].value;
                                
                                 angular.forEach(response.disbursementDetailsVO.lstbankName, function(name, index) {
                                    var tempObject = {bankacc: name.bankCode, bankname: name.bankname
                                    };
                                    $scope.banknameArr.push(tempObject);
                                });
                                $scope.postSanctionSelectBankName = $scope.banknameArr[0];

                                $scope.accountList = response.disbursementDetailsVO.lstbankAccountNo;

                                if (response.data != null) {
                                    var disbursementData = response.data;
                                    if(disbursementData.paymentMode!=null){
                                        $scope.postSanctionSelectPaymentMode = disbursementData.paymentMode;
                                    }
                                    if(disbursementData.disbursalMode!=null){
                                        $scope.disbursalMode = disbursementData.disbursalMode;
                                    }

                                    $scope.inFavorOf = disbursementData.inFavorOf;
                                    
                                    angular.forEach($scope.banknameArr, function(item, index) {
                                        if(disbursementData.bankName==item.bankname){
                                            $scope.postSanctionSelectBankName=item;
                                        }
                                    });
                                    
                                    $scope.bankAccountNoData=disbursementData.bankAccNo;                                    
                                    $scope.disbursementAmount = disbursementData.disbursementAmount;
                                    $scope.dueDate = disbursementData.dueDate;
                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.$watch('postSanctionSelectBankName', function() {
                        if ($scope.accountNumberArr.length > 1) {
                            $scope.accountNumberArr.splice(1, $scope.accountNumberArr.length);
                        }
                        $scope.bankAccountNo = $scope.accountNumberArr[0];
                        angular.forEach($scope.accountList, function(account, index) {
                            if (account.bankName == $scope.postSanctionSelectBankName.bankname) {
                                var tempObject = {bankacc: account.bankAccountNumber, bankdescription: account.bankAccountNumber,
                                    ifscCode: account.ifscCode};
                                $scope.accountNumberArr.push(tempObject);
                                
                                if($scope.bankAccountNoData==account.bankAccountNumber) {                                    
                                            $scope.bankAccountNo=tempObject;
                                            $scope.bankAccountNoData = "";
                                        }
                                
                                
                            }

                        });


                    }, true);


                    $scope.validatePostSanctionDisBursementForm = function() {

                        if ($scope.postSanctionSelectPaymentMode == null || $scope.postSanctionSelectPaymentMode == "0")
                        {
                            alert("Please Select Payment Mode");
                        } else if ($scope.disbursalMode == null || $scope.disbursalMode == "0")
                        {
                            alert("Please Select Disbursal Mode");
                        } else if ($scope.postSanctionSelectBankName.bankacc == null || $scope.postSanctionSelectBankName.bankacc == "0")
                        {
                            alert("Please Select Bank Name");
                        } else if ($scope.bankAccountNo.bankacc == null || $scope.bankAccountNo.bankacc == "0")
                        {
                            alert("Please Select Bank Account Number");
                        } else if ($scope.bankAccountNo.ifscCode == null || $scope.bankAccountNo.ifscCode == "0")
                        {
                            alert("Please Contact to System Admin");
                        } else if ($scope.dueDate == null || $scope.dueDate == "")
                        {
                            alert("Please Select Due Date");
                        }
                        else {
                            $scope.saveCheck();
                        }
                    }

                    $scope.submitPostSanctionDisBursementForm = function() {



                        var callURL = $rootScope.urlDomain + "SavePostSanctionDisbursementDetailsData.json";
                        var data = {
                            paymentMode: $scope.postSanctionSelectPaymentMode,
                            disbursalMode: $scope.disbursalMode,
                            inFavorOf: $scope.inFavorOf,
                            bankName: $scope.postSanctionSelectBankName.bankname,
                            bankCode: $scope.postSanctionSelectBankName.bankacc,
                            ifscNo: $scope.bankAccountNo.ifscCode,
                            bankAccNo: $scope.bankAccountNo.bankacc,
                            disbursementAmount: $scope.disbursementAmount,
                            dueDate: $scope.dueDate,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                $cookies.a = data.applicationId;
                                $rootScope.applicationId = data.applicationId;
                                $location.path('/postsanction/bankdetail');
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });


                    }


                }]).controller("postSanctionAssetInsuranceCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {
                    // Avinash 17-12-2015//

                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionAssetInsuranceForm();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $scope.today = function() {
                        return $scope.dt = new Date
                    };
                    $scope.showWeeks = !0;
                    $scope.toggleWeeks = function() {
                        return $scope.showWeeks = !$scope.showWeeks
                    };
                    $scope.clear = function() {

                        return $scope.dt = null
                    };
                    $scope.today();
                    $scope.disabled = function(date, mode) {
                        // return "day" === mode && (0 === date.getDay() || 6
                        // === date.getDay())
                        return "day" === mode && ($scope.dt < date)
                    };
                    $scope.toggleMin = function() {
                        var _ref;
                        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                            "null": new Date
                        }
                    };
                    $scope.toggleMin();
                    $scope.openIssued = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.openedIssued = !0
                    };
                    $scope.openEnd = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.openedEnd = !0
                    };
                    $scope.dateIssued = {
                        "year-format": "'yyyy'",
                        "starting-day": 1,
                        "show-weeks": false,
                        "datepicker-mode": 'year',
                        "year-range": 50,
                        "format-day-title": 'MMMM',
                        "format-month-title": 'yyyy',
                    };

                    $scope.dateEnd = {
                        "year-format": "'yyyy'",
                        "starting-day": 1,
                        "show-weeks": false,
                        "datepicker-mode": 'year',
                        "year-range": 50,
                        "format-day-title": 'MMMM',
                        "format-month-title": 'yyyy',
                    };

                    $scope.insuranceIssuedDate = null;
                    $scope.insuranceEndDate = null;

                    $scope.formats = ["dd-MM-yyyy", "yyyy/MM/dd", "shortDate"];
                    $scope.format = $scope.formats[0];

                    $rootScope.preloadpostSanctionAssetInsurance = function() {

                        $scope.companyNameArr = [{insurcoid: "0", insurcodesc: 'Select Insurance Company Name'}];
                        $scope.assetMakeArr = [{makeId: "0", make: 'Select Asset Make '}];
                        $scope.assetModelArr = [{modelId: "0", modelNo: 'Select Asset Model'}];
                        $scope.supplierArr = [{supplierId: "0", supplierdesc: 'Select Supplier'}];


                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchAssetInsuranceData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {
                                angular.forEach(response.assetInsuranceVO.lstinsuranceCompanyName, function(companyname, index) {
                                    var tempObject = {insurcoid: companyname.insurcoid, insurcodesc: companyname.insurcodesc};
                                    $scope.companyNameArr.push(tempObject);
                                });
                                $scope.postSanctionInsuranceCompany = $scope.companyNameArr[0];


                                if (response.data != null) {
                                    var assetInsuranceData = response.data;
                                    var _dateIssue = $filter('date')(assetInsuranceData.insuIssuedDate, 'dd-MM-yyyy');
                                    var _dateEnd = $filter('date')(assetInsuranceData.insuEndDate, 'dd-MM-yyyy');

//                                    $scope.postSanctionInsuranceCompany = assetInsuranceData.insurenceCompanyName;
                                    
                                    angular.forEach($scope.companyNameArr, function(item, index) {
                                        if(assetInsuranceData.insurenceCompanyName==item.insurcodesc){
                                            $scope.postSanctionInsuranceCompany=item;
                                        }
                                    });
                                    $scope.premiumAmount = assetInsuranceData.premiumAmount;
                                    $scope.idv = assetInsuranceData.idv;
                                    $scope.insuranceIssuedDate = _dateIssue;
                                    $scope.insuranceEndDate = _dateEnd;
                                    $scope.policyNumber = assetInsuranceData.policyNo;
                                    $scope.coverNoteNumber = assetInsuranceData.coverNoteNo;
                                    $scope.chassisNumber = response.assetInsuranceVO.chassisNos;
                                    $scope.engineNumber = response.assetInsuranceVO.engineNumber;
                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.checkDecimalValue = function(checkValue) {
                        var value = /^[0-9]{1,}(\.[0-9]{1,2})?$/;
                        return value.test(checkValue);
                    }

                    $scope.checkAlphaNumaricWithoutSpace = function(alphaNum) {
                        var check = /^[a-zA-Z\d]+$/;
                        return check.test(alphaNum);
                    }

                    $scope.validatePostSanctionAssetInsuranceForm = function() {
                        
                        var _dateIssue = $filter('date')($scope.insuranceIssuedDate, 'dd-MM-yyyy');
                        var _dateEnd = $filter('date')($scope.insuranceEndDate, 'dd-MM-yyyy');

                        if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && ($scope.premiumAmount == null || $scope.premiumAmount == ""))
                        {
                            alert("Please Fill Premium Amount");

                        } else if ( ($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && (!$scope.checkDecimalValue($scope.premiumAmount)))
                        {
                            alert("Only Numbers allowed for Premium Amount!\nPremium Amount is allowed 2 digit after decimal");
                        }
                        else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && ($scope.idv == null || $scope.idv == ""))
                        {
                            alert("Please Fill IDV");
                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && (!$scope.checkDecimalValue($scope.idv)))
                        {
                            alert("Only Numbers allowed for IDV!\nIDV is allowed 2 digit after decimal");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && ($scope.insuranceIssuedDate == null || $scope.insuranceIssuedDate == "0"))
                        {
                            alert("Please Select Insurance Issued Date");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && ($scope.insuranceEndDate == null || $scope.insuranceEndDate == "0"))
                        {
                            alert("Please Select Insurance End Date");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && (Date.parse(_dateIssue) >= Date.parse(_dateEnd))) {
                            alert("Invalid Date Range!\nStart Date cannot be after End Date!");
                            
                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && $scope.policyNumber == null || $scope.policyNumber == "")
                        {
                            alert("Please Fill Policy Number");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && (!$scope.checkAlphaNumaricWithoutSpace($scope.policyNumber)))
                        {
                            alert("Alphabets and number are allowed for  Policy Number");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && $scope.coverNoteNumber == null || $scope.coverNoteNumber == "")
                        {
                            alert("Please Fill Cover Note Number");

                        } else if (($scope.postSanctionInsuranceCompany != null && $scope.postSanctionInsuranceCompany != "0" && $scope.postSanctionInsuranceCompany != "") && (!$scope.checkAlphaNumaricWithoutSpace($scope.coverNoteNumber)))
                        {
                            alert("Alphabets and number are allowed for Cover Note Number");

                        }

                        else {
                            $scope.saveCheck();
                        }
                    }
                    $scope.submitPostSanctionAssetInsuranceForm = function() {

                        var _dateIssue = $filter('date')($scope.insuranceIssuedDate, 'dd-MM-yyyy');
                        var _dateEnd = $filter('date')($scope.insuranceEndDate, 'dd-MM-yyyy');


                        var callURL = $rootScope.urlDomain + "SavePostSanctionAssetInsuranceData.json";
                        var data = {
                        	insurenceCompanyId: $scope.postSanctionInsuranceCompany.insurcoid,
                        	insurenceCompanyName: $scope.postSanctionInsuranceCompany.insurcodesc,
                            premiumAmount: $scope.premiumAmount,
                            idv: $scope.idv,
                            insuIssuedDate: _dateIssue,
                            insuEndDate: _dateEnd,
                            policyNo: $scope.policyNumber,
                            coverNoteNo: $scope.coverNoteNumber,
                            chassisNo: $scope.chassisNumber,
                            engineNo: $scope.engineNumber,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                $cookies.a = data.applicationId;
                                $rootScope.applicationId = data.applicationId;
// $rootScope.progress = 1;
// $rootScope.openForm(5);
                                $location.path('/postsanction/referencedetail');
// alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }

                    // Avinash 17-12-2015//


                }]).controller("postSanctionInsuranceDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {

                    // anuya 18_12_15


                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionInsuranceForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $scope.today = function() {
                        return $scope.dt = new Date
                    };
                    $scope.showWeeks = !0;
                    $scope.toggleWeeks = function() {
                        return $scope.showWeeks = !$scope.showWeeks
                    };
                    $scope.clear = function() {

                        return $scope.dt = null
                    };
                    $scope.today();
                    $scope.disabled = function(date, mode) {
                        // return "day" === mode && (0 === date.getDay() || 6
                        // === date.getDay())
                        return "day" === mode && ($scope.dt < date)
                    };
                    $scope.toggleMin = function() {
                        var _ref;
                        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                            "null": new Date
                        }
                    };
                    $scope.toggleMin();
                    $scope.open = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
                    };
                    $scope.dateOptions = {
                        "year-format": "'yyyy'",
                        "starting-day": 1,
                        "show-weeks": false,
                        "datepicker-mode": 'year',
                        "year-range": 50,
                        "format-day-title": 'MMMM',
                        "format-month-title": 'yyyy',
                    };

                    $scope.healthDeclarationDate = null;
                    $scope.formats = ["yyyy-MM-dd", "dd-MM-yyyy", "shortDate"];
                    $scope.format = $scope.formats[1];

                    $scope.lifeInsurenceArr = [];

                    $rootScope.preloadPostSanctionInsuranceDetails = function() {

                        $scope.genderArr = [{value: "0", description: 'Select Gender'}];
                        $scope.companyNameArr = [{insurcoid: "0", insurcodesc: 'Select Insurance Company'}];
                        $scope.relationArr = [{value: "0", description: 'Select Relation With Nominee'}];
                        $scope.premiumCollectedArr = [{value: "0", description: 'Select Premium Collected'}, {value: 'Upfront', description: "Upfront"}, {value: 'Funded', description: "Funded"}, {value: 'Not Applicable', description: "Not Applicable"}];

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                        }
                        var callURL = $rootScope.urlDomain + "FetchLifeInsuranceData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {
                                angular.forEach(response.lifeInsuranceVO.lstgender, function(gender, index) {
                                    var tempObject = {value: gender.value, description: gender.description};
                                    $scope.genderArr.push(tempObject);
                                });
//                                $scope.postSanctionSelectGender = $scope.genderArr[0].value;

                                angular.forEach(response.lifeInsuranceVO.lstrelationWithNominee, function(relation, index) {
                                    var tempObject = {value: relation.value, description: relation.description};
                                    $scope.relationArr.push(tempObject);
                                });
//                                $scope.postSanctionSelectRelationWithNominee = $scope.relationArr[0].value;

                                angular.forEach(response.lifeInsuranceVO.lstinsuranceCompanyName, function(company, index) {
                                    var tempObject = {insurcoid: company.insurcoid, insurcodesc: company.insurcodesc};
                                    $scope.companyNameArr.push(tempObject);
                                });
//                                $scope.postSanctionInsuranceCompany = $scope.companyNameArr[0].insurcoid;

                                if (response.data != null && response.data.length > 0) {


                                    //anuya 18_12_15 (applicant data)
                                    angular.forEach(response.data, function(item, index) {

                                        var lifeInsurence = {};


                                        if (item.lifeInsurence != null) {
                                            lifeInsurence = {
                                                insuranceId: item.lifeInsurence.insuranceId,
                                                postSanctionSelectGender: item.lifeInsurence.gender,
                                                postSanctionSelectRelationWithNominee: item.lifeInsurence.relation,
                                                postSanctionInsuranceCompany: item.lifeInsurence.insurenceCompanyName,
                                                nomineeName: item.lifeInsurence.nomineeName,
                                                premiumCollectedUpFront: item.lifeInsurence.premiumCollectedUpfront,
                                                healthDeclarationDate: item.lifeInsurence.healthDeclarationDate,
                                                insurancePremiumCalculated: item.lifeInsurence.insurancePremiumCalculated,
                                                coverNoteNumber: item.lifeInsurence.coverNotNo,
                                                policyNumber: item.lifeInsurence.policyNo,
                                                barcodeNo: item.lifeInsurence.insuAppBarcodeNo,
                                                nomineeAddress: item.lifeInsurence.nomineeAddress

                                            }
                                        } else {
                                            lifeInsurence = {
                                                postSanctionSelectGender: $scope.genderArr[0].value,
                                                postSanctionSelectRelationWithNominee: $scope.relationArr[0].value,
                                                postSanctionInsuranceCompany: $scope.companyNameArr[0].insurcoid,
                                                nomineeName: "",
                                                premiumCollectedUpFront: $scope.premiumCollectedArr[0].value,
                                                healthDeclarationDate: "",
                                                insurancePremiumCalculated: "",
                                                coverNoteNumber: "",
                                                policyNumber: "",
                                                barcodeNo: "",
                                                nomineeAddress: ""
                                            }
                                        }

                                        var insuranceData = {
                                            applicantId: item.applicantId,
                                            applicantName: item.applicantName,
                                            bussinessDate: item.bussinessDate,
                                            dob:item.dob,
                                            lifeInsurence: lifeInsurence
                                        }

                                        $scope.lifeInsurenceArr.push(insuranceData);
                                    });

                                    //Anuya end

                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.validName = function(nomineeName) {
                        var nomName = /^[a-zA-Z'.,\s]{1,150}$/;
                        return nomName.test(nomineeName);
                    }

                    $scope.checkAlphaNumaricWithoutSpace = function(alphaNum) {
                        var check = /^[a-zA-Z\d]+$/;
                        return check.test(alphaNum);
                    }
                    
                    $scope.checkAddress = function(validAddress) {
                        var address = /^[a-zA-Z\s\d\/`~!@#$%^&*()-_+=.,?;:'"{}]+$/;
                        return address.test(validAddress);
                    }
                    
                    $scope.validDate = function(date) {
                        var dateRegex = /^([\d]{2}[-\/][\d]{2}[-\/][\d]{4})$/;
                        return dateRegex.test(date);
                    }

                    $scope.validatePostSanctionInsuranceForm = function(index) {




                        var lifeInsurence = $scope.lifeInsurenceArr[index].lifeInsurence;

//                        if (lifeInsurence.postSanctionInsuranceCompany == null || lifeInsurence.postSanctionInsuranceCompany == "0")
//                        {
//                            alert("Please Select Insurance Company");
//                        } else
                        if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.nomineeName == null || lifeInsurence.nomineeName == ""))
                        {
                            alert("Please Fill Nominee Name");
                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.validName(lifeInsurence.nomineeName))) {
                            alert("Special charecter and Number is not allowed for Nominee Name");
                        }
                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.postSanctionSelectRelationWithNominee == null || lifeInsurence.postSanctionSelectRelationWithNominee == "0"))
                        {
                            alert("Please Select Relation With Nominee");
                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.postSanctionSelectGender == null || lifeInsurence.postSanctionSelectGender == "0"))
                        {
                            alert("Please Select Gender");
                        }
                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.healthDeclarationDate == null || lifeInsurence.healthDeclarationDate == ""))
                        {
                            alert("Please select valid Health Declaration Date");
                        } 
//                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.validDate(lifeInsurence.healthDeclarationDate )))
//                        {
//                            alert("Other than date no other character are allowed for Health Declaration Date");
//                        }
                        
                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.healthDeclarationDate > Date.parse($scope.lifeInsurenceArr[index].bussinessDate) || lifeInsurence.healthDeclarationDate < Date.parse($scope.lifeInsurenceArr[index].dob) ))
                        {
                            alert("Health Declaration Date Should Be Between Business Date and DOB");
                        }

                       else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.premiumCollectedUpFront == null || lifeInsurence.premiumCollectedUpFront == "0"))
                        {
                            alert("Please Fill Premium Collected Up Front");

                        }
                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.coverNoteNumber == null || lifeInsurence.coverNoteNumber == ""))
                        {
                            alert("Please Fill Cover Note Number ");

                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.checkAlphaNumaricWithoutSpace(lifeInsurence.coverNoteNumber))) {
                            alert("Only Alphabets and Numbers are allowed for Cover Note Number");
                        }
                        else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.policyNumber == null || lifeInsurence.policyNumber == ""))
                        {
                            alert("Please Fill Policy Number");

                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.checkAlphaNumaricWithoutSpace(lifeInsurence.policyNumber))) {

                            alert("Only Alphabets and Numbers are allowed for Policy Number");

                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.barcodeNo == null || lifeInsurence.barcodeNo == ""))
                        {
                            alert("Please Fill Bar Code Number");

                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.checkAlphaNumaricWithoutSpace(lifeInsurence.barcodeNo))) {

                            alert("Only Alphabets and Numbers are allowed for Bar Code Number");

                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (lifeInsurence.nomineeAddress == null || lifeInsurence.nomineeAddress == ""))
                        {
                            alert("Please Fill Nominee Address");
                        } else if (lifeInsurence.postSanctionInsuranceCompany != "0" && (!$scope.checkAddress(lifeInsurence.nomineeAddress)))
                        {
                            alert("Special charecter are not allowed for Nominee Address");
                        }
                        else {
                            $scope.saveCheck(index);
                        }
                    }
                    $scope.submitPostSanctionInsuranceForm = function(index) {

                        var _date = $filter('date')($scope.lifeInsurenceArr[index].lifeInsurence.healthDeclarationDate, 'dd-MM-yyyy');

                        var callURL = $rootScope.urlDomain + "SavePostSanctionLifeInsuranceData.json";

                        var data = {
//                          lifeInsurenceArr: $scope.lifeInsurenceArr[index],

                            lifeInsurenceId: $scope.lifeInsurenceArr[index].lifeInsurence.insuranceId,
                            postSanctionSelectGender: $scope.lifeInsurenceArr[index].lifeInsurence.postSanctionSelectGender,
                            postSanctionSelectRelationWithNominee: $scope.lifeInsurenceArr[index].lifeInsurence.postSanctionSelectRelationWithNominee,
                            postSanctionInsuranceCompany: $scope.lifeInsurenceArr[index].lifeInsurence.postSanctionInsuranceCompany,
                            nomineeName: $scope.lifeInsurenceArr[index].lifeInsurence.nomineeName,
                            premiumCollectedUpFront: $scope.lifeInsurenceArr[index].lifeInsurence.premiumCollectedUpFront,
                            healthDeclarationDate: _date,
                            insurancePremiumCalculation: $scope.lifeInsurenceArr[index].lifeInsurence.insurancePremiumCalculation,
                            coverNoteNumber: $scope.lifeInsurenceArr[index].lifeInsurence.coverNoteNumber,
                            policyNumber: $scope.lifeInsurenceArr[index].lifeInsurence.policyNumber,
                            barcodeNo: $scope.lifeInsurenceArr[index].lifeInsurence.barcodeNo,
                            nomineeAddress: $scope.lifeInsurenceArr[index].lifeInsurence.nomineeAddress,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                            applicantId: $scope.lifeInsurenceArr[index].applicantId
                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                $cookies.a = data.applicationId;
                                $rootScope.applicationId = data.applicationId;
// $rootScope.progress = 1;
// $rootScope.openForm(4);
                                $location.path('/postsanction/assetinsurance');
// alert(data.msg);
                            }else{
                                alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }




                }]).controller("postSanctionReferenceDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter","$modal","$log", function($cookies, $rootScope, $scope, $http, $location, $filter,$modal,$log) {
                    // alert("test");

                    //anuya 18_12_15
                    
                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionReferenceDetailsForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }
                    
                    $scope.referenceArr = [];
                    $scope.deleteReferenceArr = [];

                    $scope.addReference = function() {
                        $scope.referenceArr.push({referenceId: "", referenceName: "", postSanctionAddressLine1: "", postSanctionAddressLine2: "", postSanctionAddressLine3: "", postSanctionAddressLine4: "", postSanctionReferenceCity: "0", postSanctionZipCode: "0", selectstate: "0", postSanctionStdCode: "", postSanctionPhoneResidence: "",
                            postSanctionMobile: "", postSanctionReferenceRelation: "0", postSanctionPhoneOffice: "", postSanctionExt: ""});
                    }


                    $scope.deleteReference = function(index) {
                        $scope.deleteReferenceArr.push({referenceId: $scope.referenceArr[index].referenceId});
                        $scope.referenceArr.splice(index, 1);

                    }
                    $rootScope.preloadPostSanctionReferenceDetails = function() {

                        $scope.relationArr = [{value: "0", description: 'Select Reference Relation'}];
                        $scope.stateArr = [{stateId: "0", stateDesc: 'Select State'}];

                        $scope.cityDefObj = {cityId: "0", cityName: 'Select Reference City'};
                        $scope.pinCodeDefObj = {zipcode: "0", zipdesc: 'Select ZIP Code'};

//                        $scope.cityArr = [{cityId: "0", cityName: 'Select Reference City'}];
//                        $scope.zipArr = [{zipcode: "0", zipdesc: 'Select ZIP Code'}];

                        $scope.cityList = "";
                        $scope.zipcodeList = "";

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchReferenceData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {
                                angular.forEach(response.referenceDetailsVO.lstreferenceRelation, function(relation, index) {
                                    var tempObject = {value: relation.value, description: relation.description};
                                    $scope.relationArr.push(tempObject);
                                });
                                $scope.postSanctionReferenceRelation = $scope.relationArr[0].value;

//                                angular.forEach(response.referenceDetailsVO.lstcity, function(city, index) {
//                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
//                                    $scope.cityArr.push(tempObject);
//                                });
//                                $scope.postSanctionReferenceCity = $scope.cityArr[0].cityId;

                                angular.forEach(response.referenceDetailsVO.lststate, function(state, index) {
                                    var tempObject = {stateId: state.stateId, stateDesc: state.stateDesc};
                                    $scope.stateArr.push(tempObject);
                                });
//                                $scope.selectstate = $scope.stateArr[0].stateId;

//                                angular.forEach(response.referenceDetailsVO.lstZip, function(zip, index) {
//                                    var tempObject = {zipcode: zip.zipcode, zipdesc: zip.zipdesc};
//                                    $scope.zipArr.push(tempObject);
//                                });
//                                $scope.postSanctionZipCode = $scope.zipArr[0].zipcode;


                                $scope.cityList = response.referenceDetailsVO.lstcity;
                                $scope.zipcodeList = response.referenceDetailsVO.lstPinCode;

                                if (response.data != null && response.data.length > 0) {
                                    $scope.referenceArr = [];

                                    angular.forEach(response.data, function(referenceDetails, index) {

                                        var referenceData = {
                                            referenceId: referenceDetails.referenceId,
                                            referenceName: referenceDetails.referenceName,
                                            postSanctionAddressLine1: referenceDetails.addressLine1,
                                            postSanctionAddressLine2: referenceDetails.addressLine2,
                                            postSanctionAddressLine3: referenceDetails.addressLine3,
                                            postSanctionAddressLine4: referenceDetails.addressLine4,
                                            postSanctionZipCode: referenceDetails.zipCode,
                                            selectstate: referenceDetails.state,
                                            postSanctionStdCode: referenceDetails.stdCode,
                                            postSanctionMobile: referenceDetails.mobile,
                                            postSanctionPhoneOffice: referenceDetails.officePhone,
                                            postSanctionExt: referenceDetails.ext,
                                            postSanctionPhoneResidence: referenceDetails.phoneResidence,
                                            postSanctionReferenceRelation: referenceDetails.referenceRelation,
                                            postSanctionReferenceCity: referenceDetails.city
                                        }
                                        $scope.setCity(referenceData);
                                        $scope.setPinCode(referenceData);
                                        $scope.referenceArr.push(referenceData);

                                    });
                                } else {
                                    $scope.addReference();
                                }

                            } else {

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };


                    $scope.setCity = function(addObj) {
                        addObj.cityArr = [];

                        addObj.cityArr.push($scope.cityDefObj);

                        if (addObj.selectstate != $scope.stateArr[0].stateId) {
                            angular.forEach($scope.cityList, function(city, index) {
                                if (city.stateId == addObj.selectstate) {
                                    var tempObject = {cityId: city.cityId, cityName: city.cityName};
                                    addObj.cityArr.push(tempObject);
                                }
                            });
                        }
                    }

                    $scope.setPinCode = function(addObj) {
                        addObj.pinCodeArr = [];
                        addObj.pinCodeArr.push($scope.pinCodeDefObj);

                        if (addObj.selectcity != addObj.cityArr[0].cityId) {
                            angular.forEach($scope.zipcodeList, function(pinCode, index) {
                                if (pinCode.cityId == addObj.postSanctionReferenceCity) {
                                    var tempObject = {zipcode: pinCode.zipcode, zipdesc: pinCode.pinCode + " - " + pinCode.zipdesc};
                                    addObj.pinCodeArr.push(tempObject);
                                }
                            });
                        }
                    }

                    //Avinash 17-12-2015//

                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }

                    $scope.checkAddress = function(validAddress) {
                        var address = /^[a-zA-Z\s\d\/`~!@#$%^&*()-_+=.,?;:'"{}]+$/;
                        return address.test(validAddress);
                    }
                    $scope.validName = function(nomineeName) {
                        var nomName = /^[a-zA-Z'.,\s]{1,150}$/;
                        return nomName.test(nomineeName);
                    }
                    
                    $scope.validatePostSanctionReferenceDetailsForm = function(index) {
                        var tempFlag = true;

                        angular.forEach($scope.referenceArr, function(reference, index) {
                            if (reference.referenceName == null || reference.referenceName == "")
                            {
                                alert("Please Fill Reference Name");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (!$scope.validName(reference.referenceName) )
                            {
                                alert("Special character and Number are not allowed for Reference Name");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }
                            else if (reference.postSanctionAddressLine1 == null || reference.postSanctionAddressLine1 == "")
                            {
                                alert(" Please Fill Address line 1 ");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkAddress(reference.postSanctionAddressLine1))
                            {
                                alert("Address line 1 should not contain special charecter");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionAddressLine2 != null && reference.postSanctionAddressLine2 != "" && !$scope.checkAddress(reference.postSanctionAddressLine2))
                            {
                                alert("Address line 2 should not contain special charecter");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionAddressLine3 != null && reference.postSanctionAddressLine3 != "" && !$scope.checkAddress(reference.postSanctionAddressLine3))
                            {
                                alert("Address line 3 should not contain special charecter");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionAddressLine4 != null && reference.postSanctionAddressLine4 != "" && !$scope.checkAddress(reference.postSanctionAddressLine4))
                            {
                                alert("Address line 4 should not contain special charecter");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.selectstate == null || reference.selectstate == "0" || reference.selectstate == "")
                            {
                                alert("Please select Reference State");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }
                            else if (reference.postSanctionReferenceCity == null || reference.postSanctionReferenceCity == "0" || reference.postSanctionReferenceCity == "")
                            {
                                alert("Please select Reference City");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionZipCode == null || reference.postSanctionZipCode == "" || reference.postSanctionZipCode == "0")
                            {
                                alert("Please select Zip Code");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }
                            else if (reference.postSanctionStdCode == null || reference.postSanctionStdCode == "")
                            {
                                alert("Please Fill STD Code");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (!$scope.checkDecimal(reference.postSanctionStdCode))
                            {
                                alert("Alphabets, special charecter and Decimal value are not allowed for STD Code");
                                $(".error").show();
                                tempFlag = false;
                                return false;

                                //anuya 18_12_15
                            } else if ((reference.postSanctionPhoneResidence == null || reference.postSanctionPhoneResidence == "") && (reference.postSanctionMobile == null || reference.postSanctionMobile == ""))
                            {
                                alert("Please Fill Phone Residence or Mobile Number");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }
                            else if (reference.postSanctionPhoneResidence != null && reference.postSanctionPhoneResidence != "" && !$scope.checkDecimal(reference.postSanctionPhoneResidence))
                            {
                                alert("Alphabets, special charecter and Decimal value are not allowed for Phone Residence");
                                $(".error").show();
                                tempFlag = false;
                                return false;
//                            } else if (reference.postSanctionMobile == null || reference.postSanctionMobile == "")
//                            {
//                                alert("Please Fill Mobile");
//                                $(".error").show();
//                                tempFlag = false;
//                                return false;
                            }
                            
                             
                            
                            else if(reference.postSanctionPhoneResidence != null && reference.postSanctionPhoneResidence != "" 
                                    && (reference.postSanctionStdCode.length + reference.postSanctionPhoneResidence.length)>11) {
                                
                                                               
                                alert("STD code + Phone Residence field should be exactly 11 digit only");
                                 $(".error").show();
                                tempFlag = false;
                                return false;
                                
                            }
                            
                            else if (reference.postSanctionMobile != null && reference.postSanctionMobile != "" && !$scope.checkDecimal(reference.postSanctionMobile))
                            {
                                alert("Alphabets, special charecter and Decimal value are not allowed for Mobile");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionMobile != null && reference.postSanctionMobile != "" && reference.postSanctionMobile.length < 10)
                            {
                                alert(" Mobile number should contain minimum 10 digit ");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionReferenceRelation == null || reference.postSanctionReferenceRelation == "0")
                            {
                                alert("Please Select Reference Relation");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            } else if (reference.postSanctionPhoneOffice != null && reference.postSanctionPhoneOffice != "" && !$scope.checkDecimal(reference.postSanctionPhoneOffice))
                            {
                                alert("Alphabets, special charecter and Decimal value are not allowed for Phone Office");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }
                            else if (reference.postSanctionExt != null && reference.postSanctionExt != "" && !$scope.checkDecimal(reference.postSanctionExt))
                            {
                                alert("Alphabets, special charecter and Decimal value are not allowed for Ext");
                                $(".error").show();
                                tempFlag = false;
                                return false;
                            }

                            //Avinash 17-12-2015//

                            else {
                                $(".error").hide();

                            }
                        });
                        if (tempFlag) {
                            
                            $scope.saveCheck();
                        }
                    }
                    $scope.submitPostSanctionReferenceDetailsForm = function(index) {

                        
                            var callURL = $rootScope.urlDomain + "SaveReferenceData.json";
                            var data = {
                                referenceArr: $scope.referenceArr,
                                deleteReferenceArr: $scope.deleteReferenceArr,
                                userId: $rootScope.userId,
                                applicationId: $rootScope.applicationId,
                            };

                            $http({
                                method: "POST",
                                url: callURL,
                                data: data
                            }).success(function(data) {
                                if (data.status == "200") {
                                    $cookies.a = data.applicationId;
                                    $rootScope.applicationId = data.applicationId;

                                    $location.path('/postsanction/instrumentdetail');
// alert(data.msg);
                                }
                                console.log(data);
                            }).error(function(error) {
                                console.log(error);
                            });
                        


                    }


                }])
                    .controller("postSanctionInstrumentDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter","$modal","$log", function($cookies, $rootScope, $scope, $http, $location, $filter,$modal,$log) {


                            //anuya 19_12_15
                    
                    $scope.saveCheck = function(index) {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitInstrumentsDetailsForm(index);
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }



                            $scope.$watch('postSanctionPdcBy', function() {
                                $scope.pdcNameArr = [{value: "0", description: 'Select PDC Name'}];

                                if ($scope.postSanctionPdcBy != $scope.pdcByArr[0].value) {

                                    angular.forEach($scope.lstPDCName, function(PDCName, index) {
                                        if (PDCName.pdcType == $scope.postSanctionPdcBy) {
                                            var tempObject = {value: PDCName.pdcName, description: PDCName.pdcName};
                                            $scope.pdcNameArr.push(tempObject);
                                        }
                                    });
                                }
                            });


                            $rootScope.preloadPostSanctionInstrumentsDetails = function() {

                                $scope.pdcNameArr = [{value: "0", description: 'Select PDC Name'}];
                                $scope.chequeTypeArr = [{value: "0", description: 'Select Cheque Type'}];
                                $scope.instrumentTypeArr = [{value: "0", description: 'Select Instrument Type'}];
                                $scope.customerAccountArr = [{value: "0", description: 'Select Customer Account Number'}];
                                $scope.pdcByArr = [{value: "0", description: 'Select PDC By'}];
                                $scope.pdcTypeArr = [{value: "0", description: 'Select PDC Type'}];
                                $scope.destAccTypeArr = [{value: "0", description: 'Select Dest Bank Account Type'}];
                                $scope.downloadPDCMSArr = [{value: "0", description: 'Select PDCMS'}, {value: 'Y', description: "Yes"}, {value: 'N', description: "No"}];

                                $scope.downloadToPDCMS = 'N';
                                $scope.defaultChequeType = 'M';                                
                                $scope.customerAccountList = "";

                                var data = {
                                    userId: $rootScope.userId,
                                    applicationId: $rootScope.applicationId
                                }
                                var callURL = $rootScope.urlDomain + "FetchInstrumentDetailsData.json";
                                $http({
                                    method: "POST",
                                    url: callURL,
                                    data: data
                                }).success(function(response) {
                                    console.log(response);
                                    if (response.status == "200") {
                                        angular.forEach(response.instrumentDetailsVO.lstchequeType, function(cheque, index) {
                                            var tempObject = {value: cheque.value, description: cheque.description};
                                            $scope.chequeTypeArr.push(tempObject);
                                        });
                                        $scope.postSanctionChequeType = $scope.defaultChequeType;

                                        angular.forEach(response.instrumentDetailsVO.lstinstrumentType, function(instrumentType, index) {
                                            var tempObject = {value: instrumentType.value, description: instrumentType.description};
                                            $scope.instrumentTypeArr.push(tempObject);
                                        });
                                        $scope.postSanctionInstrumentType = $scope.instrumentTypeArr[0].value;

                                        angular.forEach(response.instrumentDetailsVO.lstpdcBy, function(pdcBy, index) {
                                            var tempObject = {value: pdcBy.value, description: pdcBy.description};
                                            $scope.pdcByArr.push(tempObject);
                                        });
                                        $scope.postSanctionPdcBy = $scope.pdcByArr[0].value;

                                        angular.forEach(response.instrumentDetailsVO.lstpdcType, function(pdcType, index) {
                                            var tempObject = {value: pdcType.value, description: pdcType.description};
                                            $scope.pdcTypeArr.push(tempObject);
                                        });
                                        $scope.postSanctionPdcType = $scope.pdcTypeArr[0].value;
                                        
                                        angular.forEach(response.instrumentDetailsVO.lstdstBankAccountType, function(accType, index) {
                                            var tempObject = {value: accType.value, description: accType.description};
                                            $scope.destAccTypeArr.push(tempObject);
                                        });
                                        $scope.destBankAccountType = $scope.destAccTypeArr[0].value;

                                        $scope.lstPDCName = response.instrumentDetailsVO.lstPDCByName;
                                        $scope.pdcByName = $scope.pdcNameArr[0].value;

                                        $scope.customerAccountList = response.instrumentDetailsVO.lstCustomerAccountNumber;

                                        if (response.data != null) {
                                            var instrumentData = response.data;
                                            $scope.postSanctionInstrumentType = instrumentData.instrumentType;
                                            
                                            
                                          angular.forEach(response.instrumentDetailsVO.lstmICR, function(item, index) {
                                        	  

                                          
                                          if (item.micrCode == instrumentData.micr) {
                                              $scope.micr = item;
                                          }
                                      });
                                            
                                            
                                            
                                            $scope.postSanctionPdcBy = instrumentData.pdcBy;
                                            $scope.destBankAccountType = instrumentData.destBankAccType;
                                            $scope.postSanctionPdcType = instrumentData.pdcType;
                                            $scope.downloadToPDCMS = instrumentData.downloadToPdcms;
                                            $scope.pdcByName = instrumentData.pdcByName;
                                            $scope.customerAccountNo = instrumentData.custAccNo;
                                            $scope.destAccountHolder = instrumentData.destAccHolder;
                                            $scope.postSanctionChequeType = instrumentData.chequeType;

                                        }


                                    }

                                }).error(function(error) {
                                    console.log(error);
                                });
                            };



                            $scope.$watch('pdcByName', function() {
                               
                                if ($scope.customerAccountArr.length > 1) {
                                    $scope.customerAccountArr.splice(1, $scope.customerAccountArr.length);//                                    
                                }                               
                               
                                
                                if ($scope.customerAccountNo == null) {
                                	$scope.customerAccountNo = $scope.customerAccountArr[0].value;
                                }
                                
                                angular.forEach($scope.customerAccountList, function(account, index) {
                                     
                                    if (account.value == $scope.pdcByName) {
                                    
                                        var tempObject = {value: account.customerAccountNumber, description: account.customerAccountNumber};
                                        $scope.customerAccountArr.push(tempObject);
                                        
                                        if($scope.customerAccountNoData==account.customerAccountNumber) { 
                                           
                                            $scope.customerAccountNo=tempObject;
                                            $scope.customerAccountNoData = "";
                                        }
                                    }
                                   

                                });



                            }, true);
                            
                            
                            $scope.micrChange = function() {
                          	  $scope.micr.cityId = "0";
                          	  $scope.micr.cityDescription = "Select City";
                          	  $scope.micr.bankId = "0";
                          	  $scope.micr.bankDescription = "Select Bank";
                          	  $scope.micr.bankBranchId = "0";
                          	  $scope.micr.bankBranchDescription = "Select Branch";
                            }
                            
                            $scope.validateInstrumentsDetailsForm = function() {
                                
                                if ($scope.postSanctionInstrumentType == null || $scope.postSanctionInstrumentType == "0")
                                {
                                    alert("Please Select Instrument Type");
                                    
                                } else if ($scope.micr.micrCode == null || $scope.micr.micrCode == "")
                                {
                                    alert("Please Enter MICR");
                                    
                                } else if ($scope.micr.cityId == null || $scope.micr.cityId == "")
                                {
                                    alert("Please Search For MICR and Get City Description");
                                    
                                } else if ($scope.micr.bankId == null || $scope.micr.bankId == "")
                                {
                                	alert("Please Search For MICR and Get Bank Description");
                                    
                                } else if ($scope.micr.bankBranchId == null || $scope.micr.bankBranchId == "")
                                {
                                	alert("Please Search For MICR and Get Branch Description");
                                    
                                } else if ( $scope.postSanctionPdcBy == null || $scope.postSanctionPdcBy == "0")
                                {
                                    alert("Please Select PDC By");
                                } else if ($scope.pdcByName == null || $scope.pdcByName == "0")
                                {
                                    alert("Please Fill PDC By Name");
                                } 
                                 else if ( $scope.downloadToPDCMS == null || $scope.downloadToPDCMS == "0")
                                {
                                    alert("Please select Download To PDCMS");
                                } 
                                else if ( $scope.destBankAccountType == null || $scope.destBankAccountType == "0")
                                {
                                    alert("Please Select Dest Bank Account  Type");
                                }
                                 else if ($scope.destAccountHolder == null || $scope.destAccountHolder == "0")
                                {
                                    alert("Please Fill Dest. A/C Holder");
                                } else if ($scope.customerAccountNo == null || $scope.customerAccountNo == "0")
                                {
                                    alert("Please Select Customer Account Number");
                                } else if ($scope.postSanctionPdcType == null || $scope.postSanctionPdcType == "0")
                                {
                                    alert("Please Select PDC Type");
                                } 
                                else if ($scope.postSanctionChequeType == null || $scope.postSanctionChequeType == "0")
                                {
                                    alert("Please Select Cheque Type");
                                }
                                else {
                                    $scope.saveCheck();
                                }
                            }
                            
                            $scope.searchMicr = function() {
                            	
                            	if($scope.micr.micrCode.length < 9) {
                            		alert("Please Enter 9 Digit Micr Code");
                            		return false;                            		
                            	}
                            	
                            	var callURL = $rootScope.urlDomain + "SearchMicr.json";
                            	 var data = {                                        
                                         micrCode: $scope.micr.micrCode
                                     };
                            	 
                            	 $http({
                                     method: "POST",
                                     url: callURL,
                                     data: data
                                 }).success(function(response) {
                                     if (response.status == "200") {                                    	 
                                    	 angular.forEach(response.mICRVO, function(micrTemp, index) {                                    		 
                                    		 $scope.micr = micrTemp;
                                         });
                                     }
                                     console.log(data);
                                 }).error(function(error) {
                                     console.log(error);
                                 });
                            }

                            $scope.submitInstrumentsDetailsForm = function() {                                

                                    var callURL = $rootScope.urlDomain + "SavePostSanctionInstrumentData.json";
                                    var data = {
                                        postSanctionInstrumentType: $scope.postSanctionInstrumentType,
                                        micrCode: $scope.micr.micrCode, 
                                        cityId: $scope.micr.cityId,
                                        bankId: $scope.micr.bankId,
                                        bankBranchId: $scope.micr.bankBranchId,
                                        downloadToPDCMS: $scope.downloadToPDCMS,
                                        postSanctionPdcBy: $scope.postSanctionPdcBy,
                                        pdcByName: $scope.pdcByName,
                                        destBankAccountType: $scope.destBankAccountType,
                                        destAccountHolder: $scope.destAccountHolder,
                                        postSanctionPdcType: $scope.postSanctionPdcType,
                                        customerAccountNo: $scope.customerAccountNo,
//                                        employeeCode: $scope.employeeCode,


                                        postSanctionChequeType: $scope.postSanctionChequeType,
                                        userId: $rootScope.userId,
                                        applicationId: $rootScope.applicationId,
                                    };

                                    $http({
                                        method: "POST",
                                        url: callURL,
                                        data: data
                                    }).success(function(data) {
                                        if (data.status == "200") {
                                            $cookies.a = data.applicationId;
                                            $rootScope.applicationId = data.applicationId;
                                            $location.path('/postsanctiondashboard');
                                        }
                                        console.log(data);
                                    }).error(function(error) {
                                        console.log(error);
                                    });
                                

                            }


                        }]).controller("postSanctionAssetDetailsCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {

                    //anuya 17_12_15

                    $scope.saveCheck = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitPostSanctionAssetDetailsForm();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

                    $scope.today = function() {
                        return $scope.dt = new Date
                    };
                    $scope.showWeeks = !0;
                    $scope.toggleWeeks = function() {
                        return $scope.showWeeks = !$scope.showWeeks
                    };
                    $scope.clear = function() {

                        return $scope.dt = null
                    };
                    $scope.today();
                    $scope.disabled = function(date, mode) {
                        // return "day" === mode && (0 === date.getDay() || 6
                        // === date.getDay())
                        return "day" === mode && ($scope.dt < date)
                    };
                    $scope.toggleMin = function() {
                        var _ref;
                        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                            "null": new Date
                        }
                    };
                    $scope.toggleMin();
                    $scope.open = function($event) {
                        return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
                    };
                    $scope.dateOptions = {
                        "year-format": "'yyyy'",
                        "starting-day": 1,
                        "show-weeks": false,
                        "datepicker-mode": 'year',
                        "year-range": 50,
                        "format-day-title": 'MMMM',
                        "format-month-title": 'yyyy',
                    };

                    $scope.invoiceDate = null;
                    $scope.formats = ["dd-MM-yyyy", "yyyy/MM/dd", "shortDate"];
                    $scope.format = $scope.formats[0];

                    $scope.nullValidation = function(val) {
                        if (angular.isUndefined(val) || val === null || val === "null") {
                            return "";
                        } else {
                            return val;
                        }
                    }

                    $rootScope.preloadPostSanctionAssetDetails = function() {

                        $scope.manufacturerArr = [{manufacturerId: "0", manufacturerDesc: 'Select Manufacturer '}];
                        $scope.assetMakeArr = [{makeId: "0", make: 'Select Asset Make '}];
                        $scope.assetModelArr = [{modelId: "0", modelNo: 'Select Asset Model'}];
                        $scope.supplierArr = [{supplierId: "0", supplierdesc: 'Select Supplier'}];


                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchPostSanctionAssetData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {

//                                angular.forEach(response.postSanctionAssetVO.lstManufacturer, function(manufacturer, index) {
//                                    var tempObject = {manufacturerId: manufacturer.manufacturerId, manufacturerDesc: manufacturer.manufacturerDesc};
//                                    $scope.manufacturerArr.push(tempObject);
//                                });
//                                $scope.postSanctionManufacturer = $scope.manufacturerArr[0].manufacturerId;
//
//                                angular.forEach(response.postSanctionAssetVO.lstAssetMake, function(assetMake, index) {
//                                    var tempObject = {makeId: assetMake.modelId, make: assetMake.make};
//                                    $scope.assetMakeArr.push(tempObject);
//                                });
//                                $scope.postSanctionAssetMake = $scope.assetMakeArr[0].modelId;
//
//                                angular.forEach(response.postSanctionAssetVO.lstAssetModel, function(assetModel, index) {
//                                    var tempObject = {modelId: assetModel.modelId, modelNo: assetModel.modelNo};
//                                    $scope.assetModelArr.push(tempObject);
//                                });
//                                $scope.postSanctionAssetModel = $scope.assetModelArr[0].modelId;
//
//                                angular.forEach(response.postSanctionAssetVO.lstsupplier, function(supplier, index) {
//                                    var tempObject = {supplierId: supplier.supplierId, supplierdesc: supplier.supplierdesc};
//                                    $scope.supplierArr.push(tempObject);
//                                });
//                                $scope.postSanctionSupplier = $scope.supplierArr[0].supplierId;
                            	
                                    $scope.manufacturerDesc= response.postSanctionAssetVO.manufacturerDesc;
                                    $scope.assetMakeDesc= response.postSanctionAssetVO.assetMakeDesc;
                                    $scope.assetModelDesc= response.postSanctionAssetVO.assetModelDesc;
                                    $scope.supplierDesc= response.postSanctionAssetVO.supplierDesc;


                                if (response.data != null) {
                                    var assetData = response.data;

                                    $scope.postSanctionManufacturer = assetData.manufacturer;
                                    $scope.postSanctionAssetMake = assetData.assetMake;
                                    $scope.postSanctionAssetModel = assetData.assetModel;
                                    $scope.postSanctionSupplier = assetData.supplier;
                                    $scope.chassisNumber = $scope.nullValidation(assetData.chassisNumber);
                                    $scope.engineNumber = $scope.nullValidation(assetData.engineNumber);
                                    $scope.invoiceNumber = $scope.nullValidation(assetData.invoiceNomber);
                                    $scope.invoiceValue = $scope.nullValidation(assetData.invoiceValue);
                                    $scope.invoiceDate = $scope.nullValidation(assetData.invoiceDate);
                                    $scope.assetCost = $scope.nullValidation(assetData.assetCost);

                                }

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.checkAlphaNumaricWithoutSpace = function(alphaNum) {
                        var check = /^[a-zA-Z\d]+$/;
                        return check.test(alphaNum);
                    }

                    $scope.checkDecimalValue = function(checkValue) {
                        var value = /^[0-9]{1,12}(\.[0-9]{1,2})?$/;
                        return value.test(checkValue);
                    }

                    $scope.validatePostSanctionAssetDetailsForm = function() {
                    	
                    	
                        /* if ($scope.postSanctionManufacturer == null || $scope.postSanctionManufacturer == "0")
                         {
                         alert("Please Select Manufacturer");
                         } else if (
                         $scope.postSanctionAssetMake == null || $scope.postSanctionAssetMake == "0")
                         {
                         alert("Please Select Asset Make");
                         } else if (
                         $scope.postSanctionAssetModel == null || $scope.postSanctionAssetModel == "0")
                         {
                         alert("Please Select Asset Model");
                         } else if (
                         $scope.postSanctionSupplier == null || $scope.postSanctionSupplier == "0")
                         {
                         alert("Please Select Supplier");
                         } else
                         */
                        if ($scope.chassisNumber != null && $scope.chassisNumber != "" && !$scope.checkAlphaNumaricWithoutSpace($scope.chassisNumber))
                        {
                            alert("Only Alphabets and Numbers are allowed for Chassis Number");

                        } else if ($scope.engineNumber != null && $scope.engineNumber != "" && !$scope.checkAlphaNumaricWithoutSpace($scope.engineNumber))
                        {
                            alert("Only Alphabets and Numbers are allowed for Engine Number");
                        } else if ($scope.invoiceNumber != null && $scope.invoiceNumber != "" && !$scope.checkAlphaNumaricWithoutSpace($scope.invoiceNumber))
                        {
                            alert("Only Alphabets and Numbers are allowed for Invoice Number");
                        } else if ($scope.invoiceValue != null && $scope.invoiceValue != "" && !$scope.checkDecimalValue($scope.invoiceValue))
                        {
                            alert("Only number are allowed for Invoice value\n!Only 2 digit allowed for Invoice Value after decimal");
                        }else if ($scope.invoiceValue != null && $scope.invoiceValue != "" && parseFloat($scope.assetCost)<parseFloat($scope.invoiceValue))
                        {
                            alert("Invoice Value is greater than Asset Cost");
                        }
//                        else if ($scope.invoiceDate != null && $scope.invoiceDate != "" && !$scope.checkAlphaNumaric($scope.invoiceDate))
//                        {
//                            alert("Please Fill Invoice Date");
//                        }
                        else {
                            $scope.saveCheck();
                        }

                    }
                    $scope.submitPostSanctionAssetDetailsForm = function() {

                        var _date = $filter('date')($scope.invoiceDate, 'dd-MM-yyyy');

                        var callURL = $rootScope.urlDomain + "SavePostSanctionAssetData.json";
                        var data = {
                            manufacturer: $scope.postSanctionManufacturer,
                            assetMake: $scope.postSanctionAssetMake,
                            assetModel: $scope.postSanctionAssetModel,
                            supplier: $scope.postSanctionSupplier,
                            chassisNumber: $scope.chassisNumber,
                            engineNumber: $scope.engineNumber,
                            invoiceNomber: $scope.invoiceNumber,
                            invoiceValue: $scope.invoiceValue,
                            invoiceDate: _date,
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId,
                        };

                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(data) {
                            if (data.status == "200") {
                                $cookies.a = data.applicationId;
                                $rootScope.applicationId = data.applicationId;
// $rootScope.progress = 1;
// $rootScope.openForm(1);
                                $location.path('/postsanction/charges');
// alert(data.msg);
                            }
                            console.log(data);
                        }).error(function(error) {
                            console.log(error);
                        });
                    }




                }]).controller("lpcController", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {


                    $scope.lpcReset = function() {
                        $scope.exshowroom = $scope.preExshowroom;
                        $scope.insurance = $scope.preInsurance;
                        $scope.registration = $scope.preRegistration;
                        $scope.othersloandetails = $scope.preOthersloandetails;
                        $scope.assetCost = $scope.preassetCost;
                        $scope.appliedloanamount = $scope.preAppliedloanamount;
                        $scope.appliedmarginmoney = $scope.preAppliedmarginmoney;
                        $scope.appliedtenure = $scope.preAppliedtenure;
                        $scope.advinstall = $scope.preAdvinstall;
                        $scope.ratepercent = $scope.preRatepercent;
                        $scope.loanemiloandetails = $scope.preLoanemiloandetails;
                        $scope.selectfrequency = $scope.preSelectfrequency;
                        $scope.appliedLtv = $scope.preAppliedltv;
                        $scope.insurancepremiumcollection = $scope.preInsurancepremiumcollection;

//                        calculateAssetCost();
//                        calculateAppliedMarginMoney();
//                        calculateEMI();
//                        calculateAppliedltv();



                    }

                    $scope.calculateAssetCost = function() {
                        if ($scope.ourParseInt($scope.exshowroom)) {
                            $scope.exshowroom = parseInt($scope.exshowroom);
                        } else {
                            $scope.exshowroom = 0;
                        }

                        if ($scope.ourParseInt($scope.insurance)) {
                            $scope.insurance = parseInt($scope.insurance);
                        } else {
                            $scope.insurance = 0;
                        }

                        if ($scope.ourParseInt($scope.registration)) {
                            $scope.registration = parseInt($scope.registration);
                        } else {
                            $scope.registration = 0;
                        }

                        if ($scope.ourParseInt($scope.othersloandetails)) {
                            $scope.othersloandetails = parseInt($scope.othersloandetails);
                        } else {
                            $scope.othersloandetails = 0;
                        }

                        $scope.assetcost = Math.round(($scope.exshowroom + $scope.insurance + $scope.registration + $scope.othersloandetails) * 100) / 100;
                        if (isNaN($scope.assetcost)) {
                            $scope.assetcost = 0;
                        }

                        return $scope.assetcost;
                    }


                    $scope.calculateAppliedMarginMoney = function() {
                        if ($scope.ourParseInt($scope.assetcost)) {
                            $scope.assetcost = parseInt($scope.assetcost);
                        } else {
                            $scope.assetcost = 0;
                        }

                        if ($scope.ourParseInt($scope.appliedloanamount)) {
                            $scope.appliedloanamount = parseInt($scope.appliedloanamount);
                        } else {
                            $scope.appliedloanamount = 0;
                        }



                        $scope.appliedmarginmoney = Math.round(($scope.assetcost - $scope.appliedloanamount) * 100) / 100;
                        if (isNaN($scope.appliedmarginmoney)) {
                            $scope.appliedmarginmoney = 0;
                        }

                        return $scope.appliedmarginmoney;
                    }


                    $scope.calculateEMI = function() {

//                        if ($scope.ourParseInt($scope.ratepercent)) {
//                            $scope.ratepercent = parseInt($scope.ratepercent);
//                        } else {
//                            $scope.ratepercent = 0;
//                        }

                        if ($scope.ourParseInt($scope.appliedtenure)) {
                            $scope.appliedtenure = parseInt($scope.appliedtenure);
                        } else {
                            $scope.appliedtenure = 0;
                        }

                        if ($scope.ourParseInt($scope.appliedloanamount)) {
                            $scope.appliedloanamount = parseInt($scope.appliedloanamount);
                        } else {
                            $scope.appliedloanamount = 0;
                        }


                        var r = $scope.ratepercent / 100 / 12;
                        var LoanPeriod = $scope.appliedtenure;
                        $scope.loanemiloandetails = Math.ceil(Math.round(($scope.appliedloanamount * (r * Math.pow((1 + r), LoanPeriod) / (Math.pow((1 + r), LoanPeriod) - 1))) * 100) / 100);
                        if (isNaN($scope.loanemiloandetails)) {
                            $scope.loanemiloandetails = 0;
                        }

                        return $scope.loanemiloandetails;
                    }

                    $scope.calculateAppliedltv = function() {

// if ($scope.ourParseInt($scope.appliedloanamount)) {
// $scope.appliedloanamount = parseInt($scope.appliedloanamount);
// } else {
// $scope.appliedloanamount = 0;
// }
//
// if ($scope.ourParseInt($scope.advinstall)) {
// $scope.advinstall = parseInt($scope.advinstall);
// } else {
// $scope.advinstall = 0;
// }
//
// if ($scope.ourParseInt($scope.assetcost)) {
// $scope.assetcost = parseInt($scope.assetcost);
// } else {
// $scope.assetcost = 0;
// }

                        $scope.appliedltv = Math.round((($scope.ourParseIntAn($scope.appliedloanamount) - $scope.ourParseIntAn($scope.advinstall) * $scope.ourParseIntAn($scope.loanemiloandetails)) / $scope.ourParseIntAn($scope.assetcost) * 100) * 100) / 100;


                        if (isNaN($scope.appliedltv)) {
                            $scope.appliedltv = 0;
                        }
                        return $scope.appliedltv;
                    }

                    $scope.ourParseIntAn = function(fieldValue) {
                        var intReg = /^[0-9]+$/;
                        if (intReg.test(fieldValue)) {
                            return parseInt(fieldValue);
                        } else {
                            return 0;
                        }

                    }

                    $scope.ourParseInt = function(fieldValue) {
                        var intReg = /^[0-9]+$/;
                        return intReg.test(fieldValue);
                    }
                    $scope.checkDecimal = function(decimalValue) {
                        var check = /^[0-9]*$/;
                        return check.test(decimalValue);
                    }

                    $scope.$watch('selectmanufacture', function() {
                        $scope.assetmakeArr = [{makeId: "0", make: 'Select Asset Make'}];
                        if ($scope.selectmanufacture != $scope.manufacturerArr[0].manufacturerId) {

                            angular.forEach($scope.lstAssetMake, function(assetmake, index) {
                                if (assetmake.manufacturerId == $scope.selectmanufacture) {
                                    var tempObject = {modelId: assetmake.modelId, make: assetmake.make};
                                    $scope.assetmakeArr.push(tempObject);
                                }
                            });
                        }
                    });
                    $scope.$watch('selectassetmake', function() {
                        $scope.assetmodelArr = [{modelId: "0", modelNo: 'Select Asset Model', catgId: '0'}];
                        if ($scope.selectassetmake != $scope.assetmakeArr[0].makeId) {
                            $scope.make = '';
                            angular.forEach($scope.lstAssetMake, function(assetmake, index) {
                                if (assetmake.modelId == $scope.selectassetmake) {
                                    $scope.make = assetmake.make;
                                }
                            });
                            angular.forEach($scope.lstAssetModel, function(assetmodel, index) {
                                if (assetmodel.manufacturerId == $scope.selectmanufacture
                                        && assetmodel.makeId == $scope.make) {

                                    var tempObject = {modelId: assetmodel.modelId, modelNo: assetmodel.modelNo, catgId: assetmodel.assetCatg};
                                    $scope.assetmodelArr.push(tempObject);
                                }
                            });
                            angular.forEach($scope.assetmodelArr, function(model, index) {
                                if (model.modelId == $scope.tempModelId) {
                                    $scope.selectassetmodel = model;
                                }
                                console.log($scope.selectassetmodel);
                            });
                        }
                    });

// $scope.$watch('selectassetmake', function() {
// $scope.selectassetcategory=$scope.selectassetmodel.catgId;
// });

                    $scope.cancelLpc = function cancelLpc() {
                        var callURL = $rootScope.urlDomain + "cancelLpc.json";
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            if (response.status == "200") {
                                $location.path('/postsanctiondashboard');
                            } else {
                                alert(response.msg);
                            }
// console.log(response);
                        }).error(function(error) {
                            console.log(error);
                        });
                    };
                    $scope.submitLpc = function submitLpc() {
                        
                        var callURL = $rootScope.urlDomain + "editPresanction.json";
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            if (response.status == "200") {
                                $location.path('/lpcdashboard');
                            } else {
                                alert(response.msg);
                            }
// console.log(response);
                        }).error(function(error) {
                            console.log(error);
                        });
                    };

                    $scope.preloadLpc = function() {

                        $scope.FrequencyArr = [{frequencyId: "0", frequencyDesc: "Select Frequency"},
                            {frequencyId: "MONTHLY", frequencyDesc: "MONTHLY"}];
                        $scope.selectfrequency = $scope.FrequencyArr[0].frequencyId;
                        $scope.selectfrequency = "MONTHLY";
                        $scope.insurancepremiumcollectionArr = [{id: "0", desc: "Select Option"},
                            {id: "Upfront", desc: "Upfront"},
                            {id: "Funded", desc: "Funded"},
                            {id: "Not Applicable", desc: "Not Applicable"}];
                        $scope.insurancepremiumcollection = $scope.insurancepremiumcollectionArr[0].id;

                        $scope.vehicleDeliveredArr = [{value: "0", desc: 'Select Vehicle Delivered'}, {value: "Y", desc: 'Yes'}, {value: "N", desc: 'No'}];

                        $scope.manufacturerArr = [{manufacturerId: "0", manufacturerDesc: 'Select Manufacturer'}];
                        $scope.assetmakeArr = [{makeId: "0", make: 'Select Asset Make'}];
                        $scope.assetmodelArr = [{modelId: "0", modelNo: 'Select Asset Model', catgId: '0'}];
                        $scope.assetcategoryArr = [{catgId: "0", catgDesc: 'Select Category'}];
                        $scope.selectmanufacture = "";
                        $scope.selectassetmake = "";
                        $scope.selectassetmodel = "";
//                         $scope.isVehicleDelivered=$scope.vehicleDeliveredArr[0].value;
// $scope.selectassetcategory = "";
// }

                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        };
                        var callURL = $rootScope.urlDomain + "loadLpc.json";
// var callURL = $rootScope.urlDomain+"FetchAssetData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            $scope.loanVO = response.loanVO;
                            angular.forEach(response.assetDetailVO.lstManufacturer, function(manufacturer, index) {
                                var tempObject = {manufacturerId: manufacturer.manufacturerId, manufacturerDesc: manufacturer.manufacturerDesc};
                                $scope.manufacturerArr.push(tempObject);
                            });
                            if ($scope.manufacturerArr.length == 2) {
                                $scope.selectmanufacture = $scope.manufacturerArr[1].manufacturerId;
                            } else {
                                $scope.selectmanufacture = $scope.manufacturerArr[0].manufacturerId;
                            }

                            $scope.lstAssetMake = response.assetDetailVO.lstAssetMake;
                            $scope.lstAssetModel = response.assetDetailVO.lstAssetModel;

// angular.forEach(response.assetDetailVO.lstAssetMake, function(assetmake,
// index) {
// var tempObject = {modelId: assetmake.modelId, make: assetmake.make};
// $scope.assetmakeArr.push(tempObject);
// });
//
// $scope.selectassetmake = $scope.assetmakeArr[0].modelId
//
// angular.forEach(response.assetDetailVO.lstAssetModel, function(assetmodel,
// index) {
// var tempObject = {modelId: assetmodel.modelId, modelNo: assetmodel.modelNo};
// $scope.assetmodelArr.push(tempObject);
// });
//
// $scope.selectassetmodel = $scope.assetmodelArr[0].modelId

// angular.forEach(response.assetDetailVO.lstAssetCategory, function(category,
// index) {
// var tempObject = {catgId: category.catgId, catgDesc: category.catgDesc};
// $scope.assetcategoryArr.push(tempObject);
// });
// $scope.selectassetcategory = $scope.assetcategoryArr[0].catgDesc
//                           
                            if (response.dataAsset != null) {
                                var assetData = response.dataAsset;
                                $scope.selectmanufacture = assetData.manufacturer;
                                $scope.selectassetmake = assetData.assetMake;
// $scope.selectassetmodel= $scope.assetmodelArr[0];
                                $scope.tempModelId = assetData.assetModel;

// angular.forEach($scope.assetmodelArr, function(model, index) {
// if(model.modelId==assetData.assetModel){
// $scope.selectassetmodel=model;
// }
// console.log($scope.selectassetmodel);
// });

// $scope.selectassetcategory = assetData.assetCategory;
                            }
                            if (response.dataPreLoan != null) {
                                var dataPreLoan = response.dataPreLoan;
                                $scope.preExshowroom = dataPreLoan.exShowroom;
                                $scope.preInsurance = dataPreLoan.insurance;
                                $scope.preRegistration = dataPreLoan.registration;
                                $scope.preOthersloandetails = dataPreLoan.others;
                                $scope.preAssetcost = dataPreLoan.assetCost;
                                $scope.preAppliedloanamount = dataPreLoan.appliedLoanAmount;
                                $scope.preAppliedmarginmoney = dataPreLoan.appliedMarginMoney;
                                $scope.preAppliedtenure = dataPreLoan.appliedTenureMonths;
                                $scope.preAdvinstall = dataPreLoan.advInstl.toString();
                                $scope.preRatepercent = dataPreLoan.rate;
                                $scope.preLoanemiloandetails = dataPreLoan.loanEmi;
                                $scope.preSelectfrequency = dataPreLoan.frequency;
                                $scope.preAppliedltv = dataPreLoan.appliedLtv,
                                        $scope.preInsurancepremiumcollection = dataPreLoan.insurancePremiumCollected;
// $scope.preCost = dataPreLoan.assetCost;
// $scope.preMarginMoney = dataPreLoan.appliedMarginMoney;
// $scope.preLoanAmt = dataPreLoan.appliedLoanAmount;
// $scope.preTenure = dataPreLoan.appliedTenureMonths;
// $scope.preAvdInstallment = dataPreLoan.advInstl;
// $scope.preLtv = dataPreLoan.appliedLtv;


                            }
                            if (response.dataLoan != null) {
                                var dataLoan = response.dataLoan;
// $scope.cost = dataLoan.assetCost;
// $scope.marginMoney = dataLoan.appliedMarginMoney;
// $scope.loanAmt = dataLoan.appliedLoanAmount;
// $scope.tenure = dataLoan.appliedTenureMonths;
// $scope.avdInstallment = dataLoan.advInstl;
// $scope.ltv = dataLoan.appliedLtv;

                                $scope.isVehicleDelivered = dataLoan.isVehicleDelivered;
                                if ($scope.isVehicleDelivered == "Y") {
                                    $scope.isVehicleDeliveredDesc = "Yes";
                                } else {
                                    $scope.isVehicleDeliveredDesc = "No";
                                }
                                $scope.exshowroom = dataLoan.exShowroom;
                                $scope.insurance = dataLoan.insurance;
                                $scope.registration = dataLoan.registration;
                                $scope.othersloandetails = dataLoan.others;
                                $scope.assetcost = dataLoan.assetCost;
                                $scope.appliedloanamount = dataLoan.appliedLoanAmount;
                                $scope.appliedmarginmoney = dataLoan.appliedMarginMoney;
                                $scope.appliedtenure = dataLoan.appliedTenureMonths;
                                $scope.advinstall = dataLoan.advInstl.toString();
                                $scope.ratepercent = dataLoan.rate;
                                $scope.loanemiloandetails = dataLoan.loanEmi;
                                $scope.selectfrequency = dataLoan.frequency;
                                $scope.appliedltv = dataLoan.appliedLtv;
                                $scope.insurancepremiumcollection = dataLoan.insurancePremiumCollected;


                            }


                        }).error(function(error) {
                            console.log(error);
                        });
                    };
                    
                    
                    $scope.checkDecimalValue = function(checkValue) {
                        var value = /^[0-9]{1,}(\.[0-9]{1,2})?$/;
                        return value.test(checkValue);
                    }

                    $scope.validateLpc=function(){
                        if ($scope.isVehicleDelivered == null || $scope.isVehicleDelivered == "0") {
                            alert("Please select Vehicle Delivered");
                            return false;
                        } else
                        if ($scope.selectmanufacture == null || $scope.selectmanufacture == "0") {
                            alert("Please Select Manufacturer ");
                            return false;
                        } else if ($scope.selectassetmake == null || $scope.selectassetmake == "0") {
                            alert("Please Select Asset Make");
                            return false;
                        } else if ($scope.selectassetmodel.modelId == null || $scope.selectassetmodel.modelId == "0") {
                            alert("Please Select Asset Model");
                            return false;
                        // } else if ($scope.selectassetcategory == null || $scope.selectassetcategory
                        // == "0") {
                        // alert("please Select Asset Category");

                            // ////
                        } else if ($scope.exshowroom == null || $scope.exshowroom == "") {
                            alert("Please Enter Exshowroom ");
                            return false;
                        } else if (!$scope.checkDecimal($scope.exshowroom)) {
                            alert("Decimal values are not allowed for Exshowroom");
                            return false;
                        } else if ($scope.insurance != null && $scope.insurance != "" && $scope.insurance.length > 0 && !$scope.checkDecimal($scope.insurance)) {
                            alert("Special character and Decimal Value are not allowed for Insurance");
                            return false;
                        }
//                        else if (!$scope.checkDecimal($scope.insurance)) {
//                            alert("Decimal values are not allowed for Insurance");
//                        }
                        else if ($scope.registration != null && $scope.registration != "" && $scope.registration.length > 0 && !$scope.checkDecimal($scope.registration)) {
                            alert("Special character and Decimal Value are not allowed for Registration");
                            return false;
                        }
//                        else if (!$scope.checkDecimal($scope.registration)) {
//                            alert("Decimal values are not allowed for Registration");
//                        }
                        else if ($scope.othersloandetails != null && $scope.othersloandetails != "" && $scope.othersloandetails.length > 0 && !$scope.checkDecimal($scope.othersloandetails)) {
                            alert("Special character and Decimal Value are not allowed for Othersloandetails");
                            return false;
                        }
//                        else if (!$scope.checkDecimal($scope.othersloandetails)) {
//                            alert("Decimal values are not allowed for Othersloandetails");
//                        }
//                        else if ($scope.assetcost == null || $scope.assetcost == "") {
//                            alert("Please Enter Asset Cost");
//                        }
                        else if ($scope.appliedloanamount == null || $scope.appliedloanamount == "") {
                            alert("Please Enter Applied Loan Amount");
                            return false;
                        } else if (!$scope.checkDecimal($scope.appliedloanamount)) {
                            alert("Decimal values are not allowed for Applied Loan Amount");
                            return false;
                        } else if ($scope.appliedloanamount > $scope.assetcost) {
                            alert(" Loan Amount should not be greater than Asset Cost");
                            return false;
                        }
//                        else if ($scope.appliedmarginmoney == null || $scope.appliedmarginmoney == "") {
//                            alert("Please Enter Appliedmarginmoney");
//                        }
                        else if ($scope.appliedtenure == null || $scope.appliedtenure == "") {
                            alert("Please Enter Applied Tenure");
                            return false;
                        } else if (!$scope.checkDecimal($scope.appliedtenure)) {
                            alert("Decimal values are not allowed for Applied Tenure");
                            return false;
                        } else if ($scope.advinstall == null || $scope.advinstall == "") {
                            alert("Please Enter Advance Installment");
                            return false;
                        } else if (!$scope.checkDecimal($scope.advinstall)) {
                            alert("Decimal values are not allowed for Advance Installment");
                            return false;
                        } else if ($scope.advinstall >= $scope.appliedtenure) {
                            alert(" Advance installment should be less than Applied Tenure");
                            return false;
                        } else if ($scope.ratepercent == null || $scope.ratepercent == "") {
                            alert("Please Enter Ratepercent");
                            return false;
                        } else if (!$scope.checkDecimal($scope.ratepercent)) {
                            alert("Decimal values are not allowed for Rate Percent");
                            return false;
                        }
                        else if (!$scope.checkDecimalValue($scope.ratepercent)) {
                            alert("Only Numbers are allowed for Ratepercent!\nRatepercent allowed two digit after decimal");
                            return false;
                        }
//                        else if ($scope.loanemiloandetails == null || $scope.loanemiloandetails == "") {
//                            alert("Please Enter loanemiloandetails");
//                        } 
                        else if ($scope.selectfrequency == null || $scope.selectfrequency == "") {
                            alert("Please Select Selectfrequency");
                            return false;
                        }
//                        else if ($scope.appliedltv == null || $scope.appliedltv == "") {
//                            alert("Please Enter Appliedltv");
//                        }
                        else if ($scope.isVehicleDelivered == 'Y' && ($scope.appliedltv > $scope.preAppliedltv)) {
                            alert("Required LTV should be less than or equal to Sanctioned LTV");
                            return false;
                        } else if ($scope.insurancepremiumcollection == null || $scope.insurancepremiumcollection == "0") {
                            alert("Please Enter Insurance Premium Collection");
                            return false;
                        } else if ($scope.appliedloanamount < $scope.loanVO.minFinance || $scope.appliedloanamount > $scope.loanVO.maxFinance) {
                            alert("Loan amount should be between " + $scope.loanVO.minFinance + " and " + $scope.loanVO.maxFinance);
                            return false;
                        } else if ($scope.ratepercent < $scope.loanVO.minRate || $scope.ratepercent > $scope.loanVO.maxRate) {
                            alert("Rate should be between " + $scope.loanVO.minRate + " and " + $scope.loanVO.maxRate);
                            return false;
                        } else if ($scope.appliedtenure < $scope.loanVO.minTenure || $scope.appliedtenure > $scope.loanVO.maxTenure) {
                            alert("Tenure should be between " + $scope.loanVO.minTenure + " and " + $scope.loanVO.maxTenure);
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    
                     $scope.saveCheck = function(rout) {
                        if (rout == 'R') {                // Reset
                            $scope.lpcReset();
                        } else if (rout == 'C') {         // Cancel
                            $scope.cancelLpc();
                        }else if ($scope.validateLpc()) {
                            var modalInstance;
                            modalInstance = $modal.open({
                                templateUrl: "confirmationContent.html",
                                controller: "ConfirmationModalInstanceCtrl",
                                resolve: {
                                    message: function() {
                                        return "Do you want to save ?";
                                    }
                                }
                            }), modalInstance.result.then(function(isSave) {
                                $log.info("Response: " + isSave);
                                if (isSave) {
                                    
                                    if(rout=='S'){               // Save
                                      $scope.submitLpcForm();  
                                    }else if(rout=='H'){         // HL submit
                                      $scope.submitLpc();  
                                    }
                                    
                                }
                            }, function() {
                                $log.info("Modal dismissed at: " + new Date)
                            });
                        }
                    }

                    $scope.submitLpcForm = function() {
                            var callURL = $rootScope.urlDomain + "saveLpcData.json";
                            var data = {
                                selectmanufacture: $scope.selectmanufacture,
                                selectassetmake: $scope.selectassetmake,
                                selectassetmodel: $scope.selectassetmodel.modelId,
                                selectassetcategory: $scope.selectassetmodel.catgId,
                                isVehicleDelivered: $scope.isVehicleDelivered,
                                exshowroom: $scope.exshowroom,
                                insurance: $scope.insurance,
                                registration: $scope.registration,
                                othersloandetails: $scope.othersloandetails,
                                assetcost: $scope.assetcost,
                                appliedloanamount: $scope.appliedloanamount,
                                appliedmarginmoney: $scope.appliedmarginmoney,
                                appliedtenure: $scope.appliedtenure,
                                advinstall: $scope.advinstall,
                                ratepercent: $scope.ratepercent,
                                loanemiloandetails: $scope.loanemiloandetails,
                                selectfrequency: $scope.selectfrequency,
                                appliedltv: $scope.appliedltv,
                                insurancepremiumcollection: $scope.insurancepremiumcollection,
                                userId: $rootScope.userId,
                                applicationId: $rootScope.applicationId
                            };
//                            console.log(data);
                            $http({
                                method: "POST",
                                url: callURL,
                                // transformRequest: transformRequestAsFormPost,
                                data: data
                            }).success(function(data) {
                                if (data.status == "200") {
                                    $location.path('/lpcdashboard');
                                    
                                } else {
                                    console.log(data);
                                    // alert(data.msg);
                                }


                            }).error(function(error) {
                                console.log(error);
                            });
                        


                    }


                }]).controller("postSanctionChargesCtrl", ["$cookies", "$rootScope", "$scope", "$http", "$location", "$filter", "$modal", "$log", function($cookies, $rootScope, $scope, $http, $location, $filter, $modal, $log) {

                    $scope.amtBroker = 0;
                    $scope.amtSupp = 0;

                    $scope.nullValidation = function(val) {
                        if (angular.isUndefined(val) || val === null || val === "null") {
                            return "";
                        } else {
                            return val;
                        }
                    }
                    
                    $scope.saveCheck = function() {
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "confirmationContent.html",
                            controller: "ConfirmationModalInstanceCtrl",
                            resolve: {
                                message: function() {
                                    return "Do you want to save ?";
                                }
                            }
                        }), modalInstance.result.then(function(isSave) {
                            $log.info("Response: " + isSave);
                            if (isSave) {
                                // Save
                                $scope.submitChargesForm();
                            }
                        }, function() {
                            $log.info("Modal dismissed at: " + new Date)
                        });
                    }

//                    $scope.bpTypeChange = function() {
//                        $scope.calulateTaxBroker();
//                        $scope.calculateTaxSupp();
//                    }

                    $scope.checkDecimalValue = function(checkValue) {
                        var value = /^[0-9]{1,2}(\.[0-9]{1,2})?$/;
                        return value.test(checkValue);
                    }
                    
                    $scope.checkNumValue = function(checkValue) {
                        var value = /^[0-9]{1,12}$/;
                        return value.test(checkValue);
                    }

                    $scope.calculateAmt = function(index) {
                        
                        if ($scope.chargeDescriptionArr[index].chargeRate == null || $scope.chargeDescriptionArr[index].chargeRate == "" || !$scope.checkDecimalValue($scope.chargeDescriptionArr[index].chargeRate)) {
                            alert("Only Numbers are allowed for Rate!\nRate allowed two digit before decimal and after decimal");
                        } else {

                            $scope.chargeDescriptionArr[index].calchargeamt = Math.round(parseFloat($scope.loanAmount) * parseFloat($scope.chargeDescriptionArr[index].chargeRate) / 100);
                            if ($scope.chargeDescriptionArr[index].taxApplFlg == "Y") {
                                $scope.chargeDescriptionArr[index].chargeAmount = $scope.chargeDescriptionArr[index].calchargeamt - ((parseFloat($scope.chargeConsVO.serviceTax) + parseFloat($scope.chargeConsVO.surcharge)) / 100 * $scope.chargeDescriptionArr[index].calchargeamt);
                                if ($scope.chargeDescriptionArr[index].bptype == $scope.chargeConsVO.bpBroker) {
                                    $scope.calulateTaxBroker();
                                } else if ($scope.chargeDescriptionArr[index].bptype == $scope.chargeConsVO.bpSupplier) {
                                    $scope.calculateTaxSupp();
                                }
                            } else {
                                $scope.chargeDescriptionArr[index].chargeAmount = $scope.chargeDescriptionArr[index].calchargeamt;
                            }

                        }
                    }

                    $scope.calculateRate = function(index) {
                         if ($scope.chargeDescriptionArr[index].calchargeamt == null || $scope.chargeDescriptionArr[index].calchargeamt == "" || !$scope.checkNumValue($scope.chargeDescriptionArr[index].calchargeamt)) {
                            alert("Only Numbers are allowed for Amount.");
                        } else {
                            
                        
                        $scope.chargeDescriptionArr[index].chargeRate = Math.round(parseFloat($scope.chargeDescriptionArr[index].calchargeamt) / $scope.loanAmount * 10000) / 100;
                        if ($scope.chargeDescriptionArr[index].taxApplFlg == "Y") {
                            $scope.chargeDescriptionArr[index].chargeAmount = Math.round(((100 - $scope.chargeConsVO.serviceTax - $scope.chargeConsVO.surcharge) / 100 * $scope.chargeDescriptionArr[index].calchargeamt) * 100) / 100;
                            if ($scope.chargeDescriptionArr[index].bptype == $scope.chargeConsVO.bpBroker) {
                                $scope.calulateTaxBroker();
                            } else if ($scope.chargeDescriptionArr[index].bptype == $scope.chargeConsVO.bpSupplier) {
                                $scope.calculateTaxSupp();
                            }
                        } else {
                            $scope.chargeDescriptionArr[index].chargeAmount = $scope.chargeDescriptionArr[index].calchargeamt;
                        }
                        }
                    }

                    $scope.calulateTaxBroker = function() {
                        $scope.amtBroker = 0;
                        angular.forEach($scope.chargeDescriptionArr, function(charges, index) {
                            if (charges.bptype == $scope.chargeConsVO.bpBroker && charges.taxApplFlg == "Y") {
                                $scope.amtBroker = parseFloat($scope.amtBroker) + parseFloat(charges.calchargeamt);
                            }
                        });
                        $scope.chargeDescriptionArr[$scope.indexTaxBroker].chargeAmount = Math.round(($scope.chargeConsVO.serviceTax / 100 * $scope.amtBroker) * 100) / 100;
                        $scope.chargeDescriptionArr[$scope.indexSurTaxBroker].chargeAmount = Math.round(($scope.chargeConsVO.surcharge / 100 * $scope.amtBroker) * 100) / 100;
                    }
                    $scope.calculateTaxSupp = function() {
                        $scope.amtSupp = 0;
                        angular.forEach($scope.chargeDescriptionArr, function(charges, index) {
                            if (charges.bptype == $scope.chargeConsVO.bpSupplier && charges.taxApplFlg == "Y") {
                                $scope.amtSupp = parseFloat($scope.amtSupp) + parseFloat(charges.calchargeamt);
                            }
                        });
                        $scope.chargeDescriptionArr[$scope.indexTaxSupp].chargeAmount = Math.round(($scope.chargeConsVO.serviceTax / 100 * $scope.amtSupp) * 100) / 100;
                        $scope.chargeDescriptionArr[$scope.indexSurTaxSupp].chargeAmount = Math.round(($scope.chargeConsVO.surcharge / 100 * $scope.amtSupp) * 100) / 100;
                    }

                    $scope.isCustomCalc = function(index) {
                        if ($scope.indexMarginMoney == index) {
                            return true;
                        } else if ($scope.indexTaxBroker == index) {
                            return true;
                        } else if ($scope.indexTaxSupp == index) {
                            return true;
                        } else if ($scope.indexSurTaxBroker == index) {
                            return true;
                        } else if ($scope.indexSurTaxSupp == index) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                    
                    
                    $scope.validatePostSanctionChargesForm = function() {
                        $scope.saveCheck();
                    }

                    $rootScope.preloadPostSanctionCharges = function() {

                        $scope.chargeDescriptionArr = [];
//    $scope.paymentmodeArr = [{value:"0", description: 'Select Payment Mode'}];

                        $scope.businessPartnerArr = [{value: "0", description: 'Select Business Partner'}, {value: 'S1', description: "Supplier"}, {value: 'B2', description: "DMA"}];
                        $scope.chargeTypeArr = [{value: "0", description: 'Select Charge Type'}, {value: 'A', description: "Amount"}, {value: 'R', description: "Rate"}];
                        var data = {
                            userId: $rootScope.userId,
                            applicationId: $rootScope.applicationId
                        }
                        var callURL = $rootScope.urlDomain + "FetchChargesData.json";
                        $http({
                            method: "POST",
                            url: callURL,
                            data: data
                        }).success(function(response) {
                            console.log(response);
                            if (response.status == "200") {
                                $scope.chargeConsVO = response.chargeConsVO;
                                $scope.loanAmount = response.loanDetails.loanAmount;
                                $scope.marginMoney = response.loanDetails.marginMoney;
                                $scope.advEmi = response.loanDetails.loanEmi*response.loanDetails.advInstl;
                                $scope.cli = Math.ceil(response.loanDetails.totalPremiumAmount);
//                                $scope.chargeDescriptionArr = response.listCharges;

                                angular.forEach(response.listCharges, function(charges, index) {
                                    var rate = 0;
                                    var amt = 0;
                                    var calchargeamt = 0;
                                    if (charges.chargeid == $scope.chargeConsVO.chargeMarginMoney) {
                                        $scope.indexMarginMoney = index;
                                        rate = Math.round(parseFloat($scope.marginMoney) / parseFloat($scope.loanAmount) * 10000)/100;
                                        amt = Math.round(parseFloat($scope.marginMoney));
                                        calchargeamt = amt;
                                    }else if (charges.chargeid == $scope.chargeConsVO.chargeAdvEmi) {
                                        $scope.indexAdvEmi = index;
                                        rate = Math.round(parseFloat($scope.advEmi) / parseFloat($scope.loanAmount) * 10000)/100;
                                        amt = Math.round(parseFloat($scope.advEmi));
                                        calchargeamt = amt;
                                    }else if (charges.chargeid == $scope.chargeConsVO.chargeCli) {
                                        $scope.indexCli = index;
                                        rate = Math.round(parseFloat($scope.cli) / parseFloat($scope.loanAmount) * 10000)/100;
                                        amt = Math.round(parseFloat($scope.cli));
                                        calchargeamt = amt;
                                    }
                                    else if (charges.chargeid == $scope.chargeConsVO.chargeSerBroker) {
                                        $scope.indexTaxBroker = index;
                                        rate = 0;
                                    } else if (charges.chargeid == $scope.chargeConsVO.chargeSerSupplier) {
                                        $scope.indexTaxSupp = index;
                                        rate = 0;
                                    } else if (charges.chargeid == $scope.chargeConsVO.chargeSurBroker) {
                                        $scope.indexSurTaxBroker = index;
                                        rate = 0;
                                    } else if (charges.chargeid == $scope.chargeConsVO.chargeSurSupplier) {
                                        $scope.indexSurTaxSupp = index;
                                        rate = 0;
                                    } else {
                                        if (charges.chargeRate != null) {
                                            rate = charges.chargeRate;
                                        }
                                        if (charges.chargeAmount != null) {
                                            amt = charges.chargeAmount;
                                        }
                                        if (charges.calchargeamt != null) {
                                            calchargeamt = Math.round(charges.calchargeamt);
                                        }
                                    }
                                    var tempObject = {
                                        dataChargeId: $scope.nullValidation(charges.dataChargeId),
                                        taxApplFlg: $scope.nullValidation(charges.taxApplFlg),
                                        chargeRate: rate,
                                        chargeAmount: amt,
                                        moduleid: $scope.nullValidation(charges.moduleid),
                                        txntype: $scope.nullValidation(charges.txntype),
                                        txnid: $scope.nullValidation(charges.txnid),
                                        chargeid: $scope.nullValidation(charges.chargeid),
                                        bpid: $scope.nullValidation(charges.bpid),
                                        bptype: $scope.nullValidation(charges.bptype),
                                        adviceflg: $scope.nullValidation(charges.adviceflg),
                                        chargecodeid: $scope.nullValidation(charges.chargecodeid),
                                        rcptpmntflg: $scope.nullValidation(charges.rcptpmntflg),
                                        chargedesc: $scope.nullValidation(charges.chargedesc),
                                        currencyid: $scope.nullValidation(charges.currencyid),
                                        productid: $scope.nullValidation(charges.chSchemeId),
                                        chargetype: $scope.nullValidation(charges.chargetype),
                                        calchargeamt: calchargeamt,
                                        creditPeriod: $scope.nullValidation(charges.creditPeriod),
                                        content: $scope.loanAmount

                                    };
                                    $scope.chargeDescriptionArr.push(tempObject);
                                });
                                $scope.calulateTaxBroker();
                                $scope.calculateTaxSupp();

                            }

                        }).error(function(error) {
                            console.log(error);
                        });
                    };




                    $scope.submitChargesForm = function() {
                        var flagError = false;
//                        angular.forEach($scope.chargeDescriptionArr, function(charge, index) {
//                            if (charge.creditPeriod == null || charge.creditPeriod == "0")
//                            {
//                                alert("Please Enter CreditPeriod");
//                            } 
//                        });


                        if (!false) {

                            var callURL = $rootScope.urlDomain + "SavePostSanctionChargesData.json";
                            var data = {
                                chargeArr: $scope.chargeDescriptionArr,
                                userId: $rootScope.userId,
                                applicationId: $rootScope.applicationId,
                            };
                            $http({
                                method: "POST",
                                url: callURL,
                                data: data
                            }).success(function(data) {
                                if (data.status == "200") {
                                    $cookies.a = data.applicationId;
                                    $rootScope.applicationId = data.applicationId;
                                    $location.path('/postsanction/disbursementdetail');
                                }
                                console.log(data);
                            }).error(function(error) {
                                console.log(error);
                            });
                        }
                    }




                }])
                    .controller("lpcDashboardCtrl", ["$cookies", "$rootScope", "$scope", "$filter", "$http", "$location", function($cookies, $rootScope, $scope, $filter, $http, $location) {
                            var init;
                            $scope.preload = function preload() {
                                $rootScope.setCookies(0, 0);
                                var callURL = $rootScope.urlDomain + "lpcDashboard.json";
                                var data = {
                                    userId: $rootScope.userId
                                };
                                console.log($rootScope.userId + " " + callURL + " " + data);
                                $http({
                                    method: "post",
                                    url: callURL,
                                    data: data
                                }).success(function(response, status, headers, config)
                                {

                                    if (response.status == "200") {
                                        $scope.stores = response.lstDashboardVO,
                                                $scope.searchKeywords = "", $scope.filteredStores = [], $scope.row = "", $scope.select = function(page) {
                                            var end, start;
                                            $scope.currentPage = page;
                                            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
                                        }, $scope.onFilterChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
                                        }, $scope.onNumPerPageChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1
                                        }, $scope.onOrderChange = function() {
                                            return $scope.select(1), $scope.currentPage = 1
                                        }, $scope.search = function() {
                                            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
                                        }, $scope.order = function(rowName) {
                                            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
                                        }, $scope.numPerPageOpt = [5, 10, 15, 30, 50, 100], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageStores = [], (init = function() {
                                            return $scope.search(), $scope.select($scope.currentPage)
                                        })()

                                    } else {
                                        alert(response.msg);
                                    }
                                }).error(function(data, status, headers, config)
                                {
                                    alert("Error while requesting");
                                });
                            };
                            $scope.showform = function showform(appId, progress) {
                                $cookies.a = appId;
                                $rootScope.applicationId = appId;
                                $cookies.c = progress;
                                $rootScope.progress = progress;
                                $location.path('/lpc/lpc');
                            };
                        }]).controller("postSanctionController", ["$scope", "$rootScope", "$http", "$location", function($scope, $rootScope, $http, $location) {

                    $rootScope.openForm = function(formNumber) {
                        $rootScope.progress = formNumber;
                        if (formNumber <= $rootScope.progress || $rootScope.progress >= 3)
                        {
                            updateTabs(formNumber, $rootScope.progress);
                            enableForm(formNumber);
                        }
                    };


                    $scope.selectForm = function(formNo) {

                        $rootScope.openForm(formNo);
                    }
                    var enableForm = function(formNumber) {
                        hideForms();
                        if (formNumber == 0) {
                            $('#form_post_sanstion_sourcing').css("display", "block");
                        }
                        else if (formNumber == 1) {
                            $('#form_post_sanstion_bank_details').css("display", "block");
                            $rootScope.preloadPostSanctionBankDetails();
                        }
                        else if (formNumber == 2) {
                            $('#form_post_sanstion_disbursement_details').css("display", "block");
                            $rootScope.preloadPostSanctionDisbursement();
                        }
                        else if (formNumber == 3) {
                            $('#form_post_sanstion_insurance_details').css("display", "block");
                            $rootScope.preloadPostSanctionInsuranceDetails();
                        }
                        else if (formNumber == 4) {
                            $('#form_post_sanstion_assetinsurance').css("display", "block");
                            $rootScope.preloadpostSanctionAssetInsurance();
                        }
                        else if (formNumber == 5) {
                            $('#form_post_sanstion_reference_details').css("display", "block");
                            $rootScope.preloadPostSanctionReferenceDetails();
                        }
                        else if (formNumber == 6) {
                            $('#form_post_sanstion_documents').css("display", "block");
                        }
                        else if (formNumber == 7) {
                            $('#form_post_sanstion_instruments_details').css("display", "block");
                            $rootScope.preloadPostSanctionInstrumentsDetails();
                        }
                        else if (formNumber == 8) {
                            $('#form_post_sanstion_asset_details').css("display", "block");
                            $rootScope.preloadPostSanctionAssetDetails();
                        }
                        else if (formNumber == 9) {
                            $('#form_post_sanstion_charges').css("display", "block");
                            $rootScope.preloadPostSanctionAssetDetails();
                        }

                    }

                    var updateTabs = function(formNumber, progress) {
                        if (progress == 0) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 1) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 2) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 3) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 4) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 5) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 6) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 7) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        else if (progress == 8) {
                            setClass($('#tab_post_sanstion_sourcing'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_reference_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_bank_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_asset_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_documents'), 'TabEnabled');
                            setClass($('#tab_post_sanstion_charges'), 'TabEnabled');
                        }
                        if (formNumber == 0)
                            setClass($('#tab_post_sanstion_sourcing'), 'TabActive');
                        if (formNumber == 1)
                            setClass($('#tab_post_sanstion_bank_details'), 'TabActive');
                        if (formNumber == 2)
                            setClass($('#tab_post_sanstion_disbursement_details'), 'TabActive');
                        if (formNumber == 3)
                            setClass($('#tab_post_sanstion_insurance_details'), 'TabActive');
                        if (formNumber == 4)
                            setClass($('#tab_post_sanstion_assetinsurance'), 'TabActive');
                        if (formNumber == 5)
                            setClass($('#tab_post_sanstion_reference_details'), 'TabActive');
                        if (formNumber == 6)
                            setClass($('#tab_post_sanstion_documents'), 'TabActive');
                        if (formNumber == 7)
                            setClass($('#tab_post_sanstion_instruments_details'), 'TabActive');
                        if (formNumber == 8)
                            setClass($('#tab_post_sanstion_asset_details'), 'TabActive');
                        if (formNumber == 9)
                            setClass($('#tab_post_sanstion_charges'), 'TabActive');

                    }


                    var setClass = function(view, className) {
                        view.removeClass('TabActive').removeClass('TabDisabled').removeClass('TabEnabled');
                        view.addClass(className);
                    };

                    var hideForms = function() {
                        $('#form_post_sanstion_sourcing').css("display", "none");
                        $('#form_post_sanstion_instruments_details').css("display", "none");
                        $('#form_post_sanstion_reference_details').css("display", "none");
                        $('#form_post_sanstion_insurance_details').css("display", "none");
                        $('#form_post_sanstion_assetinsurance').css("display", "none");
                        $('#form_post_sanstion_bank_details').css("display", "none");
                        $('#form_post_sanstion_asset_details').css("display", "none");
                        $('#form_post_sanstion_disbursement_details').css("display", "none");
                        $('#form_post_sanstion_documents').css("display", "none");
                        $('#form_post_sanstion_charges').css("display", "none");

                    }


                }])

        }.call(this);


